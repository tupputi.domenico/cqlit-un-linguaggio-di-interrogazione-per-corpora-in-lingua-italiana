/*
 * Word.java
 *
 * Created on 2 aprile 2005, 13.07
 */
package di.uniba.it.nlpita.acopost;

/**
 *
 * @author  Basile Pierpaolo and Grieco Franco
 */
public class Word {

    private String string;
    private int count;
    private int[] tagcount = null;
    private double[] lp;

    /** Creates a new instance of Word */
    public Word(String s, int cnt, int not) {

        this.string = s;
        this.count = cnt;
        tagcount = new int[not];
        lp = new double[not];
        for (int i = 0; i < not; i++) {
            lp[i] = -Double.MAX_VALUE;
            tagcount[i] = 0;
        }




    }

    /**
     * Getter for property count.
     * @return Value of property count.
     */
    public int getCount() {
        return count;
    }

    /**
     * Setter for property count.
     * @param count New value of property count.
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Getter for property lp.
     * @return Value of property lp.
     */
    public double[] getLp() {
        return lp;
    }

    /**
     * Setter for property lp.
     * @param lp New value of property lp.
     */
    public void setLp(double[] lp) {
        this.lp = lp;
    }

    /**
     * Getter for property string.
     * @return Value of property string.
     */
    public java.lang.String getString() {
        return string;
    }

    /**
     * Setter for property string.
     * @param string New value of property string.
     */
    public void setString(java.lang.String string) {
        this.string = string;
    }

    /**
     * Getter for property tagcount.
     * @return Value of property tagcount.
     */
    public int[] getTagcount() {
        return tagcount;
    }

    /**
     * Setter for property tagcount.
     * @param tagcount New value of property tagcount.
     */
    public void setTagcount(int[] tagcount) {
        this.tagcount = tagcount;
    }
}
