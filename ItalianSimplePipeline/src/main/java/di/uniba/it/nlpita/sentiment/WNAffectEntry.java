/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.sentiment;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author pierpaolo
 */
public class WNAffectEntry {

    private String synset;

    private Set<String> labels = new HashSet<>();

    private Set<String> lemmas = new HashSet<>();

    private String relation;

    public WNAffectEntry(String synset) {
        this.synset = synset;
    }

    public WNAffectEntry(String synset, String relation) {
        this.synset = synset;
        this.relation = relation;
    }

    public String getSynset() {
        return synset;
    }

    public void setSynset(String synset) {
        this.synset = synset;
    }

    public Set<String> getLabels() {
        return labels;
    }

    public void setLabels(Set<String> labels) {
        this.labels = labels;
    }

    public Set<String> getLemmas() {
        return lemmas;
    }

    public void setLemmas(Set<String> lemmas) {
        this.lemmas = lemmas;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.synset);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WNAffectEntry other = (WNAffectEntry) obj;
        if (!Objects.equals(this.synset, other.synset)) {
            return false;
        }
        return true;
    }

}
