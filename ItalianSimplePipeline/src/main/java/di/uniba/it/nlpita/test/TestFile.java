/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.test;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.Utils;
import di.uniba.it.nlpita.sentiment.SentixAPI;
import di.uniba.it.nlpita.sentiment.WNAffectAPI;
import di.uniba.it.nlpita.sentiment.WordNetFreqAPI;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class TestFile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            List<List<Token>> processedText = instance.processText(Utils.loadTextFromFile(new File(args[0]), false));
            WordNetFreqAPI wnapi = new WordNetFreqAPI();
            wnapi.init(new File("it_nlp/sentiment/index.sense.gz"));
            SentixAPI sentapi = new SentixAPI(wnapi);
            sentapi.init(new File("it_nlp/sentiment/sentix.gz"));
            WNAffectAPI affectapi = new WNAffectAPI();
            affectapi.init(new File("it_nlp/sentiment/wn_affect.lex"));
            float polarityScore = sentapi.getTextPolarityScore(processedText);
            Map<String, Integer> affectMap = affectapi.getTextAffectMap(processedText);
            for (List<Token> list : processedText) {
                for (Token token : list) {
                    System.out.println(token);
                }
                System.out.println();
            }
            /*System.out.println("Polarity score: " + polarityScore);
            System.out.println("Emo labels: " + affectMap);
            System.out.println("Semplicity score: " + Utils.getSimplictyScore(processedText));
            System.out.println("Gulpease score: " + Utils.getGulpeaseScore(processedText));*/
        } catch (Exception ex) {
            Logger.getLogger(TestFile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
