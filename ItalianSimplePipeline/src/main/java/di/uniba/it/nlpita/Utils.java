/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author pierpaolo
 */
public class Utils {

    /**
     * Load a string hashset given an input file, one string per line
     *
     * @param file Input file
     * @return String hashset
     * @throws IOException
     */
    public static Set<String> loadFileInSet(File file) throws IOException {
        Set<String> set = new HashSet<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        while (reader.ready()) {
            set.add(reader.readLine().toLowerCase().trim());
        }
        reader.close();
        return set;
    }

    public static float getSimplictyScore(List<List<Token>> sentences) {
        float s = 0;
        float d = 0;
        for (List<Token> sentence : sentences) {
            for (Token t : sentence) {
                if (t.isCommonWord() || t.isStopWord()) {
                    s++;
                }
                d++;
            }
        }
        if (d == 0) {
            return 0;
        } else {
            return s / d;
        }
    }

    public static float getGulpeaseScore(List<List<Token>> sentences) {
        float s = 0;
        float w = 0;
        float c = 0;
        for (List<Token> sentence : sentences) {
            s++;
            w += sentence.size();
            for (Token token : sentence) {
                c += token.getToken().length();
            }
        }
        return 89 - (c * 100 / w) / 10 + (s * 100 / w) * 3;
    }

    public static float getColemanLiauScore(List<List<Token>> sentences) {
        float s = 0;
        float w = 0;
        float c = 0;
        for (List<Token> sentence : sentences) {
            s++;
            w += sentence.size();
            for (Token token : sentence) {
                c += token.getToken().length();
            }
        }
        return 0.0588f * (c * 100 / w) + 0.296f * (s * 100 / w) - 15.8f;
    }

    public static float getARIScore(List<List<Token>> sentences) {
        float s = 0;
        float w = 0;
        float c = 0;
        for (List<Token> sentence : sentences) {
            s++;
            w += sentence.size();
            for (Token token : sentence) {
                c += token.getToken().length();
            }
        }
        return 4.71f * (c / w) + 0.5f * (w / s) - 21.43f;
    }

    public static String loadTextFromFile(File file, boolean skipFirstLine) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        if (skipFirstLine && reader.ready()) {
            reader.readLine();
        }
        while (reader.ready()) {
            sb.append(reader.readLine());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String loadTextFromFile(File file, String encoding, boolean skipFirstLine) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        if (skipFirstLine && reader.ready()) {
            reader.readLine();
        }
        while (reader.ready()) {
            sb.append(reader.readLine());
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String[] getConll(String[] tokens, String[] postags, MorphoEntry[] morphoEntries) {
        StringBuilder writer = new StringBuilder();
        for (int i = 0; i < tokens.length; i++) {
            writer.append(String.valueOf(i + 1)).append("\t");
            writer.append(tokens[i]).append("\t");
            writer.append(morphoEntries[i].getLemma()).append("\t");
            writer.append(postags[i].substring(0, 1)).append("\t");
            writer.append(postags[i]).append("\t");
            String[] split = morphoEntries[i].getMorphoInfo().split(":");
            if (split.length > 1) {
                String[] f = split[1].split("[+]");
                for (int j = 0; j < f.length; j++) {
                    writer.append(f[j]);
                    if (j < f.length - 1) {
                        writer.append("|");
                    }
                }
            } else {
                writer.append("_");
            }
            writer.append("\n");
        }
        return writer.toString().split("\\n");
    }

    public static POSenum getPos(String tag) {
        POSenum postag = null;
        if (tag.startsWith("S")) {
            postag = POSenum.NOUN;
        } else if (tag.startsWith("V")) {
            postag = POSenum.VERB;
        } else if (tag.startsWith("A") || tag.startsWith("NO")) {
            postag = POSenum.ADJ;
        } else if (tag.startsWith("B")) {
            postag = POSenum.ADV;
        } else {
            postag = POSenum.OTHER;
        }
        return postag;
    }

}
