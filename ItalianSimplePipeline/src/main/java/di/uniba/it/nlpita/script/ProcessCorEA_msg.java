/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.Utils;
import di.uniba.it.nlpita.sentiment.SentixAPI;
import di.uniba.it.nlpita.sentiment.SentixEntry;
import di.uniba.it.nlpita.sentiment.WNAffectAPI;
import di.uniba.it.nlpita.sentiment.WordNetFreqAPI;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class ProcessCorEA_msg {

    private static final Logger LOG = Logger.getLogger(ProcessCorEA_msg.class.getName());

    private static int numWords(List<List<Token>> processedText) {
        int w = 0;
        for (List<Token> sentence : processedText) {
            w += sentence.size();
        }
        return w;
    }

    private static float getMaxPositivePolarity(List<List<Token>> processedText) {
        float maxPos = 0;
        for (List<Token> sentence : processedText) {
            for (Token token : sentence) {
                if (token.getPolarity() > 0 && token.getPolarity() >= maxPos) {
                    maxPos = token.getPolarity();
                }
            }
        }
        return maxPos;
    }

    private static float getMaxNegativePolarity(List<List<Token>> processedText) {
        float maxNeg = 0;
        for (List<Token> sentence : processedText) {
            for (Token token : sentence) {
                if (token.getPolarity() < 0 && token.getPolarity() < maxNeg) {
                    maxNeg = token.getPolarity();
                }
            }
        }
        return maxNeg;
    }

    private static void saveMultiset(File file, Multiset<String> mset) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        for (Multiset.Entry<String> entry : mset.entrySet()) {
            writer.append(entry.getElement()).append("\t").append(String.valueOf(entry.getCount()));
            writer.newLine();
        }
        writer.close();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            WordNetFreqAPI wnapi = new WordNetFreqAPI();
            wnapi.init(new File("it_nlp/sentiment/index.sense.gz"));
            SentixAPI sentapi = new SentixAPI(wnapi);
            sentapi.init(new File("it_nlp/sentiment/sentix.gz"));
            WNAffectAPI affectapi = new WNAffectAPI();
            affectapi.init(new File("it_nlp/sentiment/wn_affect.lex"));
            File inputFile = new File(args[0]);
            String outputdir = args[1];
            BufferedWriter mainStats = new BufferedWriter(new FileWriter(outputdir + "/main_stats.tsv"));
            mainStats.append("id\tnum-sentences\tnum-words\tsemplicity\tGulpease\tavg-polarity\tmax-pos\tmax-neg");
            Set<String> affectLabels = affectapi.getLabels();
            for (String al : affectLabels) {
                mainStats.append("\t");
                mainStats.append(al);
            }
            mainStats.newLine();
            Multiset<String> lex = HashMultiset.create();
            Multiset<String> lexPos = HashMultiset.create();
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            int pc=0;
            //skip header
            if (reader.ready())
                reader.readLine();
            while (reader.ready()) {
                String[] split = reader.readLine().split("\t");
                String mid = split[0];
                List<List<Token>> processedText = instance.processText(split[6]);
                float polarityScore = sentapi.getTextPolarityScore(processedText);
                Map<String, Integer> affectMap = affectapi.getTextAffectMap(processedText);
                mainStats.append(mid).append("\t");
                mainStats.append(String.valueOf(processedText.size())).append("\t");
                mainStats.append(String.valueOf(numWords(processedText))).append("\t");
                mainStats.append(String.valueOf(Utils.getSimplictyScore(processedText))).append("\t");
                mainStats.append(String.valueOf(Utils.getGulpeaseScore(processedText))).append("\t");
                mainStats.append(String.valueOf(polarityScore)).append("\t");
                mainStats.append(String.valueOf(getMaxPositivePolarity(processedText))).append("\t");
                mainStats.append(String.valueOf(getMaxNegativePolarity(processedText)));
                for (String al : affectLabels) {
                    if (affectMap == null || !affectMap.containsKey(al)) {
                        mainStats.append("\t0");
                    } else {
                        mainStats.append("\t").append(affectMap.get(al).toString());
                    }
                }
                mainStats.newLine();
                BufferedWriter fileStats = new BufferedWriter(new FileWriter(outputdir + "/" + mid + "_stats.tsv"));
                fileStats.append("token\tmain-POS\tlemma\tPOS\tmorpho\tchunk\tIob2\tDep\tis-stop-word\tis-common-word\tpolarity");
                for (String al : affectLabels) {
                    fileStats.append("\t");
                    fileStats.append(al);
                }
                fileStats.newLine();
                for (List<Token> list : processedText) {
                    for (Token token : list) {
                        fileStats.append(token.getToken());
                        fileStats.append("\t");
                        fileStats.append(token.getPostag());
                        fileStats.append("\t");
                        fileStats.append(token.getLemma());
                        fileStats.append("\t");
                        fileStats.append(token.getPos().toString());
                        fileStats.append("\t");
                        fileStats.append(token.getMorphoInfo());
                        fileStats.append("\t");
                        fileStats.append(token.getChunk());
                        fileStats.append("\t");
                        fileStats.append(token.getIob2());
                        fileStats.append("\t");
                        fileStats.append(token.getDepInfo());
                        fileStats.append("\t");
                        fileStats.append(String.valueOf(token.isStopWord()));
                        fileStats.append("\t");
                        fileStats.append(String.valueOf(token.isCommonWord()));
                        fileStats.append("\t");
                        fileStats.append(String.valueOf(token.getPolarity()));
                        for (String al : affectLabels) {
                            if (token.getEmolabels() == null || !token.getEmolabels().contains(al)) {
                                fileStats.append("\t0");
                            } else {
                                fileStats.append("\t1");
                            }
                        }
                        fileStats.newLine();
                        lex.add(token.getToken());
                        lexPos.add(token.getLemma() + "_" + token.getPostag());
                    }
                }
                fileStats.close();
                pc++;
                if (pc%10==0) {
                    System.out.print(".");
                    if (pc%1000==0) {
                        System.out.println(pc);
                    }
                }
            }
            reader.close();
            mainStats.close();
            saveMultiset(new File(outputdir + "/lex.tsv"), lex);
            saveMultiset(new File(outputdir + "/lexpos.tsv"), lexPos);
            BufferedWriter writer = new BufferedWriter(new FileWriter(outputdir + "/sentilex.tsv"));
            for (Multiset.Entry<String> entry : lexPos.entrySet()) {
                String[] split = entry.getElement().split("_");
                SentixEntry sent = sentapi.getPolarity(split[0], split[1]);
                Set<String> labels = affectapi.getLabels(split[0], split[1]);
                writer.append(entry.getElement()).append("\t").append(String.valueOf(entry.getCount()));
                writer.append("\t");
                if (sent != null) {
                    writer.append(String.valueOf(sent.getPolarityScore()));
                } else {
                    writer.append(String.valueOf(0));
                }
                for (String al : affectLabels) {
                    if (labels == null || !labels.contains(al)) {
                        writer.append("\t0");
                    } else {
                        writer.append("\t1");
                    }
                }
                writer.newLine();
            }
            writer.close();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

}
