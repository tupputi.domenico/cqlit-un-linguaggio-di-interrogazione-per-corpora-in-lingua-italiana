/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.sentiment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.insert.Insert;

/**
 *
 * @author pierpaolo
 */
public class LexicalizeWordNetAffect {

    private static final Logger LOG = Logger.getLogger(LexicalizeWordNetAffect.class.getName());

    private static String getLine(File file, String id) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
        String line = null;
        boolean found = false;
        while (reader.ready()) {
            line = reader.readLine();
            if (line.contains("VALUES ('" + id + "'")) {
                found = true;
                break;
            }
        }
        reader.close();
        if (found) {
            return line;
        } else {
            return null;
        }
    }

    private static String getSet2String(Set<String> set) {
        StringBuilder sb = new StringBuilder();
        for (String v : set) {
            sb.append(v);
            sb.append(" ");
        }
        return sb.toString().trim();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            List<WNAffectEntry> listwne = new ArrayList<>();
            while (reader.ready()) {
                String[] split = reader.readLine().split("\\|");
                WNAffectEntry wne = new WNAffectEntry(split[0], split[1]);
                for (int i = 2; i < split.length; i++) {
                    wne.getLabels().add(split[i]);
                }
                listwne.add(wne);
            }
            reader.close();
            for (WNAffectEntry wnentry : listwne) {
                String line = getLine(new File(args[1]), wnentry.getSynset());
                if (line != null) {
                    try {
                        Statement stm = CCJSqlParserUtil.parse(line);
                        Insert ins = (Insert) stm;
                        String v = ins.getItemsList().toString().split(",")[1];
                        int start = v.indexOf("'");
                        int end = v.indexOf("'", start + 1);
                        String[] lemmas = v.substring(start + 1, end).split(" ");
                        for (String lemma : lemmas) {
                            wnentry.getLemmas().add(lemma);
                        }
                    } catch (JSQLParserException ex) {
                        Logger.getLogger(LexicalizeWordNetAffect.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    LOG.log(Level.WARNING, "No line for: {0}", wnentry.getSynset());
                }
            }
            BufferedWriter writer = new BufferedWriter(new FileWriter(args[2]));
            for (WNAffectEntry wnentry : listwne) {
                if (wnentry.getLemmas().size() > 0) {
                    writer.append(wnentry.getSynset()).append("\t").append(wnentry.getRelation());
                    writer.append("\t");
                    writer.append(getSet2String(wnentry.getLemmas()));
                    writer.append("\t");
                    writer.append(getSet2String(wnentry.getLabels()));
                    writer.newLine();
                }
            }
            writer.close();
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

}
