/*
 * Questo software è stato sviluppato dal gruppo di ricerca SWAP del Dipartimento di Informatica dell'Università degli Studi di Bari.
 * Tutti i diritti sul software appartengono esclusivamente al gruppo di ricerca SWAP.
 * Il software non può essere modificato e utilizzato per scopi di ricerca e/o industriali senza alcun permesso da parte del gruppo di ricerca SWAP.
 * Il software potrà essere utilizzato a scopi di ricerca scientifica previa autorizzazione o accordo scritto con il gruppo di ricerca SWAP.
 * 
 * Bari, Marzo 2014
 */
package di.uniba.it.nlpita;

import java.util.Set;
import org.json.simple.JSONObject;

/**
 * Contiene le informazioni su una singola parola (Token)
 *
 * @author Pierpaolo Basile
 */
public class Token {

    private String token;
    private String lemma;
    private String morphoInfo;
    private POSenum pos;
    private String postag;
    private String chunk;
    private int position;
    private int beginOffset = 0;
    private int endOffset = 0;
    private boolean stopWord = false;
    private boolean commonWord = false;
    private float polarity = 0;
    private String depInfo;
    private Set<String> emolabels;
    private String iob2 = "O";

    /**
     *
     * @return
     */
    public int getBeginOffset() {
        return beginOffset;
    }

    /**
     *
     * @param beginOffset
     */
    public void setBeginOffset(int beginOffset) {
        this.beginOffset = beginOffset;
    }

    /**
     *
     * @return
     */
    public int getEndOffset() {
        return endOffset;
    }

    /**
     *
     * @param endOffset
     */
    public void setEndOffset(int endOffset) {
        this.endOffset = endOffset;
    }

    public String getDepInfo() {
        return depInfo;
    }

    public void setDepInfo(String depInfo) {
        this.depInfo = depInfo;
    }

    /**
     * Crea una nuova istanza di parola
     */
    public Token() {
    }

    /**
     * Crea una nuova istanza di parola
     *
     * @param token La parola
     * @param lemma Il lemma
     * @param pos La posizione
     * @param postag Il pos-tag in formato stringa {@link String}
     * @param position La poszione
     */
    public Token(String token, String lemma, POSenum pos, String postag, int position) {
        this.token = token;
        this.lemma = lemma;
        this.pos = pos;
        this.postag = postag;
        this.position = position;
    }

    /**
     * Crea una nuova istanza di parola
     *
     * @param token La parola
     * @param lemma Il lemma
     * @param pos La posizione
     * @param postag Il pos-tag in formato stringa {@link String}
     * @param chunk Il chunk
     * @param position La poszione
     */
    public Token(String token, String lemma, POSenum pos, String postag, String chunk, int position) {
        this.token = token;
        this.lemma = lemma;
        this.pos = pos;
        this.postag = postag;
        this.position = position;
        this.chunk = chunk;
    }

    /**
     *
     * @return
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    public String getLemma() {
        return lemma;
    }

    /**
     *
     * @param lemma
     */
    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    /**
     *
     * @return
     */
    public POSenum getPos() {
        return pos;
    }

    /**
     *
     * @param pos
     */
    public void setPos(POSenum pos) {
        this.pos = pos;
    }

    /**
     *
     * @return
     */
    public int getPosition() {
        return position;
    }

    /**
     *
     * @param position
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     *
     * @return
     */
    public String getPostag() {
        return postag;
    }

    /**
     *
     * @param postag
     */
    public void setPostag(String postag) {
        this.postag = postag;
    }

    public String getChunk() {
        return chunk;
    }

    public void setChunk(String chunk) {
        this.chunk = chunk;
    }

    public boolean isStopWord() {
        return stopWord;
    }

    public void setStopWord(boolean stopWord) {
        this.stopWord = stopWord;
    }

    public boolean isCommonWord() {
        return commonWord;
    }

    public void setCommonWord(boolean commonWord) {
        this.commonWord = commonWord;
    }

    public String getMorphoInfo() {
        return morphoInfo;
    }

    public void setMorphoInfo(String morphoInfo) {
        this.morphoInfo = morphoInfo;
    }

    public float getPolarity() {
        return polarity;
    }

    public void setPolarity(float polarity) {
        this.polarity = polarity;
    }

    public Set<String> getEmolabels() {
        return emolabels;
    }

    public void setEmolabels(Set<String> emolabels) {
        this.emolabels = emolabels;
    }

    public String getIob2() {
        return iob2;
    }

    public void setIob2(String iob2) {
        this.iob2 = iob2;
    }

    @Override
    public String toString() {
        //return "Token{" + "token=" + token + ", lemma=" + lemma + ", pos=" + pos + ", postag=" + postag + ", chunk=" + chunk + ", position=" + position + ", beginOffset=" + beginOffset + ", endOffset=" + endOffset + '}';
        return token + "\t" + postag + "\t" + pos + "\t" + lemma + "\t" + morphoInfo + "\t" + chunk + "\t" + stopWord + "\t" + commonWord + "\t" + polarity + "\t" + (emolabels == null ? "UNK" : emolabels) + "\t" + iob2 + "\t" + depInfo;
    }

    public JSONObject toJSON() {
        JSONObject obj = new JSONObject();
        obj.put("token", token);
        obj.put("position", position);
        obj.put("beginOffset", beginOffset);
        obj.put("endOffset", endOffset);
        if (postag != null) {
            obj.put("postag", postag);
        }
        if (pos != null) {
            obj.put("pos", pos.name());
        }
        if (lemma != null) {
            obj.put("lemma", lemma);
        }
        if (morphoInfo != null) {
            obj.put("morpho", morphoInfo);
        }
        if (chunk != null) {
            obj.put("chunk", chunk);
        }
        obj.put("stopWord", stopWord);
        obj.put("commonWord", commonWord);
        obj.put("iob2", iob2);
        obj.put("depInfo", depInfo);
        return obj;
    }

}
