/*
 * Model.java
 *
 * Created on 2 aprile 2005, 13.09
 */
package di.uniba.it.nlpita.acopost;

/**
 *
 * @author  Basile Pierpaolo and Grieco Franco
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Model {

    private List<String> tags = new ArrayList<String>();      /* tags */

    private Map<String, Integer> taghash = new HashMap<String, Integer>();    /* lookup table tags[taghash{"tag"}-1]="tag */

    private double theta = 0.0;       /* standard deviation of unconditioned ML probs */

    private double[] tp = null;         /* smoothed transition probs */

    private int[][] count = new int[3][64];//           /* uni-, bi- and trigram counts */
    private int[] type = {0, 0, 0};        /* uni-, bi- and trigram type counts */

    private int[] token = {0, 0, 0};       /* uni-, bi- and trigram token counts */

    private double[] lambda = {0, 0, 0};   /* lambda_1 - _3 for trigram interpolation */

    private Map<String, Word> dictionary = new HashMap<String, Word>(); /* dictionary: string->array */

    private Trie lower_trie = new Trie(); /* suffix trie for all/lowercase words */

    private Trie upper_trie = new Trie(); /* suffix trie for uppercase words */

    private int lc_count = 0;
    private int uc_count = 0;
   

    /** Creates a new instance of Model */
    public Model() {
        //count = new int[3][2];
    }

    public int findTag(String t) {
        if (getTaghash().containsKey(t)) {

            return ((Integer) getTaghash().get(t)).intValue() - 1;
        } else {
            return -1;
        }
    }

    public int registerTag(String t) {

        int i = findTag(t);

        if (i < 0) {
            String newS = new String(t);
            getTags().add(newS);
            i = getTags().size();
            getTaghash().put(newS, new Integer(i));
        }

        return i;


    }

    /**
     * Getter for property count.
     * @return Value of property count.
     */
    public int[][] getCount() {
        return this.count;
    }

    /**
     * Setter for property count.
     * @param count New value of property count.
     */
    public void setCount(int[][] count) {
        this.count = count;
    }

    /**
     * Getter for property lambda.
     * @return Value of property lambda.
     */
    public double[] getLambda() {
        return this.lambda;
    }

    /**
     * Setter for property lambda.
     * @param lambda New value of property lambda.
     */
    public void setLambda(double[] lambda) {
        this.lambda = lambda;
    }

    /**
     * Getter for property lc_count.
     * @return Value of property lc_count.
     */
    public int getLc_count() {
        return lc_count;
    }

    /**
     * Setter for property lc_count.
     * @param lc_count New value of property lc_count.
     */
    public void setLc_count(int lc_count) {
        this.lc_count = lc_count;
    }

    /**
     * Getter for property lower_trie.
     * @return Value of property lower_trie.
     */
    public Trie getLower_trie() {
        return lower_trie;
    }

    /**
     * Setter for property lower_trie.
     * @param lower_trie New value of property lower_trie.
     */
    public void setLower_trie(Trie lower_trie) {
        this.lower_trie = lower_trie;
    }

   

  


    /**
     * Getter for property theta.
     * @return Value of property theta.
     */
    public double getTheta() {
        return theta;
    }

    /**
     * Setter for property theta.
     * @param theta New value of property theta.
     */
    public void setTheta(double theta) {
        this.theta = theta;
    }

    /**
     * Getter for property token.
     * @return Value of property token.
     */
    public int[] getToken() {
        return this.token;
    }

    /**
     * Setter for property token.
     * @param token New value of property token.
     */
    public void setToken(int[] token) {
        this.token = token;
    }

    /**
     * Getter for property tp.
     * @return Value of property tp.
     */
    public double[] getTp() {
        return tp;
    }

    /**
     * Setter for property tp.
     * @param tp New value of property tp.
     */
    public void setTp(double[] tp) {
        this.tp = tp;
    }

    /**
     * Getter for property type.
     * @return Value of property type.
     */
    public int[] getType() {
        return this.type;
    }

    /**
     * Setter for property type.
     * @param type New value of property type.
     */
    public void setType(int[] type) {
        this.type = type;
    }

    /**
     * Getter for property uc_count.
     * @return Value of property uc_count.
     */
    public int getUc_count() {
        return uc_count;
    }

    /**
     * Setter for property uc_count.
     * @param uc_count New value of property uc_count.
     */
    public void setUc_count(int uc_count) {
        this.uc_count = uc_count;
    }

    /**
     * Getter for property upper_trie.
     * @return Value of property upper_trie.
     */
    public Trie getUpper_trie() {
        return upper_trie;
    }

    /**
     * Setter for property upper_trie.
     * @param upper_trie New value of property upper_trie.
     */
    public void setUpper_trie(Trie upper_trie) {
        this.upper_trie = upper_trie;
    }

    /**
     * @return the tags
     */
    public List<String> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    /**
     * @return the taghash
     */
    public Map<String, Integer> getTaghash() {
        return taghash;
    }

    /**
     * @param taghash the taghash to set
     */
    public void setTaghash(Map<String, Integer> taghash) {
        this.taghash = taghash;
    }

    /**
     * @return the dictionary
     */
    public Map<String, Word> getDictionary() {
        return dictionary;
    }

    /**
     * @param dictionary the dictionary to set
     */
    public void setDictionary(Map<String, Word> dictionary) {
        this.dictionary = dictionary;
    }

  
}
