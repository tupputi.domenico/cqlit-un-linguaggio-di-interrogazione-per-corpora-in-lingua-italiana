/*
 * JavaT3.java
 *
 * Created on 2 aprile 2005, 13.17
 */
package di.uniba.it.nlpita.acopost;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Basile Pierpaolo and Grieco Franco
 */
/**
 * This class provides to POS-tag a document as a String, using HMM models To
 * training the t3 pos-tagger you have to set the dictionary file and the model
 * file using the method configure
 */
public class JavaT3 {

    private static final Logger LOG = Logger.getLogger(JavaT3.class.getName());

    public static int MAX_TOKEN = 100;
    /**
     * The welcome string
     */
    final String banner = "Trigram POS Tagger - Java Version - Universita' degli Studi di Bari";
    /**
     * An hastable contains each token found in the processed text
     */
    Map<String, String> table = null;
    /**
     * A globals object wich contain the setting for the t3 tagger
     */
    Globals g = new Globals();
    /**
     * The using model
     */
    Model m;
    /**
     * Holds value of property verbose.
     */
    private boolean verbose = false;

    /**
     * Creates a new instance of JavaT3
     */
    public JavaT3() {
    }

    void addWordToTrie(String s, Word wd) {

        String temp = "ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ";
        int uc = temp.indexOf(s.charAt(0));
        Trie tr;
        if (uc > -1) {
            tr = m.getUpper_trie();
        } else {
            tr = m.getLower_trie();
        }

        if (wd.getCount() > g.getRwt()) {
            return;
        }

        tr.addWordInfoToTrieNode(m, wd);

        char t;
        int i;
        int chCount = s.length() - 1;

        for (i = g.getMsl(); chCount >= 0 && i > 0; chCount--, i--) {
            byte c;
            if (g.isStics()) {
                c = s.getBytes()[chCount];
            } else {
                c = s.toLowerCase().getBytes()[chCount];
            }

            Trie daughter = tr.getDaughter(c);

            if (daughter == null) {
                daughter = new Trie(m, tr);
                if (uc > -1) {
                    m.setUc_count(m.getUc_count() + 1);
                } else {
                    m.setLc_count(m.getLc_count() + 1);
                }
                tr.addDaughter(c, daughter);
            }
            tr = daughter;
            tr.addWordInfoToTrieNode(m, wd);
        }
    }

    int ngramIndex(int n, int s, int t1, int t2, int t3) {

        switch (n) {
            case 0:
                return t1;
            case 1:
                return t1 * s + t2;
            case 2:
                return (t1 * s + t2) * s + t3;
        }

        return -1; /* for the compiler */

    }

    void readNgramFile() throws Exception {

        File f = new java.io.File(g.getMf());
        int lno, not;
        int[] t = {0, 0, 0};
        int i, size;
        String s;

        m.setTags(new java.util.ArrayList(64));
        m.getTags().add("*BOUNDARY*");
        m.setTaghash(new HashMap<String, Integer>());

        try {
            BufferedReader in = new BufferedReader(new FileReader(f));
            lno = 1;
            int tagCounter = 0;
            while ((s = in.readLine()) != null) {
                if (s.charAt(0) == '\t') {
                    continue;
                }

                String token = s.split("[\t]|[ ]")[0];

                tagCounter++;
                if (token == null) {
                    if (verbose) {
                        LOG.info("\t\t\t\tCan't find tag in " + g.getMf() + "line " + lno + "\n");
                    }
                    return;
                }
                m.registerTag(token);
                lno++;
            }

            not = m.getTags().size();
            size = 1;
            for (i = 0; i < 3; i++) {
                size = not * size;
                m.getCount()[i] = new int[size];
                m.getType()[i] = 0;
                m.getToken()[i] = 0;
            }

            /*Initialize count*/
            for (int ind1 = 0; ind1 < 3; ind1++) {
                for (int ind2 = 0; ind2 < m.getCount()[ind1].length; ind2++) {
                    m.getCount()[ind1][ind2] = 0;
                }
            }

            in.close();
            in = new BufferedReader(new FileReader(f));
            lno = 1;
            while ((s = in.readLine()) != null) {
                int cnt;
                for (i = 0; s.charAt(i) == '\t' || s.charAt(i) == ' '; i++) { /* nada */ }
                if (i > 2) {
                    if (verbose) {
                        LOG.log(Level.INFO, "\t\t\t\tToo many tabs in file {0} in line {1}", new Object[]{g.getMf(), lno});
                    }
                    return;
                }
                s = s.substring(i);
                String token = s.split("[\t]|[ ]")[0];

                if (token == null) {
                    if (verbose) {
                        LOG.log(Level.INFO, "\t\t\t\tCan''t find tag in file {0} in line {1}", new Object[]{g.getMf(), lno});
                    }
                }

                t[i] = m.findTag(token);
                if (t[i] < 0) {
                    if (verbose) {
                        LOG.log(Level.INFO, "\t\t\t\tUnknown tag {0} in file {1} in line {2}", new Object[]{token, g.getMf(), lno});
                    }
                }
                String count = s.split("[\t]|[ ]")[1];
                if (count == null) {
                    if (verbose) {
                        LOG.log(Level.INFO, "\t\t\t\tCan not find count in file {0} in line {1}", new Object[]{g.getMf(), lno});
                    }
                }

                cnt = new Integer(count);

                int ngi = ngramIndex(i, not, t[0], t[1], t[2]);

                m.getCount()[i][ngi] = cnt;
                m.getType()[i]++;
                m.getToken()[i] += cnt;
                lno++;
            }

            in.close();
        } catch (IOException ex) {
            if (verbose) {
                LOG.info("\t\t\t\tError to read from the model file " + g.getMf() + ":\n" + ex.getMessage());
            }
            throw (Exception) ex;
            //throw new Exception("\t\tError to read from the model file " + g.getMf() + ":\n" + ex.getMessage());
        }

        if (verbose) {
            LOG.info("\t\t\t\tRead " + m.getType()[0] + "/" + m.getToken()[0] + " unigram count");
        }
        if (verbose) {
            LOG.info("\t\t\t\tRead " + m.getType()[1] + "/" + m.getToken()[1] + " bigram count");
        }
        if (verbose) {
            LOG.info("\t\t\t\tRead " + m.getType()[0] + "/" + m.getToken()[2] + " trigram count");
        }

    }

    String registerString(String s) {

        String t = null;

        if (s == null) {
            return s;
        }
        if (table == null) {
            table = new HashMap<String, String>();
        } else {
            t = (String) table.get(s);
            if (t != null) {
                return t;
            }
        }

        table.put(s, s);
        return s;
    }

    void computeCountsForBoundary() {
        /* compute transition probs for artificial boundary tags */
        int not = m.getTags().size();
        int i, uni = 0, bi = 0, tri = 0, ows = 0, nos = 0;


        /* we don't start at zero because of the boundary tags */
        for (i = 1; i < not; i++) {
            int j, bx, xb, bx_ = 0;

            bx = xb = m.getCount()[0][ngramIndex(0, not, i, -1, -1)];
            for (j = 1; j < not; j++) {
                int k, bxy, xyb;

                bx -= m.getCount()[1][ngramIndex(1, not, j, i, -1)];
                xb -= m.getCount()[1][ngramIndex(1, not, i, j, -1)];

                bxy = xyb = m.getCount()[1][ngramIndex(1, not, i, j, -1)];
                for (k = 1; k < not; k++) {
                    bxy -= m.getCount()[2][ngramIndex(2, not, k, i, j)];
                    xyb -= m.getCount()[2][ngramIndex(2, not, i, j, k)];
                }
                bx_ += bxy;

                m.getCount()[2][ngramIndex(2, not, 0, i, j)] = bxy;
                m.getCount()[2][ngramIndex(2, not, i, j, 0)] = xyb;
                tri += bxy + xyb;
            }
            /* Boundary unigrams, two at the beginning, one at the end */
            uni += bx + bx + xb;
            /* For each start of a sentence, there must be tags t-2, t-1. */
            m.getCount()[1][ngramIndex(1, not, 0, 0, -1)] += bx;
            bi += bx;
            /* t-1, w1 */
            m.getCount()[1][ngramIndex(1, not, 0, i, -1)] = bx;
            bi += bx;
            /* wn, t+1 */
            m.getCount()[1][ngramIndex(1, not, i, 0, -1)] = xb;
            bi += xb;

            /* (t-2, t-1, w1) */
            m.getCount()[2][ngramIndex(2, not, 0, 0, i)] = bx;
            tri += bx;

            /*
             FIXME: really true?
             This compensates the lack of a continuation of the sentence.
             For bigrams, we assume that tag t+1 has a successor t+2.
             */
            m.getCount()[1][ngramIndex(1, not, 0, 0, -1)] += bx;
            bi += bx;
            /*
             FIXME: really true?
             Wrong again. There are no triples <0,0,0> of course.
             However, we added bigram counts for <0,0> for tag t+1
             (see above) and here we add corresponding (artificial)
             trigrams.
             */
            m.getCount()[2][ngramIndex(2, not, 0, 0, 0)] += bx;
            tri += bx;
            /*
             FIXME:
             Strictly speaking, the following is wrong. There's only one
             boundary tag at the end. But we need trigrams <x, 0, _>.
             See above.
             Below, the corresponding (real) bigram is added.
             */
            m.getCount()[2][ngramIndex(2, not, i, 0, 0)] = xb;
            tri += xb;

            /* This is for one-word sentences: t-1, w1, t+1 */
            j = bx - bx_;
            m.getCount()[2][ngramIndex(2, not, 0, i, 0)] = j;
            tri += j;
            ows += j;
        }

        /* TODO: check what to use */
        m.getCount()[0][ngramIndex(0, not, 0, -1, -1)] = uni; /* 0? uni? uni/3? */

        nos = uni / 3;
        m.getToken()[0] += 3 * nos;
        m.getToken()[1] = m.getToken()[0];
        m.getToken()[2] = m.getToken()[0];

        if (verbose) {
            LOG.info("\t\t\t\tModel generated from " + uni / 3 + " sentences (thereof " + ows + " one-word)");
        }
        if (verbose) {
            LOG.info("\t\t\t\tFound  " + uni + " unigram counts for the boundary tag");
        }
        if (verbose) {
            LOG.info("\t\t\t\tFound  " + bi + " bigram counts for the boundary tag");
        }
        if (verbose) {
            LOG.info("\t\t\t\tFound  " + tri + " trigram counts for the boundary tag");
        }

    }

    void enterRareWordTagCounts(String key, Word wd, int[] lowcount) {

        int not = m.getTags().size();
        int i;

        if (wd.getCount() > g.getRwt()) {
            return;
        }
        for (i = 0; i < not; i++) {
            lowcount[i] += wd.getTagcount()[i];
        }
    }

    //The hash_map2 function for the void enterRareWordTagCounts
    void hashMap2(Map<String, Word> ht, int[] lowcount) {
        Iterator<String> iterator = ht.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            enterRareWordTagCounts(key, ht.get(key), lowcount);
        }
    }

    //The hash_map1 function for the void addWordToTrie
    private void hashMap1(Map<String, Word> ht) {
        Iterator<String> iterator = ht.keySet().iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            addWordToTrie(key, ht.get(key));
        }
    }

    void computeTheta() {
        /* TODO: check whether to include 0 */
        final int START_AT_TAG = 1;
        int not = m.getTags().size();
        int[] lowcount = new int[not];
        double inv_not = 1.0 / (double) (not - START_AT_TAG), sum;
        int ttc, i;

        for (i = 0; i < not; i++) {
            lowcount[i] = 0;
        }
        hashMap2(m.getDictionary(), lowcount);
        ttc = 0;
        for (i = START_AT_TAG; i < not; i++) {
            ttc += lowcount[i];
        }
        sum = 0.0;
        for (i = START_AT_TAG; i < not; i++) {
            double x = (double) lowcount[i] / (double) ttc - inv_not;
            sum += x * x;
        }
        m.setTheta(Math.sqrt(sum / (double) (not - START_AT_TAG - 1)));
        if (verbose) {
            LOG.info("\t\t\t\tTheta = " + m.getTheta());
        }

    }

    void computeLambdas() {
        int i, sum = 0;
        int not = m.getTags().size();
        int[] li = {0, 0, 0};

        final int START_AT_TAG = 1;

        for (i = START_AT_TAG; i < not; i++) {
            int j;
            for (j = START_AT_TAG; j < not; j++) {
                int f12 = m.getCount()[1][ngramIndex(1, not, i, j, 0)] - 1;
                int f2 = m.getCount()[0][ngramIndex(0, not, j, 0, 0)] - 1;
                int k;
                for (k = START_AT_TAG; k < not; k++) {
                    int f123, f23, f3, b;
                    double[] q = {0.0, 0.0, 0.0};

                    f123 = m.getCount()[2][ngramIndex(2, not, i, j, k)] - 1;
                    if (f123 < 0) {
                        continue;
                    }

                    if (m.getToken()[0] > 1) {
                        f3 = m.getCount()[0][ngramIndex(0, not, k, 0, 0)] - 1;
                        q[2] = (double) f3 / (double) (m.getToken()[0] - 1);
                        if (f2 > 0) {
                            f23 = m.getCount()[1][ngramIndex(1, not, j, k, 0)] - 1;
                            q[1] = (double) f23 / (double) f2;
                            if (f12 > 0) {
                                q[0] = (double) f123 / (double) f12;
                            }
                        }
                    }
                    b = 0;
                    if (q[1] > q[b]) {
                        b = 1;
                    }
                    if (q[2] > q[b]) {
                        b = 2;
                    }
                    li[b] += f123 + 1;
                }
            }
        }
        for (i = 0; i < 3; i++) {
            sum += li[i];
        }
        /* TODO: check which lambda to use. */
        for (i = 0; i < 3; i++) {
            m.getLambda()[i] = (double) li[2 - i] / (double) sum;
        }

    }

    void computeTransitionProbs() {
        int not = m.getTags().size();
        /*
         FIXME
         inv_not is used for the empirical probability distributions
         if the denomiator is zero. Maybe we should simple set it
         to zero, but then \sum_0^n p(t_i | t_x, t_y) might be much
         smaller than one because one addend of the linear interpolation
         becomes zero for all tags:

         p(t_k | t_i, t_j) =
         l_1 \hat{P}(t_k) +            <--- zero if N=0 (very unlikely)
         l_2 \hat{P}(t_k | t_j) +      <--- zero if f(t_j)=0 (unlikely)
         l_3 \hat{P}(t_k | t_i, t_j)   <--- zero if f(t_i, t_j)=0 (likely)
         */

        double inv_not;
        if (g.isZuetp()) {
            inv_not = 0.0;
        } else {
            inv_not = 1.0 / (double) not;
        }
        int i;

        m.setTp(new double[not * not * not]);

        for (i = 0; i < not; i++) {
            int ft3 = m.getCount()[0][ngramIndex(0, not, i, -1, -1)];
            double pt3;
            if (m.getToken()[0] > 0) {
                pt3 = (double) ft3 / (double) m.getToken()[0];
            } else {
                pt3 = inv_not;
            }

            double l1pt3 = pt3 * m.getLambda()[0];
            int j;
            for (j = 0; j < not; j++) {
                int ft2t3 = m.getCount()[1][ngramIndex(1, not, j, i, -1)];
                int ft2 = m.getCount()[0][ngramIndex(0, not, j, -1, -1)];
                double pt3_t2 = ft2 > 0 ? (double) ft2t3 / (double) ft2 : inv_not;
                double l2pt3_t2 = pt3_t2 * m.getLambda()[1];
                int k;
                for (k = 0; k < not; k++) {
                    int index = ngramIndex(2, not, k, j, i);
                    int ft1t2t3 = m.getCount()[2][index];
                    int ft1t2 = m.getCount()[1][ngramIndex(1, not, k, j, -1)];
                    double pt3_t1t2 = ft1t2 > 0 ? (double) ft1t2t3 / (double) ft1t2 : inv_not;
                    double l3pt3_t1t2 = pt3_t1t2 * m.getLambda()[2];
                    m.getTp()[index] = l1pt3 + l2pt3_t2 + l3pt3_t1t2;

                    m.getTp()[index] = Math.log(m.getTp()[index]);
                }
            }
        }
        if (verbose) {
            LOG.info("\t\t\t\tComputed smoothed transition probabilities");
        }

    }

    void readDictionaryFile() throws Exception {

        try {
            File f = new File(g.getDf());
            String s;

            int lno, no_token = 0;
            int not = m.getTags().size();

            m.setDictionary(new HashMap<String, Word>());

            BufferedReader in = new BufferedReader(new FileReader(f));

            lno = 1;
            while ((s = in.readLine()) != null) {

                int cnt;
                Word wd;
                String[] words = s.split("[\t]|[ ]");
                if (words == null || words.length == 0) {
                    if (verbose) {
                        LOG.info("\t\t\t\tCan't find word in file " + g.getDf() + " in line " + lno);
                    }
                    continue;
                }

                s = words[0];

                s = registerString(s);
                //Hashtable dictionary = getDictionaryByString(m, s);
                Map<String, Word> dictionary = m.getDictionary();

                if (dictionary.containsKey(s)) {
                    wd = (Word) dictionary.get(s);
                } else {
                    wd = new Word(s, 0, not);
                    dictionary.put(s, wd);
                }

                for (int i = 1; i < words.length; i++) {
                    String tag = words[i];
                    int fti, ti = m.findTag(tag);
                    if (ti < 0) {
                        if (verbose) {
                            LOG.info("\t\t\t\tInvalid tag " + s + " in file " + g.getDf() + " in line " + lno);
                        }
                        continue;
                    }
                    i++;
                    if (i == words.length) {
                        if (verbose) {
                            LOG.info("\t\t\t\tCan non find tag count in file " + g.getDf() + " in line " + lno);
                        }
                        continue;
                    }
                    String count = words[i];

                    cnt = new Integer(count).intValue();
                    wd.setCount(wd.getCount() + cnt);
                    wd.getTagcount()[ti] = cnt;
                    int ngi = ngramIndex(0, not, ti, -1, -1);

                    fti = m.getCount()[0][ngi];

                    if (fti <= 0) {
                        if (verbose) {
                            LOG.info("\t\t\t\tInvalifd frequency count for " + s + " in file " + g.getDf() + " in line " + lno);
                        }
                        return;
                    }

                    double lpt = ((double) cnt / (double) fti);

                    wd.getLp()[ti] = Math.log(lpt);

                }
                no_token += wd.getCount();
                lno++;

            }
        } catch (Exception ex) {
            if (verbose) {
                LOG.info("\t\t\t\tError to read from the dictionary file  " + g.getDf() + ":\n " + ex.getMessage());
            }
            throw new Exception("Error to read from the dictionary file  " + g.getDf() + ":\n " + ex.toString());
        }

    }

    void countNodes(Trie tr, int[] s) {
        /* 0 leaves, 1 unary branching, 2 total */
        s[2]++;
        if (tr.getChildren() == 0) {
            s[0]++;
        } else if (tr.getChildren() == 1) {
            s[1]++;
            countNodes(tr.getUnarynext(), s);
        } else if (tr.getNext() != null) {
            int i;
            for (i = 0; i < 256; i++) {
                if (tr.getNext()[i] != null) {
                    countNodes(tr.getNext()[i], s);
                }
            }
        } else {
            if (verbose) {
                LOG.info("\t\t\t\ttr " + tr.toString() + " neither a leave, nor unary nor next\n");
            }
            return;
        }
    }

    void buildSuffixTrie() {

        m.setLower_trie(new Trie(m, null));
        m.setUpper_trie(new Trie(m, null));

        hashMap1(m.getDictionary());
        if (verbose) {
            LOG.info("\t\t\t\tBuilt suffix tries with " + m.getLc_count() + " lowercase and " + m.getUc_count() + " uppercase nodes");
        }

        int[] c = {0, 0, 0};
        countNodes(m.getLower_trie(), c);
        if (verbose) {
            LOG.info("\t\t\t\tLeaves/single/total LC: " + c[0] + " " + c[1] + " " + c[2]);
        }
        c[0] = 0;
        c[1] = 0;
        c[2] = 0;
        countNodes(m.getUpper_trie(), c);
        if (verbose) {
            LOG.info("\t\t\t\tLeaves/single/total UC: " + c[0] + " " + c[1] + " " + c[2]);
        }

    }

    void smoothSuffixProbs(Trie tr, Trie dad) {

        int not = m.getTags().size();
        double one_plus_theta = 1.0 + m.getTheta();
        int i;

        tr.setLp(new double[not]);

        for (i = 0; i < not; i++) {
            int tc = tr.getTagcount()[i];
            double p = 0.0;
            if (tc > 0) {
                p = (double) tc / (double) tr.getCount();
                /*
                 p is an estimate of P(t_i | w). However, for Viterbi
                 we need P(w | t_i).

                 Using Bayes' formula:
                 P(w | t_i) = P(w) * P(t_i | w)/P(t_i)
                 P(w) is constant:
                 P(w | t_i) = P(t_i | w)/P(t_i)
                 P(t_i) = f(t_i)/N and N is constant:
                 P(w | t_i) = P(t_i | w)/f(t_i)
                 That means that we have to divide by f(t_i).

                 f(t_i) cannot be zero because we already saw words with that
                 tag, e. g. this one.
                 */
                p /= m.getCount()[0][ngramIndex(0, not, i, -1, -1)];
            }
            if (dad != null) {
                p += m.getTheta() * dad.getLp()[i];
                p /= one_plus_theta;
            }
            tr.getLp()[i] = p;
        }

        if (tr.getChildren() == 1) {
            smoothSuffixProbs(tr.getUnarynext(), tr);
        } else if (tr.getNext() != null) {
            for (i = 0; i < 256; i++) {
                Trie son = tr.getNext()[i];
                if (son == null) {
                    continue;
                }
                smoothSuffixProbs(son, tr);
            }
        }

        for (i = 0; i < not; i++) {
            tr.getLp()[i] = Math.log(tr.getLp()[i]);
        }

    }

    /*private Map<String, Word> getDictionaryByString(Model m, String s) {
     return m.getDictionary();
     }*/
    void computeUnknownWordProbs() {
        if (m.getLower_trie() != null) {
            smoothSuffixProbs(m.getLower_trie(), null);
        }
        if (m.getUpper_trie() != null) {
            smoothSuffixProbs(m.getUpper_trie(), null);

        }
        if (verbose) {
            LOG.info("\t\t\t\tSuffix probabilities smoothing done [theta " + m.getTheta() + "]");
        }
    }

    Trie lookupSuffixInTrie(Trie tr, String s) {

        byte t;
        Trie d = tr;

        for (int index = s.length() - 1; index >= 0 && d != null; index--) {
            t = s.getBytes()[index];
            d = tr.getDaughter(t);
            if (d != null) {
                tr = d;
            }
        }

        return tr;
    }

    double[] getLexicalProbs(String s) {
        //Hashtable dictionary = getDictionaryByString(m, s);
        Map<String, Word> dictionary = m.getDictionary();
        Word w = (Word) dictionary.get(s);

        if (w != null) {
            return w.getLp();
        } else {
            String temp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\u00c0\u00c1\u00c2\u00c8\u00c9\u00cc\u00cd\u00d2\u00d3\u00d9\u00da";
            int uc = temp.indexOf(s.charAt(0));
            Trie tr;
            if (uc > -1) {
                tr = m.getUpper_trie();
            } else {
                tr = m.getLower_trie();
            }

            tr = lookupSuffixInTrie(tr, s);
            return tr.getLp();
        }
    }

    /*
     Extend viterbi() so that it can also work in multiple-tags
     mode.
     - Add parameter ``prob_t probs[]''.
     - if probs==NULL we are in best-sequence mode.
     - if probs!=NULL we are in multi-tag mode
     Probs is then a pre-allocated C-array of size not*wno.
     - The probability that word w_i has tag t_j is stored in
     probs[i*not+j].
     - Change declaration of a to ``prob_t a[wno][not][not]''
     - Adapt updating of nai and cai.
     - When finished, the traverse b[][][] and a[][][] and enter
     infos in probs.
     */
    void viterbi(List words, List tags) {
        int not = m.getTags().size();
        int wno = words.size();

        double[][][] a = new double[2][not][not];
        int[][][] b = new int[wno][not][not];

        double max_a;
        double b_a = -Double.MAX_VALUE;
        int b_i = 1, b_j = 1;
        int i, j, k, l, cai, nai = 0;

        for (j = 0; j < not; j++) {
            for (k = 0; k < not; k++) {
                a[0][j][k] = -Double.MAX_VALUE;
            }
        }

        a[0][0][0] = 0.0;
        max_a = 0.0;
        for (i = 0; i < wno; i++) {
            double max_a_new = -Double.MAX_VALUE;
            String w = (String) words.get(i);
            double[] lp = getLexicalProbs(w);

            cai = i % 2;
            nai = 1 - cai;

            /* clear next a column */
            for (j = 0; j < not; j++) {
                for (k = 0; k < not; k++) {
                    a[nai][j][k] = -Double.MAX_VALUE;
                }
            }

            /* TODO: precompute log(g->bw) */
            if (g.getBw() == 0) {
                max_a = -Double.MAX_VALUE;
            } else {
                max_a -= Math.log(g.getBw());
            }
            for (l = 0; l < not; l++) {
                if (lp[l] == -Double.MAX_VALUE) {
                    continue;
                }

                for (j = 0; j < not; j++) {
                    for (k = 0; k < not; k++) {
                        double newD;
                        if (a[cai][j][k] < max_a) {
                            continue;
                        }
                        newD = a[cai][j][k] + m.getTp()[ngramIndex(2, not, j, k, l)] + lp[l];

                        if (newD > a[nai][k][l]) {

                            a[nai][k][l] = newD;
                            b[i][k][l] = j;
                            if (newD > max_a_new) {
                                max_a_new = newD;
                            }
                        }
                    }
                }
                max_a = max_a_new;
            }
            max_a = max_a_new;

        }

        /* find highest prob in last column */
        for (i = 0; i < not; i++) {

            for (j = 0; j < not; j++) {
                /*
                 FIXME:
                 Should we use bigrams here? Cf. Brants (2000) page 1.
                 prob_t new=a[nai][i][j] + m->tp[ ngram_index(1, not, j, 0, -1) ];
                 */
                double newD = a[nai][i][j] + m.getTp()[ngramIndex(2, not, i, j, 0)];

                if (newD > b_a) {
                    b_a = newD;
                    b_i = i;
                    b_j = j;
                }
            }
        }


        /* best final state is (b_i, b_j) */
        ArrayList temp = new ArrayList();
        for (i = wno - 1; i >= 0; i--) {
            int tmp = b[i][b_i][b_j];
            temp.add(new Integer(b_j));

            b_j = b_i;
            b_i = tmp;
        }
        for (i = temp.size() - 1; i >= 0; i--) {
            tags.add(temp.get(i));
        }

    }

    //Anziche' scrivere su standard output restituisce la stringa in output
    String tagSentence(List words, List tags, String l) {

        String output = "";
        String t;
        int i;
        words.clear();
        tags.clear();
        String[] tokens = l.split("[\t]|[ ]");
        for (int ind = 0; ind < tokens.length; ind++) {
            if (tokens[ind].length() > 0) {
                words.add(tokens[ind]);
            }
        }
        viterbi(words, tags);
        for (i = 0; i < words.size(); i++) {
            int ti = ((Integer) tags.get(i)).intValue();
            String tn = (String) m.getTags().get(ti);
            String wd = (String) words.get(i);

            if (i > 0) {
                output += " ";
            }
            output += wd + " " + tn;

        }
        output += "\n";
        return output;
    }

    //Anziche' scrivere su standard output restituisce la stringa in output
    ArrayList tagTokens(List words, List tags, String l) {

        String output = "";
        String t;

        words.clear();
        tags.clear();

        ArrayList result = new ArrayList();
        int index = 0;
        String[] tokens = l.split("\n");
        while (index < tokens.length) {
            words.clear();
            tags.clear();
            while (index < tokens.length && !tokens[index].equals(".")) {
                words.add(tokens[index]);
                index++;
            }
            if (index < tokens.length && tokens[index].equals(".")) {
                words.add(tokens[index]);
                index++;
            }

            viterbi(words, tags);

            for (int i = 0; i < tags.size(); i++) {
                int ti = ((Integer) tags.get(i)).intValue();
                result.add(m.getTags().get(ti));
            }

        }

        return result;

    }

    //Anziche' scrivere su standard output restituisce la stringa in output
    public ArrayList tagTokens(List words, List tags, String[] tokens) {

        words.clear();
        tags.clear();

        ArrayList result = new ArrayList();

        int i = 0;
        int c = 0;
        while (i < tokens.length) {
            words.add(tokens[i]);
            i++;
            c++;
            if (c > MAX_TOKEN) {
                c = 0;
                viterbi(words, tags);
                for (int j = 0; j < tags.size(); j++) {
                    int ti = ((Integer) tags.get(j)).intValue();
                    result.add(m.getTags().get(ti));
                }
                words.clear();
                tags.clear();
                //Logger.getLogger(JavaT3.class.getName()).log(Level.WARNING, "Sentence out of bound, splitted.");
            }
        }
        if (c > 0) {
            c = 0;
            viterbi(words, tags);
            for (int j = 0; j < tags.size(); j++) {
                int ti = ((Integer) tags.get(j)).intValue();
                result.add(m.getTags().get(ti));
            }
            words.clear();
            tags.clear();
        }

        /*int index = 0;
         while (index < tokens.length) {
         words.clear();
         tags.clear();
         while (index < tokens.length && !tokens[index].equals(".")) {
         words.add(tokens[index]);
         index++;
         }
         if (index < tokens.length && tokens[index].equals(".")) {
         words.add(tokens[index]);
         index++;
         }


         viterbi(words, tags);

         for (int i = 0; i < tags.size(); i++) {
         int ti = ((Integer) tags.get(i)).intValue();
         result.add(m.getTags().get(ti));
         }


         }*/
        return result;

    }

    //Anziche' scrivere su standard output restituisce la stringa in output
    /**
     * This method POS-tag the given tokenized string
     *
     * @param input The text to be POS-tagged
     * @return The result is an array list of string wich contains in order for
     * each token in the input text the POS tag
     */
    public ArrayList taggingTokenized(String input) {
        if (input == null) {
            return null;
        }

        if (m == null) {
            if (verbose) {
                LOG.info("\t\t\t\tNo model found");
            }
            return null;
        }
        ArrayList words = new ArrayList(128), tags = new ArrayList(128);
        ArrayList result = tagTokens(words, tags, input);

        words.clear();
        tags.clear();

        return result;

    }

    /**
     * This method POS-tag the given string
     *
     * @param input The text to be POS-tagged
     * @return The result is a string wich contains the input in wich each word
     * is followed by the POS tag
     */
    public String tagging(String input) {
        if (input == null) {
            return null;
        }

        if (m == null) {
            if (verbose) {
                LOG.info("\t\t\t\tNo model found");
            }
            return null;
        }
        ArrayList words = new ArrayList(128), tags = new ArrayList(128);
        String output = "";
        String[] lines = input.split(" . ");
        String l;

        for (int i = 0; i < lines.length; i++) {
            l = lines[i];
            output += tagSentence(words, tags, l);

        }

        words.clear();
        tags.clear();
        return output;
    }

    private void initMy(String ngram, String dictionary) throws Exception {

        try {
            m = new Model();
            g = new Globals();

            g.setMf(ngram);
            g.setDf(dictionary);

            LOG.info("[JAVA T3 POS-TAG] Init " + banner);
            LOG.info("\t[JAVA T3 POS-TAG] use the following NGram file: " + g.getMf());
            LOG.info("\t[JAVA T3 POS-TAG] use the following Lexicon file: " + g.getDf());

            if (verbose) {
                LOG.info("\t\t\t[JAVA T3 POS-TAG] I'm reading the ngram file from " + g.getMf() + "...");
            }
            readNgramFile();

            if (verbose) {
                LOG.info("\t\t\t[JAVA T3 POS-TAG] Compute counts for boundary...");
            }
            computeCountsForBoundary();

            if (g.getLambda()[0] < 0.0) {
                if (verbose) {
                    LOG.info("\t\t\t[JAVA T3 POS-TAG] Compute lambdas...");
                }
                computeLambdas();
            } else {
                m.setLambda(g.getLambda());
            }

            if (verbose) {
                LOG.info("\t\t\t[JAVA T3 POS-TAG] Compute transition probs...");
            }
            computeTransitionProbs();

            if (verbose) {
                LOG.info("\t\t\t[JAVA T3 POS-TAG] I'm reading the dictionary file from " + g.getDf() + "...");
            }
            readDictionaryFile();

            if (g.getTheta() < 0.0) {
                if (verbose) {
                    LOG.info("\t\t\t[JAVA T3 POS-TAG] Compute theta...");
                }
                computeTheta();
            } else {
                m.setTheta(g.getTheta());
            }

            if (verbose) {
                LOG.info("\t\t\t[JAVA T3 POS-TAG] I'm building the suffix trie...");
            }
            buildSuffixTrie();

            if (verbose) {
                LOG.info("\t\t\tCompute unknown word probs...");
            }
            computeUnknownWordProbs();
        } catch (Exception ex) {
            throw ex;
        }

    }

    /**
     * This method allows to load model file and dictionary file (lexicon) to
     * initialize the pos-tagger
     *
     * @param model The name of the model file to use
     * @param dictionary The name of the lexicon file to use
     * @throws Exception Throw Exception
     */
    public void configure(String model, String dictionary) throws Exception {
        try {
            initMy(model, dictionary);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Setter for the lambda vector (lambda1, lambda2, lambda3)
     *
     * @param l1 Value for lambda1
     * @param l2 Value for lambda2
     * @param l3 Value for lambda3
     */
    public void setLambda(double l1, double l2, double l3) {
        double[] tmp = {l1, l2, l3};
        g.setLambda(tmp);
    }

    /**
     * Setter for the Beam Width property
     *
     * @param bw The new beam width
     */
    public void setBeamWidth(int bw) {
        g.setBw(bw);
    }

    /**
     * Setter for the maximum suffix length
     *
     * @param msl The new maximum suffix length
     */
    public void setMaximumSuffixLength(int msl) {
        g.setMsl(msl);
    }

    /**
     * Setter for the Rare Word Threshold
     *
     * @param rwt New rare word threshold
     */
    public void setRareWordThreshold(int rwt) {
        g.setRwt(rwt);
    }

    /**
     * Setter for Theta to use for Suffix Backoff
     *
     * @param theta New value for theta
     */
    public void setThetaForSuffixBackoff(double theta) {
        g.setTheta(theta);
    }

    /**
     * Setter for Case Sensitive Suffix Tries mode
     *
     * @param stcs put it to true if you want to have a case sensitive suffix
     * tries
     */
    public void setCaseSensitiveSuffixTries(boolean stcs) {
        g.setStcs(stcs);
    }

    /**
     * Setter for Case Sensitive mode when branching in suffix tries
     *
     * @param stics case sensitive mode when branching in suffix tries
     */
    public void setCaseSensitiveWhenBranchingInSuffixTries(boolean stics) {
        g.setStics(stics);
    }

    /**
     * Setter for the Zero Empirical Transition Probabilities mode
     *
     * @param zuetp Zero Empirical Transition Probabilities
     */
    public void zeroEmpiricalTransitionProbsIfUndefined(boolean zuetp) {
        g.setZuetp(zuetp);
    }

    /**
     * Getter for property verbose.
     *
     * @return Value of property verbose.
     */
    public boolean isVerbose() {
        return this.verbose;
    }

    /**
     * Setter for property verbose.
     *
     * @param verbose New value of property verbose.
     */
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
