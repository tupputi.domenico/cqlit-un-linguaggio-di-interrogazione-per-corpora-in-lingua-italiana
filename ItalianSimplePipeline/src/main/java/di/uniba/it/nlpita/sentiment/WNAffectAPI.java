/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.sentiment;

import di.uniba.it.nlpita.POSenum;
import di.uniba.it.nlpita.Token;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class WNAffectAPI {

    private Map<String, WNAffectEntry> map;

    private Map<String, Set<String>> lexMap;

    private Set<String> labelsSet = new TreeSet<>();

    private static final Logger LOG = Logger.getLogger(WNAffectAPI.class.getName());

    public WNAffectAPI() {
    }

    public void init(File lexAffectFile) throws IOException {
        LOG.log(Level.INFO, "Init WN-Affect API: {0}", lexAffectFile.getAbsolutePath());
        map = new HashMap<>();
        lexMap = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(lexAffectFile));
        while (reader.ready()) {
            String[] split = reader.readLine().split("\\t");
            WNAffectEntry entry = new WNAffectEntry(split[0], split[1]);
            String[] lemmas = split[2].split(" ");
            entry.getLemmas().addAll(Arrays.asList(lemmas));
            String[] labels = split[3].split(" ");
            entry.getLabels().addAll(Arrays.asList(labels));
            map.put(entry.getSynset(), entry);
            for (String lemma : lemmas) {
                String key = lemma + "#" + split[0].substring(0, 1);
                Set<String> set = lexMap.get(key);
                if (set == null) {
                    set = new HashSet<>();
                    lexMap.put(key, set);
                }
                List<String> labelList = Arrays.asList(labels);
                set.addAll(labelList);
                labelsSet.addAll(labelList);
            }
        }
        reader.close();
    }

    public Set<String> getLabels() {
        return labelsSet;
    }

    public Map<String, Integer> getAffectMap(List<Token> tokens) {
        Map<String, Integer> affectMap = new TreeMap<>();
        for (Token token : tokens) {
            if (token.getPos() != POSenum.OTHER) {
                Set<String> labels = getLabels(token.getLemma(), token.getPos());
                if (labels != null) {
                    token.setEmolabels(labels);
                    for (String label : labels) {
                        Integer v = affectMap.get(label);
                        if (v == null) {
                            affectMap.put(label, 1);
                        } else {
                            affectMap.put(label, v + 1);
                        }
                    }
                }
            }
        }
        return affectMap;
    }

    public Map<String, Integer> getTextAffectMap(List<List<Token>> text) {
        Map<String, Integer> affectMap = new TreeMap<>();
        for (List<Token> sentence : text) {
            for (Token token : sentence) {
                if (token.getPos() != POSenum.OTHER) {
                    Set<String> labels = getLabels(token.getLemma(), token.getPos());
                    if (labels != null) {
                        token.setEmolabels(labels);
                        for (String label : labels) {
                            Integer v = affectMap.get(label);
                            if (v == null) {
                                affectMap.put(label, 1);
                            } else {
                                affectMap.put(label, v + 1);
                            }
                        }
                    }
                }
            }
        }
        return affectMap;
    }

    public Set<String> getLabels(String lemma, POSenum pos) {
        String key = lemma + "#" + generatePosString(pos);
        return lexMap.get(key);
    }

    public Set<String> getLabels(String lemma, String pos) {
        POSenum postag = null;
        if (pos.startsWith("S")) {
            postag = POSenum.NOUN;
        } else if (pos.startsWith("V")) {
            postag = POSenum.VERB;
        } else if (pos.startsWith("A") || pos.startsWith("NO")) {
            postag = POSenum.ADJ;
        } else if (pos.startsWith("B")) {
            postag = POSenum.ADV;
        } else {
            postag = POSenum.OTHER;
        }
        String key = lemma + "#" + generatePosString(postag);
        return lexMap.get(key);
    }

    private String generatePosString(POSenum pos) {
        switch (pos) {
            case NOUN:
                return "n";
            case ADJ:
                return "a";
            case VERB:
                return "v";
            case ADV:
                return "r";
            default:
                return pos.name();
        }
    }

}
