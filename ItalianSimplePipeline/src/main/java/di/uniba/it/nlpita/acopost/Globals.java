/*
 * Globals.java
 *
 * Created on 2 aprile 2005, 12.56
 */
package di.uniba.it.nlpita.acopost;

/**
 *
 * @author  Basile Pierpaolo and Grieco Franco
 */
public class Globals {

    private String mf = null;     /* model file name */

    private String df = null;     /* dictionary file name */

    private int bw = 0;       /* beam width */

    private int rwt = 1;      /* rare word threshold */

    private int msl = 10;      /* max. suffix length */

    private boolean stcs = true;     /* use one or two (case-sensitive) suffix trees */

    private boolean stics = true;    /* case sensitive internal in suffix trie */

    private boolean zuetp = false;    /* zero undefined empirical transition probs */

    private double theta = -1.0; /* suffix backoff weight */

    private double lambda[] = {-1.0, -1.0, -1.0}; /* transition probs smoothing weights */


    /** Creates a new instance of Globals */
    public Globals() {
    }

    /**
     * Getter for property bw.
     * @return Value of property bw.
     */
    public int getBw() {
        return bw;
    }

    /**
     * Setter for property bw.
     * @param bw New value of property bw.
     */
    public void setBw(int bw) {
        this.bw = bw;
    }

    /**
     * Getter for property df.
     * @return Value of property df.
     */
    public java.lang.String getDf() {
        return df;
    }

    /**
     * Setter for property df.
     * @param df New value of property df.
     */
    public void setDf(java.lang.String df) {
        this.df = df;
    }

    /**
     * Getter for property lambda.
     * @return Value of property lambda.
     */
    public double[] getLambda() {
        return this.lambda;
    }

    /**
     * Setter for property lambda.
     * @param lambda New value of property lambda.
     */
    public void setLambda(double[] lambda) {
        this.lambda = lambda;
    }

    /**
     * Getter for property mf.
     * @return Value of property mf.
     */
    public java.lang.String getMf() {
        return mf;
    }

    /**
     * Setter for property mf.
     * @param mf New value of property mf.
     */
    public void setMf(java.lang.String mf) {
        this.mf = mf;
    }

    /**
     * Getter for property msl.
     * @return Value of property msl.
     */
    public int getMsl() {
        return msl;
    }

    /**
     * Setter for property msl.
     * @param msl New value of property msl.
     */
    public void setMsl(int msl) {
        this.msl = msl;
    }

    /**
     * Getter for property rwt.
     * @return Value of property rwt.
     */
    public int getRwt() {
        return rwt;
    }

    /**
     * Setter for property rwt.
     * @param rwt New value of property rwt.
     */
    public void setRwt(int rwt) {
        this.rwt = rwt;
    }

    /**
     * Getter for property stcs.
     * @return Value of property stcs.
     */
    public boolean isStcs() {
        return stcs;
    }

    /**
     * Setter for property stcs.
     * @param stcs New value of property stcs.
     */
    public void setStcs(boolean stcs) {
        this.stcs = stcs;
    }

    /**
     * Getter for property stics.
     * @return Value of property stics.
     */
    public boolean isStics() {
        return stics;
    }

    /**
     * Setter for property stics.
     * @param stics New value of property stics.
     */
    public void setStics(boolean stics) {
        this.stics = stics;
    }

    /**
     * Getter for property theta.
     * @return Value of property theta.
     */
    public double getTheta() {
        return theta;
    }

    /**
     * Setter for property theta.
     * @param theta New value of property theta.
     */
    public void setTheta(double theta) {
        this.theta = theta;
    }

    /**
     * Getter for property zuetp.
     * @return Value of property zuetp.
     */
    public boolean isZuetp() {
        return zuetp;
    }

    /**
     * Setter for property zuetp.
     * @param zuetp New value of property zuetp.
     */
    public void setZuetp(boolean zuetp) {
        this.zuetp = zuetp;
    }
}
