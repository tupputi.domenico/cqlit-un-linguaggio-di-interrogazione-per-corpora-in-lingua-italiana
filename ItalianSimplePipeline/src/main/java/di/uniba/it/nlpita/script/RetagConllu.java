/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.MorphoEntry;
import di.uniba.it.nlpita.POSenum;
import di.uniba.it.nlpita.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class RetagConllu {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            String line;
            List<String> tokens = new ArrayList<>();
            List<String> rel = new ArrayList<>();
            List<String> pos = new ArrayList<>();
            Set<String> ambSet = new HashSet<>();
            int modIdx = 0;
            while (reader.ready()) {
                line = reader.readLine();
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.length() > 0) {
                    String[] split = line.split("\\t");
                    if (split[0].matches("[0-9]+-[0-9]+")) {
                        /*String[] spanStr = split[0].split("-");
                        tokens.add(split[1]);
                        int up = Integer.parseInt(spanStr[1]);
                        int down = Integer.parseInt(spanStr[0]);
                        int ov = up - down + 1;
                        List<CandidateRelation> candidate = new ArrayList<>();
                        for (int k = 0; k < ov; k++) {
                            line = reader.readLine();
                            String[] split1 = line.split("\\t");
                            int target = Integer.parseInt(split1[6]);
                            if (target > up || target < down) {
                                candidate.add(new CandidateRelation(split1[7], split1[6]));
                            }
                        }
                        if (candidate.size() > 1) {
                            String ar = "";
                            for (CandidateRelation cr : candidate) {
                                ar += cr.getRel() + "_";
                            }
                            ambSet.add(ar);
                        }
                        Collections.sort(candidate);
                        modIdx += (up - down);
                        int newIdx = Integer.parseInt(candidate.get(candidate.size() - 1).getPos()) - modIdx;
                        pos.add(String.valueOf(newIdx));
                        rel.add(candidate.get(candidate.size() - 1).getRel());*/
                    } else {
                        tokens.add(split[1]);
                        int newIdx = Integer.parseInt(split[6]) - modIdx;
                        pos.add(String.valueOf(newIdx));
                        rel.add(split[7]);
                    }
                } else if (tokens.size() > 0) {
                    String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                    MorphoEntry[] morphoEntry = instance.lemmatize(tokens.toArray(new String[tokens.size()]), postags);
                    for (int i = 0; i < tokens.size(); i++) {
                        writer.append(String.valueOf(i + 1)).append("\t");
                        writer.append(tokens.get(i)).append("\t");
                        writer.append(morphoEntry[i].getLemma()).append("\t");
                        //writer.append(Utils.getPos(postags[i]).name()).append("\t");
                        writer.append(postags[i].substring(0, 1)).append("\t");
                        writer.append(postags[i]).append("\t");
                        String[] split = morphoEntry[i].getMorphoInfo().split(":");
                        if (split.length > 1) {
                            String[] f = split[1].split("[+]");
                            for (int j = 0; j < f.length; j++) {
                                writer.append(f[j]);
                                if (j < f.length - 1) {
                                    writer.append("|");
                                }
                            }
                        } else {
                            writer.append("_");
                        }
                        writer.append("\t");
                        writer.append(pos.get(i)).append("\t");
                        writer.append(rel.get(i)).append("\t");
                        writer.append("_\t_");
                        writer.newLine();
                    }
                    writer.newLine();
                    tokens.clear();
                    rel.clear();
                    pos.clear();
                    modIdx = 0;
                }
            }
            reader.close();
            if (tokens.size() > 0) {
                String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                MorphoEntry[] morphoEntry = instance.lemmatize(tokens.toArray(new String[tokens.size()]), postags);
                for (int i = 0; i < tokens.size(); i++) {
                    writer.append(String.valueOf(i + 1)).append("\t");
                    writer.append(tokens.get(i)).append("\t");
                    writer.append(morphoEntry[i].getLemma()).append("\t");
                    writer.append(Utils.getPos(postags[i]).name()).append("\t");
                    writer.append(postags[i]).append("\t");
                    String[] split = morphoEntry[i].getMorphoInfo().split(":");
                    if (split.length > 1) {
                        String[] f = split[1].split("[+]");
                        for (int j = 0; j < f.length; j++) {
                            writer.append(f[j]);
                            if (j < f.length - 1) {
                                writer.append("|");
                            }
                        }
                    } else {
                        writer.append("_");
                    }
                    writer.append("\t");
                    writer.append(pos.get(i)).append("\t");
                    writer.append(rel.get(i)).append("\t");
                    writer.append("_\t_");
                    writer.newLine();
                }
                writer.newLine();
            }
            writer.close();
            System.out.println(ambSet);
        } catch (Exception ex) {
            Logger.getLogger(RetagConllu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static class CandidateRelation implements Comparable<CandidateRelation> {
        
        private final String rel;
        
        private final String pos;
        
        public CandidateRelation(String rel, String pos) {
            this.rel = rel;
            this.pos = pos;
        }
        
        @Override
        public int hashCode() {
            int hash = 7;
            hash = 11 * hash + Objects.hashCode(this.rel);
            hash = 11 * hash + Objects.hashCode(this.pos);
            return hash;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CandidateRelation other = (CandidateRelation) obj;
            if (!Objects.equals(this.rel, other.rel)) {
                return false;
            }
            if (!Objects.equals(this.pos, other.pos)) {
                return false;
            }
            return true;
        }
        
        public String getRel() {
            return rel;
        }
        
        public String getPos() {
            return pos;
        }
        
        @Override
        public int compareTo(CandidateRelation o) {
            if (this.getRel().equalsIgnoreCase("root")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("det") && o.getRel().equalsIgnoreCase("exp")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("det") && o.getRel().equalsIgnoreCase("mwe")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("dobj") && o.getRel().equalsIgnoreCase("advcl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("mwe") && o.getRel().equalsIgnoreCase("expl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("dobj") && o.getRel().equalsIgnoreCase("aux")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("det") && o.getRel().equalsIgnoreCase("mark")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("advmod") && o.getRel().equalsIgnoreCase("acl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("advmod") && o.getRel().equalsIgnoreCase("auxpass")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("advmod") && o.getRel().equalsIgnoreCase("advcl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("aux") && o.getRel().equalsIgnoreCase("expl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("advmod") && o.getRel().equalsIgnoreCase("aux")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("advmod") && o.getRel().equalsIgnoreCase("expl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("acl") && o.getRel().equalsIgnoreCase("expl")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("dobj") && o.getRel().equalsIgnoreCase("iobj")) {
                return 1;
            } else if (this.getRel().equalsIgnoreCase("csubj") && o.getRel().equalsIgnoreCase("expl")) {
                return 1;
            } else {
                return 0;
            }
        }
        
        @Override
        public String toString() {
            return "CandidateRelation{" + "rel=" + rel + ", pos=" + pos + '}';
        }
        
    }
    
}
