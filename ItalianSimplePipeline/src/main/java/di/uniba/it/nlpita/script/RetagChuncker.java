/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import di.uniba.it.nlpita.ItalianTextProcessing;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class RetagChuncker {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            String line;
            List<String> tokens = new ArrayList<>();
            List<String> chunks = new ArrayList<>();
            while (reader.ready()) {
                line = reader.readLine();
                if (line.length() > 0) {
                    String[] split = line.split("\\s+");
                    tokens.add(split[0]);
                    chunks.add(split[2]);
                } else if (tokens.size() > 0) {
                    String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                    for (int i = 0; i < tokens.size(); i++) {
                        writer.append(tokens.get(i)).append(" ");
                        writer.append(postags[i]).append(" ");
                        writer.append(chunks.get(i));
                        writer.newLine();
                    }
                    writer.newLine();
                    tokens.clear();
                    chunks.clear();
                }
            }
            reader.close();
            if (tokens.size() > 0) {
                String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                for (int i = 0; i < tokens.size(); i++) {
                    writer.append(tokens.get(i)).append(" ");
                    writer.append(postags[i]).append(" ");
                    writer.append(chunks.get(i));
                    writer.newLine();
                }
                writer.newLine();
            }
            writer.close();
        } catch (Exception ex) {
            Logger.getLogger(RetagChuncker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
