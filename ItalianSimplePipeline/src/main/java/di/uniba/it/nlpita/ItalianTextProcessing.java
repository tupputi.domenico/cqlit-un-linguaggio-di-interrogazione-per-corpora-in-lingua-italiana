/*
 * Questo software è stato sviluppato dal gruppo di ricerca SWAP del Dipartimento di Informatica dell'Università degli Studi di Bari.
 * Tutti i diritti sul software appartengono esclusivamente al gruppo di ricerca SWAP.
 * Il software non può essere modificato e utilizzato per scopi di ricerca e/o industriali senza alcun permesso da parte del gruppo di ricerca SWAP.
 * Il software potrà essere utilizzato a scopi di ricerca scientifica previa autorizzazione o accordo scritto con il gruppo di ricerca SWAP.
 * 
 * Bari, Marzo 2014
 */
package di.uniba.it.nlpita;

import di.uniba.it.nlpita.acopost.JavaT3;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.maltparser.concurrent.ConcurrentMaltParserModel;
import org.maltparser.concurrent.ConcurrentMaltParserService;

/**
 * Implementa la pipeline di NLP
 *
 * @author pierpaolo
 */
public class ItalianTextProcessing {

    private static ItalianTextProcessing instance;

    private final String opennlpDir;

    private boolean init = false;

    private SentenceDetectorME sentenceDetector;

    private Tokenizer tokenizer;

    private POSTaggerME tagger;

    private ChunkerME chunker;

    private ItalianLemmatizer lemmatizer;

    private ConcurrentMaltParserModel maltModel = null;

    private JavaT3 t3;

    /**
     * Restituisce una nuova istanza della pipeline di NLP
     *
     * @param opennlpdir La pipeline di NLP
     * @return
     */
    public synchronized static ItalianTextProcessing getInstance(String opennlpdir) {
        if (instance == null) {
            instance = new ItalianTextProcessing(opennlpdir);
        }
        return instance;
    }

    /**
     * Individua le frasi in un testo
     *
     * @param text Il testo
     * @return L'array di frasi
     * @throws Exception
     */
    public String[] sentenceDetection(String text) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return sentenceDetector.sentDetect(text);
    }

    /**
     * Tokenizza un testo
     *
     * @param text Il testo
     * @return L'array di token
     * @throws Exception
     */
    public String[] tokenize(String text) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return tokenizer.tokenize(text);
    }

    /**
     * Effettua il pos-tag di un array di parole
     *
     * @param tokens Le parole
     * @return L'array di pos-tag
     * @throws Exception
     */
    public String[] postag(String[] tokens) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return tagger.tag(tokens);
    }

    /**
     * Effettua il pos-tag di un array di parole utilizzando Acopost-pos-tagger
     *
     * @param tokens Le parole
     * @return L'array di pos-tag
     * @throws Exception
     */
    public String[] postagWithAcopost(String[] tokens) throws Exception {
        if (t3 == null) {
            t3 = new JavaT3();
            t3.configure(opennlpDir + "/t3/model_it", opennlpDir + "/t3/lex_it");
        }
        List words = new ArrayList<>();
        List tags = new ArrayList<>();
        ArrayList postags = t3.tagTokens(words, tags, tokens);
        String[] r = new String[postags.size()];
        for (int i = 0; i < postags.size(); i++) {
            r[i] = postags.get(i).toString();
        }
        return r;
    }

    /**
     * Esegue il chunking di un array di token
     *
     * @param tokens L'array di token
     * @param posTags L'array di pos-tag
     * @return L'array dei chunks estratti
     * @throws Exception
     */
    public String[] chunk(String[] tokens, String[] posTags) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return chunker.chunk(tokens, posTags);
    }

    /**
     * Effettua la lemmatizzazione di un array di token
     *
     * @param tokens Le parole
     * @param postag I pos-tag delle parole
     * @return L'array di lemmi
     * @throws Exception
     */
    public MorphoEntry[] lemmatize(String[] tokens, String[] postag) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return lemmatizer.lemmatize(tokens, postag);
    }

    public String[] parse(String[] tokens, String[] postags, MorphoEntry[] morphoEntries) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        String[] outputTokens = maltModel.parseTokens(Utils.getConll(tokens, postags, morphoEntries));
        String[] results = new String[outputTokens.length];
        for (int i = 0; i < outputTokens.length; i++) {
            String[] f = outputTokens[i].split("\\t");
            results[i] = f[6] + "_" + f[7];
        }
        return results;
    }

    public Map<String, Span[]> ner(String[] tokens, String[] postags) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        return nerTagger.ner(tokens, postags);
    }

    private void initSentencDetector() throws Exception {
        InputStream modelIn = new FileInputStream(opennlpDir + "/it-sent.bin");
        SentenceModel model = new SentenceModel(modelIn);
        sentenceDetector = new SentenceDetectorME(model);
    }

    private void initTokenizer() throws Exception {
        InputStream modelIn = new FileInputStream(opennlpDir + "/it-token.bin");
        TokenizerModel model = new TokenizerModel(modelIn);
        tokenizer = new TokenizerME(model);
    }

    private void initPosTagger() throws Exception {
        InputStream modelIn = new FileInputStream(opennlpDir + "/it-pos-maxent.bin");
        POSModel model = new POSModel(modelIn);
        tagger = new POSTaggerME(model);
    }

    private void initLemmatizer() throws Exception {
        lemmatizer = new ItalianLemmatizer(new File(opennlpDir + "/morph-it_048-utf8.gz"), new File(opennlpDir + "/tanl_morph-it"));
    }

    private void initChunker() throws Exception {
        InputStream modelIn = new FileInputStream(opennlpDir + "/chunk_it_all.tanl.bin");
        ChunkerModel model = new ChunkerModel(modelIn);
        chunker = new ChunkerME(model);
    }

    private void initParser() throws Exception {
        //URL italianModelURL = new File(opennlpDir + "/parsing/tut.mco").toURI().toURL();
        URL italianModelURL = new File(opennlpDir + "/parsing/ud_it.mco").toURI().toURL();
        maltModel = ConcurrentMaltParserService.initializeParserModel(italianModelURL);
    }

    private NERTagger nerTagger;

    private void initNER() throws Exception {
        nerTagger = new NERTagger();
        nerTagger.addNameFinder("LOC", new File(opennlpDir + "/ner/icab.train.opennlp.LOC.model"));
        nerTagger.addNameFinder("PER", new File(opennlpDir + "/ner/icab.train.opennlp.PER.model"));
        nerTagger.addNameFinder("GPE", new File(opennlpDir + "/ner/icab.train.opennlp.GPE.model"));
        nerTagger.addNameFinder("ORG", new File(opennlpDir + "/ner/icab.train.opennlp.ORG.model"));
    }

    private Set<String> stopWord;

    private Set<String> commonWord;

    private void init() throws Exception {
        initSentencDetector();
        initTokenizer();
        initPosTagger();
        initLemmatizer();
        initChunker();
        initNER();
        initParser();
        stopWord = Utils.loadFileInSet(new File(opennlpDir + "/stop_word"));
        commonWord = Utils.loadFileInSet(new File(opennlpDir + "/common_word"));
        init = true;
    }

    private ItalianTextProcessing(String opennlpDir) {
        this.opennlpDir = opennlpDir;
        try {
            init();
        } catch (Exception ex) {
            Logger.getLogger(ItalianTextProcessing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Processa un testo e restituisce le frasi individuate con i relativi token
     * {@link Token}
     *
     * @param text Il testo da processare
     * @return Le frasi con i token {@link Token}
     * @throws Exception
     */
    public List<List<Token>> processText(String text) throws Exception {
        List<List<Token>> sentences = new ArrayList<>();
        String[] sentenceDetection = sentenceDetection(text);
        for (String sentence : sentenceDetection) {
            List<Token> list = new ArrayList<>();
            String[] tokens = tokenize(sentence);
            String[] postags = postag(tokens);
            MorphoEntry[] lemmas = lemmatize(tokens, postags);
            String[] chunks = chunk(tokens, postags);
            int p = 1;
            for (int i = 0; i < tokens.length; i++) {
                POSenum postag = null;
                if (postags[i].startsWith("S")) {
                    postag = POSenum.NOUN;
                } else if (postags[i].startsWith("V")) {
                    postag = POSenum.VERB;
                } else if (postags[i].startsWith("A") || postags[i].startsWith("NO")) {
                    postag = POSenum.ADJ;
                } else if (postags[i].startsWith("B")) {
                    postag = POSenum.ADV;
                } else {
                    postag = POSenum.OTHER;
                }
                Token tobj = new Token(tokens[i], lemmas[i].getLemma(), postag, postags[i], chunks[i], p);
                tobj.setMorphoInfo(lemmas[i].getMorphoInfo());
                list.add(tobj);
                p++;
            }
            tagStopWord(list);
            tagCommonWord(list);
            String[] deps = parse(tokens, postags, lemmas);
            for (int j = 0; j < deps.length; j++) {
                list.get(j).setDepInfo(deps[j]);
            }
            Map<String, Span[]> nerMap = ner(tokens, postags);
            for (String btag : nerMap.keySet()) {
                Span[] spans = nerMap.get(btag);
                for (Span span : spans) {
                    list.get(span.getStart()).setIob2("B-" + span.getType());
                    for (int k = span.getStart() + 1; k < span.getEnd(); k++) {
                        list.get(k).setIob2("I");
                    }
                }
            }
            sentences.add(list);
        }
        return sentences;
    }

    public void tagStopWord(List<Token> tokens) throws IOException {
        for (Token token : tokens) {
            if (stopWord.contains(token.getToken().toLowerCase())) {
                token.setStopWord(true);
            } else if (stopWord.contains(token.getLemma().toLowerCase())) {
                token.setStopWord(true);
            } else {
                token.setStopWord(false);
            }
        }
    }

    public void tagCommonWord(List<Token> tokens) throws IOException {
        for (Token token : tokens) {
            if (commonWord.contains(token.getToken().toLowerCase())) {
                token.setCommonWord(true);
            } else if (commonWord.contains(token.getLemma().toLowerCase())) {
                token.setCommonWord(true);
            } else {
                token.setCommonWord(false);
            }
        }
    }

    /**
     * Rimuove le stop word dato in input un file contenente una stop word su
     * ogni riga
     *
     * @param stopWordFile Il file delle stop word
     * @param tokens La lista di token da processare
     * @return La lista di token priva delle stop word
     * @throws IOException
     */
    public List<Token> removeStopWord(File stopWordFile, List<Token> tokens) throws IOException {
        Set<String> stopWordSet = Utils.loadFileInSet(stopWordFile);
        return removeStopWord(stopWordSet, tokens);
    }

    /**
     * Rimuove le stop word dato in input un insieme di stop word
     *
     * @param stopWordSet Il file delle stop word
     * @param tokens La lista di token da processare
     * @return La lista di token priva delle stop word
     */
    public List<Token> removeStopWord(Set<String> stopWordSet, List<Token> tokens) {
        List<Token> list = new ArrayList<>();
        for (Token t : tokens) {
            if (!stopWordSet.contains(t.getToken().toLowerCase())) {
                list.add(t);
            }
        }
        return list;
    }

}
