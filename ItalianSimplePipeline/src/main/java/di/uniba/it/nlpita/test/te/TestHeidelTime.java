/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.test.te;

import de.unihd.dbs.heideltime.standalone.DocumentType;
import de.unihd.dbs.heideltime.standalone.HeidelTimeStandalone;
import de.unihd.dbs.heideltime.standalone.OutputType;
import de.unihd.dbs.heideltime.standalone.components.ResultFormatter;
import de.unihd.dbs.heideltime.standalone.components.impl.TimeMLResultFormatter;
import de.unihd.dbs.heideltime.standalone.components.impl.XMIResultFormatter;
import de.unihd.dbs.heideltime.standalone.exceptions.DocumentCreationTimeMissingException;
import de.unihd.dbs.uima.annotator.heideltime.resources.Language;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class TestHeidelTime {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Language lang=Language.ITALIAN;
            HeidelTimeStandalone htTagger = new HeidelTimeStandalone(lang, DocumentType.COLLOQUIAL, OutputType.TIMEML, "it_nlp/ht/config.props");
            String output=htTagger.process("Il primo martedì di ogni mese bisogna pagare la luce.", Calendar.getInstance().getTime());
            System.out.println(output);
        } catch (DocumentCreationTimeMissingException ex) {
            Logger.getLogger(TestHeidelTime.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
