/*
 * Questo software è stato sviluppato dal gruppo di ricerca SWAP del Dipartimento di Informatica dell'Università degli Studi di Bari.
 * Tutti i diritti sul software appartengono esclusivamente al gruppo di ricerca SWAP.
 * Il software non può essere modificato e utilizzato per scopi di ricerca e/o industriali senza alcun permesso da parte del gruppo di ricerca SWAP.
 * Il software potrà essere utilizzato a scopi di ricerca scientifica previa autorizzazione o accordo scritto con il gruppo di ricerca SWAP.
 * 
 * Bari, Marzo 2014
 */
package di.uniba.it.nlpita;

/**
 * Contiene l'enumeratore dei possibili pos-tag
 * @author Pierpaolo Basile pierpaolo.basile@gmail.com
 */
public enum POSenum {
    
    /**
     * Nome
     */
    NOUN,

    /**
     * Verbo
     */
    VERB,

    /**
     * Aggettivo
     */
    ADJ,

    /**
     * Avverbio
     */
    ADV,

    /**
     * Altre classi
     */
    OTHER
    
}
