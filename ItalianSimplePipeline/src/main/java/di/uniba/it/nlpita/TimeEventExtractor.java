/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita;

import de.unihd.dbs.heideltime.standalone.DocumentType;
import de.unihd.dbs.heideltime.standalone.HeidelTimeStandalone;
import de.unihd.dbs.heideltime.standalone.OutputType;
import de.unihd.dbs.uima.annotator.heideltime.resources.Language;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author pierpaolo
 */
public class TimeEventExtractor {

    private final HeidelTimeStandalone tagger;

    private static TimeEventExtractor instance;

    private TimeEventExtractor() {
        tagger = new HeidelTimeStandalone(Language.ITALIAN, DocumentType.COLLOQUIAL, OutputType.TIMEML, "it_nlp/ht/config.props");
    }

    public static synchronized TimeEventExtractor getInstance() {
        if (instance == null) {
            instance = new TimeEventExtractor();
        }
        return instance;
    }

    public String process(String text) throws Exception {
        return process(text, Calendar.getInstance().getTime());
    }

    public String process(String text, Date date) throws Exception {
        return tagger.process(text, date);
    }

    /*public List<String> extractEvent(String timemlDoc) throws Exception {
        Document xmlDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(timemlDoc)));
        List<String> events = new ArrayList<>();
        NodeList nodes = xmlDoc.getElementsByTagName("TIMEX3");
        for (int i = 0; i < nodes.getLength(); i++) {
            events.add(nodes.item(i).toString());
        }
        return events;
    }*/
    public List<TimeEvent> extractEvent(String timemlDoc) throws Exception {
        List<TimeEvent> events = new ArrayList<>();
        int s = timemlDoc.indexOf("<TIMEX3");
        while (s >= 0) {
            int e1 = timemlDoc.indexOf(">", s + 1);
            int e2 = timemlDoc.indexOf("</TIMEX3>", s + 1);
            TimeEvent event = new TimeEvent(timemlDoc.substring(e1 + 1, e2).trim(), timemlDoc.substring(s + 7, e1).trim());
            events.add(event);
            s = timemlDoc.indexOf("<TIMEX3", e2 + 9);
        }
        return events;
    }

}
