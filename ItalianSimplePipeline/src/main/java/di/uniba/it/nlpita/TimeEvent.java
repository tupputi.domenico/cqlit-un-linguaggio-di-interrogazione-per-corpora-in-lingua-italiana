/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita;

import org.json.simple.JSONObject;

/**
 *
 * @author pierpaolo
 */
public class TimeEvent {

    private String text;

    private String attributes;

    public TimeEvent(String text, String attributes) {
        this.text = text;
        this.attributes = attributes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public JSONObject toJSON() {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("text", text);
        jsonObj.put("attributes", attributes);
        return jsonObj;
    }

}
