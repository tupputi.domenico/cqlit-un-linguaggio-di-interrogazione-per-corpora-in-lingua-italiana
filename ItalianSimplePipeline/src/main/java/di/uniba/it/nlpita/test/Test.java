/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.test;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.Utils;
import di.uniba.it.nlpita.analysis.AnalysisUtils;
import di.uniba.it.nlpita.analysis.Chunk;
import di.uniba.it.nlpita.sentiment.SentixAPI;
import di.uniba.it.nlpita.sentiment.WNAffectAPI;
import di.uniba.it.nlpita.sentiment.WordNetFreqAPI;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            List<List<Token>> processedText = instance.processText("sai che ora sono?");
            WordNetFreqAPI wnapi = new WordNetFreqAPI();
            wnapi.init(new File("it_nlp/sentiment/index.sense.gz"));
            SentixAPI sentapi = new SentixAPI(wnapi);
            sentapi.init(new File("it_nlp/sentiment/sentix.gz"));
            WNAffectAPI affectapi = new WNAffectAPI();
            affectapi.init(new File("it_nlp/sentiment/wn_affect.lex"));
            for (List<Token> list : processedText) {
                float polarityScore = sentapi.getPolarityScore(list);
                affectapi.getAffectMap(list);
                for (Token token : list) {
                    System.out.println(token);
                }
                System.out.println("Polarity score: " + polarityScore);
                System.out.println("Output analysis...");
                List<Chunk> chunks = AnalysisUtils.getChunk(list);
                AnalysisUtils.relateChunks(chunks);
                System.out.println(chunks);
            }
            System.out.println("=========================================================");
            System.out.println("Semplicity score: " + Utils.getSimplictyScore(processedText));
            System.out.println("Gulpease score: " + Utils.getGulpeaseScore(processedText));
            
           
        } catch (Exception ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
