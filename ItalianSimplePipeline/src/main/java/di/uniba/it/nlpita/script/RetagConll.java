/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.MorphoEntry;
import di.uniba.it.nlpita.POSenum;
import di.uniba.it.nlpita.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class RetagConll {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            BufferedWriter writer = new BufferedWriter(new FileWriter(args[1]));
            String line;
            List<String> tokens = new ArrayList<>();
            List<String> rel = new ArrayList<>();
            List<String> pos = new ArrayList<>();
            while (reader.ready()) {
                line = reader.readLine();
                if (line.startsWith("#"))
                    continue;
                if (line.length() > 0) {
                    String[] split = line.split("\\t");
                    tokens.add(split[1]);
                    pos.add(split[6]);
                    rel.add(split[7]);
                } else if (tokens.size() > 0) {
                    String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                    MorphoEntry[] morphoEntry = instance.lemmatize(tokens.toArray(new String[tokens.size()]), postags);
                    for (int i = 0; i < tokens.size(); i++) {
                        writer.append(String.valueOf(i + 1)).append("\t");
                        writer.append(tokens.get(i)).append("\t");
                        writer.append(morphoEntry[i].getLemma()).append("\t");
                        //writer.append(Utils.getPos(postags[i]).name()).append("\t");
                        writer.append(postags[i].substring(0, 1)).append("\t");
                        writer.append(postags[i]).append("\t");
                        String[] split = morphoEntry[i].getMorphoInfo().split(":");
                        if (split.length > 1) {
                            String[] f = split[1].split("[+]");
                            for (int j = 0; j < f.length; j++) {
                                writer.append(f[j]);
                                if (j < f.length - 1) {
                                    writer.append("|");
                                }
                            }
                        } else {
                            writer.append("_");
                        }
                        writer.append("\t");
                        writer.append(pos.get(i)).append("\t");
                        writer.append(rel.get(i)).append("\t");
                        writer.append("_\t_");
                        writer.newLine();
                    }
                    writer.newLine();
                    tokens.clear();
                    rel.clear();
                    pos.clear();
                }
            }
            reader.close();
            if (tokens.size() > 0) {
                String[] postags = instance.postag(tokens.toArray(new String[tokens.size()]));
                MorphoEntry[] morphoEntry = instance.lemmatize(tokens.toArray(new String[tokens.size()]), postags);
                for (int i = 0; i < tokens.size(); i++) {
                    writer.append(String.valueOf(i + 1)).append("\t");
                    writer.append(tokens.get(i)).append("\t");
                    writer.append(morphoEntry[i].getLemma()).append("\t");
                    writer.append(Utils.getPos(postags[i]).name()).append("\t");
                    writer.append(postags[i]).append("\t");
                    String[] split = morphoEntry[i].getMorphoInfo().split(":");
                    if (split.length > 1) {
                        String[] f = split[1].split("[+]");
                        for (int j = 0; j < f.length; j++) {
                            writer.append(f[j]);
                            if (j < f.length - 1) {
                                writer.append("|");
                            }
                        }
                    } else {
                        writer.append("_");
                    }
                    writer.append("\t");
                    writer.append(pos.get(i)).append("\t");
                    writer.append(rel.get(i)).append("\t");
                    writer.append("_\t_");
                    writer.newLine();
                }
                writer.newLine();
            }
            writer.close();
        } catch (Exception ex) {
            Logger.getLogger(RetagConll.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
