/*
 * Trie.java
 *
 * Created on 2 aprile 2005, 12.59
 */
package di.uniba.it.nlpita.acopost;

/**
 *
 * @author  Basile Pierpaolo and Grieco Franco
 */
public class Trie {

    private int children = 0;             /* no of children */

    private byte unarychar = '\0';           /* if children=1 this is the char to follow */

    private Trie unarynext = null;         /* if children=1 daughter */

    private int count = 0;                /* number of word tokens with this suffix */

    private int[] tagcount = null;            /* counts distinguished by tags */

    private double[] lp = null;               /* smoothed lexical probabilities */

    private Trie mother = null;    /* mother node */

    private Trie[] next = null;     /* successors */


    /** Creates a new instance of Trie */
    public Trie() {
    }

    /** Creates a new instance of Trie */
    public Trie(Model m, Trie mother) {
        int not = m.getTags().size();
        this.mother = mother;
        tagcount = new int[not];
        for (int i = 0; i < not; i++) {
            tagcount[i] = 0;
        }
    }

    public Trie getDaughter(byte c) {

        if (children == 1) {
            if (unarychar == c) {
                return unarynext;
            }
        } else if (next != null && next[128 + c] != null) {
            return next[128 + c];
        }
        return null;
    }

    public void addDaughter(byte c, Trie daughter) {

        if (children == 0) {
            unarychar = c;
            unarynext = daughter;
        } else {
            if (next == null) {
                next = new Trie[256];

            }
            if (children == 1) {
                next[128 + unarychar] = unarynext;
            }
            next[128 + c] = daughter;
        }
        children++;
    }

    public void addWordInfoToTrieNode(Model m, Word wd) {
        int i;
        int not = m.getTags().size();
        count += wd.getCount();
        for (i = 0; i < not; i++) {
            tagcount[i] += wd.getTagcount()[i];
        }
    }

    /**
     * Getter for property children.
     * @return Value of property children.
     */
    public int getChildren() {
        return children;
    }

    /**
     * Setter for property children.
     * @param children New value of property children.
     */
    public void setChildren(int children) {
        this.children = children;
    }

    /**
     * Getter for property count.
     * @return Value of property count.
     */
    public int getCount() {
        return count;
    }

    /**
     * Setter for property count.
     * @param count New value of property count.
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Getter for property lp.
     * @return Value of property lp.
     */
    public double[] getLp() {
        return lp;
    }

    /**
     * Setter for property lp.
     * @param lp New value of property lp.
     */
    public void setLp(double[] lp) {
        this.lp = lp;
    }

    /**
     * Getter for property mother.
     * @return Value of property mother.
     */
    public Trie getMother() {
        return mother;
    }

    /**
     * Setter for property mother.
     * @param mother New value of property mother.
     */
    public void setMother(Trie mother) {
        this.mother = mother;
    }

    /**
     * Getter for property next.
     * @return Value of property next.
     */
    public Trie[] getNext() {
        return next;
    }

    /**
     * Setter for property next.
     * @param next New value of property next.
     */
    public void setNext(Trie[] next) {
        this.next = next;
    }

    /**
     * Getter for property tagcount.
     * @return Value of property tagcount.
     */
    public int[] getTagcount() {
        return tagcount;
    }

    /**
     * Setter for property tagcount.
     * @param tagcount New value of property tagcount.
     */
    public void setTagcount(int[] tagcount) {
        this.tagcount = tagcount;
    }

    /**
     * Getter for property unarychar.
     * @return Value of property unarychar.
     */
    public byte getUnarychar() {
        return unarychar;
    }

    /**
     * Setter for property unarychar.
     * @param unarychar New value of property unarychar.
     */
    public void setUnarychar(byte unarychar) {
        this.unarychar = unarychar;
    }

    /**
     * Getter for property unarynext.
     * @return Value of property unarynext.
     */
    public Trie getUnarynext() {
        return unarynext;
    }

    /**
     * Setter for property unarynext.
     * @param unarynext New value of property unarynext.
     */
    public void setUnarynext(Trie unarynext) {
        this.unarynext = unarynext;
    }
}
