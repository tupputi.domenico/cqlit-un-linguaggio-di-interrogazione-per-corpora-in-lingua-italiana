/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.rules;

import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.analysis.Chunk;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author pierpaolo
 */
public class RuleUtils {

    public static boolean matchLemmaInChunk(String lemma, Chunk chunk) {
        for (Token t : chunk.getTokens()) {
            if (t.getLemma().equalsIgnoreCase(lemma)) {
                return true;
            }
        }
        return false;
    }

    public static boolean matchLemmaInChunk(String[] lemmas, Chunk chunk) {
        Set<String> set = new HashSet(Arrays.asList(lemmas));
        for (Token t : chunk.getTokens()) {
            if (set.contains(t.getLemma())) {
                return true;
            }
        }
        return false;
    }

    public static boolean matchAndLemmaInChunk(String[] lemmas, Chunk chunk) {
        Set<String> set = new HashSet();
        for (Token t : chunk.getTokens()) {
            set.add(t.getLemma());
        }
        boolean m = true;
        for (String l : lemmas) {
            m = m && set.contains(l);
        }
        return m;
    }

    public static Set<Chunk> getRelatedChunks(int head, String relation, Map<Integer, Chunk> chunks) {
        Set<Chunk> set = new HashSet<>();
        for (Chunk chunk : chunks.values()) {
            if (chunk.getTarget() == head && chunk.getDep().matches(relation)) {
                set.add(chunk);
            }
        }
        return set;
    }

    public static Set<Chunk> getRelatedChunks(int head, String relation, String type, Map<Integer, Chunk> chunks) {
        Set<Chunk> set = new HashSet<>();
        for (Chunk chunk : chunks.values()) {
            if (chunk.getTarget() == head && chunk.getDep().matches(relation) && chunk.getType().equalsIgnoreCase(type)) {
                set.add(chunk);
            }
        }
        return set;
    }

    public static Chunk getRootChunk(Map<Integer, Chunk> chunks) {
        for (Chunk chunk : chunks.values()) {
            if (chunk.getTarget() == 0) {
                return chunk;
            }
        }
        return null;
    }

}
