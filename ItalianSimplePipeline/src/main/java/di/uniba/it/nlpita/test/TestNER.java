/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.test;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.PoSFeatureGenerator;
import di.uniba.it.nlpita.script.RetagChuncker;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;

/**
 *
 * @author pierpaolo
 */
public class TestNER {
    
    private static String[][] getPosFeatures(String[] postags) {
        String[][] features = new String[postags.length][1];
        for (int i = 0; i < postags.length; i++) {
            features[i][0] = PoSFeatureGenerator.POS_PREFIX + "=" + postags[i];
        }
        return features;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            ItalianTextProcessing nlp = ItalianTextProcessing.getInstance("it_nlp/");
            InputStream modelIn = new FileInputStream(new File(args[0]));
            TokenNameFinderModel model = new TokenNameFinderModel(modelIn);
            NameFinderME nameFinder = new NameFinderME(model);
            String[] sentences = nlp.sentenceDetection("Slitta l'incarico di Marco Carrai a capo della cyber security: farà parte come consulente dello staff di Palazzo Chigi. Il prefetto di Roma dovrebbe già essere nominato al vertice del Viminale");
            for (String sentence : sentences) {
                String[] tokens = nlp.tokenize(sentence);
                String[] postag = nlp.postag(tokens);
                Span nameSpans[] = nameFinder.find(tokens, getPosFeatures(postag));
                for (Span span : nameSpans) {
                    for (int k=span.getStart();k<span.getEnd();k++) {
                        System.out.print(tokens[k]+" ");
                    }
                    System.out.println(span.getType());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(RetagChuncker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
