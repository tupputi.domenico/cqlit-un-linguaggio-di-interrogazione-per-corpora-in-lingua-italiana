/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.service.v1;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.TimeEvent;
import di.uniba.it.nlpita.TimeEventExtractor;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.analysis.AnalysisUtils;
import di.uniba.it.nlpita.analysis.Chunk;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author pierpaolo
 */
@Path("v1")
public class ServiceV1 {

    private static final Logger LOG = Logger.getLogger(ServiceV1.class.getName());

    @POST
    @Path("process")
    public Response process(String jsonString) {
        try {
            JSONObject json = (JSONObject) JSONValue.parse(jsonString);
            String text = json.get("text").toString();
            String outtimeml = TimeEventExtractor.getInstance().process(text);
            List<TimeEvent> timeEvents = TimeEventExtractor.getInstance().extractEvent(outtimeml);
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            List<List<Token>> processedText = instance.processText(text);
            JSONObject jsonResponse = new JSONObject();
            JSONArray jsonTE = new JSONArray();
            for (TimeEvent te : timeEvents) {
                jsonTE.add(te.toJSON());
            }
            jsonResponse.put("timeEvents", jsonTE);
            JSONArray jsonSentences = new JSONArray();
            for (List<Token> sentence : processedText) {
                JSONObject jsonObjSentence = new JSONObject();

                JSONArray jsonSentence = new JSONArray();
                for (Token token : sentence) {
                    jsonSentence.add(token.toJSON());
                }
                jsonObjSentence.put("sentence", jsonSentence);

                List<Chunk> chunks = AnalysisUtils.getChunk(sentence);
                AnalysisUtils.relateChunks(chunks);
                JSONArray jsonChunks = new JSONArray();
                for (Chunk chunk : chunks) {
                    jsonChunks.add(chunk.toJSON());
                }
                jsonObjSentence.put("chunks", jsonChunks);

                jsonSentences.add(jsonObjSentence);
            }
            jsonResponse.put("text", text);
            jsonResponse.put("numSent", processedText.size());
            jsonResponse.put("sentences", jsonSentences);
            return Response.ok(jsonResponse.toString()).build();
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error to process text", ex);
            return Response.serverError().build();
        }
    }

}
