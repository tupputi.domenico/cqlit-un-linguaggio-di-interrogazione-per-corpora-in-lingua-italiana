/*
 * Questo software è stato sviluppato dal gruppo di ricerca SWAP del Dipartimento di Informatica dell'Università degli Studi di Bari.
 * Tutti i diritti sul software appartengono esclusivamente al gruppo di ricerca SWAP.
 * Il software non può essere modificato e utilizzato per scopi di ricerca e/o industriali senza alcun permesso da parte del gruppo di ricerca SWAP.
 * Il software potrà essere utilizzato a scopi di ricerca scientifica previa autorizzazione o accordo scritto con il gruppo di ricerca SWAP.
 * 
 * Bari, Marzo 2014
 */
package di.uniba.it.nlpita;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/**
 * Implementa un lemmatizzatore per la lingua italiano basato sul dizionario
 *
 * @author pierpaolo
 */
public class ItalianLemmatizer {

    private boolean init = false;

    private Map<String, MorphoEntry> dict;

    private static final Logger LOG = Logger.getLogger(ItalianLemmatizer.class.getName());

    /**
     * Crea una nuova istanza del lemmatizzatore
     *
     * @param dictFile Il dizionario
     * @param posTagConversionFile Il file di conversione dei pos-tag
     * @throws Exception
     */
    public ItalianLemmatizer(File dictFile, File posTagConversionFile) throws Exception {
        init(dictFile, posTagConversionFile);
    }

    private void init(File dictFile, File posTagConversionFile) throws Exception {
        LOG.log(Level.INFO, "Init lemmatizer: {0}", dictFile.getAbsolutePath());
        LOG.log(Level.INFO, "POS mapping: {0}", posTagConversionFile.getAbsolutePath());
        Map<String, Set<String>> postagMap = buildMorpht2TanlPosTagMap(posTagConversionFile);
        dict = loadDict(dictFile, postagMap);
        init = true;
    }

    /**
     * Lemmatizza una parola
     *
     * @param tokens La parola
     * @param posTag Il pos-tag
     * @return Il lemma
     * @throws Exception
     */
    public MorphoEntry[] lemmatize(String[] tokens, String[] posTag) throws Exception {
        if (!init) {
            throw new Exception("Pipeline is not inizialized");
        }
        if (tokens.length != posTag.length) {
            throw new IllegalArgumentException("Tokens and pos-tags with different size");
        }
        MorphoEntry[] lemmas = new MorphoEntry[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            MorphoEntry lemma = dict.get(tokens[i] + "_" + posTag[i]);
            if (lemma == null) {
                String failsafeTagTanl = getFailsafeTagTanl(posTag[i]);
                if (failsafeTagTanl != null) {
                    lemma = dict.get(tokens[i] + "_#" + failsafeTagTanl + "#");
                } else {
                    lemma = dict.get(tokens[i] + "_#!#");
                }
                if (lemma == null) {
                    lemma = new MorphoEntry(tokens[i].toLowerCase(), "UNDEF");
                }
            }
            lemmas[i] = lemma;
        }
        return lemmas;
    }

    private String getFailsafeTagTanl(String tag) {
        if (tag.startsWith("S")) {
            return "N";
        } else if (tag.startsWith("V")) {
            return "V";
        } else if (tag.startsWith("B")) {
            return "R";
        } else if (tag.startsWith("A") || tag.startsWith("NO")) {
            return "A";
        }
        return null;
    }

    private String getFailsafeTagMorpthIt(String tag) {
        if (tag.contains("NOUN")) {
            return "N";
        } else if (tag.contains("VER") || tag.contains("AUX") || tag.contains("MOD")) {
            return "V";
        } else if (tag.contains("ADV")) {
            return "R";
        } else if (tag.contains("ADJ")) {
            return "A";
        }
        return null;
    }

    private Map<String, MorphoEntry> loadDict(File dictFile, Map<String, Set<String>> posTagMap) throws IOException {
        Map<String, MorphoEntry> dict = new HashMap<>();
        BufferedReader reader;
        if (dictFile.getName().endsWith(".gz")) {
            reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(dictFile))));
        } else {
            reader = new BufferedReader(new FileReader(dictFile));
        }
        String line;
        while (reader.ready()) {
            line = reader.readLine();
            String[] split = line.split("\\s+");
            if (split.length == 3) {
                Set<String> set = posTagMap.get(split[2]);
                if (set != null) {
                    for (String p : set) {
                        dict.put(split[0] + "_" + p, new MorphoEntry(split[1], split[2]));
                    }
                } else {
                    String failsafeTag = getFailsafeTagMorpthIt(split[2]);
                    if (failsafeTag != null) {
                        dict.put(split[0] + "_#" + failsafeTag + "#", new MorphoEntry(split[1], split[2]));
                    } else {
                        dict.put(split[0] + "_#!#", new MorphoEntry(split[1], split[2]));
                    }
                }
            } else {
                throw new IOException("No valid dict file, line: " + line);
            }
        }
        reader.close();
        return dict;
    }

    private Map<String, Set<String>> buildMorpht2TanlPosTagMap(File file) throws IOException {
        Map<String, Set<String>> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        while (reader.ready()) {
            line = reader.readLine();
            String[] posSplit = line.split("\t");
            if (posSplit.length == 2) {
                String[] morphPosSplit = posSplit[1].split(" ");
                for (String mp : morphPosSplit) {
                    Set<String> set = map.get(mp);
                    if (set == null) {
                        set = new HashSet<>();
                        map.put(mp, set);
                    }
                    set.add(posSplit[0]);
                }
            } else {
                throw new IOException("No valid pos tag conversion file, line: " + line);
            }
        }
        reader.close();
        return map;
    }

}
