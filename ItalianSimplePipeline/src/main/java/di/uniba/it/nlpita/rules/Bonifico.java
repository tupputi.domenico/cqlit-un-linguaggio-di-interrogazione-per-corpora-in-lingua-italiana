/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.rules;

import di.uniba.it.nlpita.analysis.Chunk;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author pierpaolo
 */
public class Bonifico {

    private static final String[] activationLemmas = new String[]{"pagare", "disporre", "eseguire", "inoltrare", "inviare", "mandare", "bonificare", "bonifica", "fare"};

    private static final String[] rootObj = new String[]{"bonifico", "pagamento", "denaro"};

    public EvaluationResult evalutate(Map<Integer, Chunk> pmap) {
        //start from root
        double score = 0;
        Chunk rootChunk = RuleUtils.getRootChunk(pmap);
        if (RuleUtils.matchLemmaInChunk(activationLemmas, rootChunk)) {
            score += 0.5;
            Set<Chunk> relatedChunks = RuleUtils.getRelatedChunks(1, "OBJ", "NP", pmap);
            double avg = 0;
            for (Chunk relRootchunck : relatedChunks) {
                if (RuleUtils.matchLemmaInChunk(rootObj, relRootchunck)) {
                    avg++;
                }
            }
            score += 0.25 * (avg / (double) relatedChunks.size());
        }
        EvaluationResult er = new EvaluationResult(score);
        return er;
    }

}
