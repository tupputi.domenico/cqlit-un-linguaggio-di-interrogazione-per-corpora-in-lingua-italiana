/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.test;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.Utils;
import di.uniba.it.nlpita.analysis.AnalysisUtils;
import di.uniba.it.nlpita.analysis.Chunk;
import di.uniba.it.nlpita.rules.Bonifico;
import di.uniba.it.nlpita.rules.BonificoEstero;
import di.uniba.it.nlpita.sentiment.SentixAPI;
import di.uniba.it.nlpita.sentiment.WordNetFreqAPI;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class TestBank {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Bonifico bonifico=new Bonifico();
            BonificoEstero bonificoEstero=new BonificoEstero();
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            List<List<Token>> processedText = instance.processText("voglio fare un bonifico di 1000 euro a Luca per affitto domani");
            for (List<Token> list : processedText) {
                for (Token token : list) {
                    System.out.println(token);
                }
                List<Chunk> chunks = AnalysisUtils.getChunk(list);
                AnalysisUtils.relateChunks(chunks);
                System.out.println(chunks);
                Map<Integer, Chunk> pmap = AnalysisUtils.getChunkMap(chunks);
                
                System.out.println(bonifico.evalutate(pmap));
                System.out.println(bonificoEstero.evalutate(pmap));
                
            }

        } catch (Exception ex) {
            Logger.getLogger(TestBank.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
