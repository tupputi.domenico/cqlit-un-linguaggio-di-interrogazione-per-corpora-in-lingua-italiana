/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita;

/**
 *
 * @author pierpaolo
 */
public class MorphoEntry {
    
    private String lemma;
    
    private String morphoInfo;

    public MorphoEntry(String lemma, String morphoInfo) {
        this.lemma = lemma;
        this.morphoInfo = morphoInfo;
    }

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public String getMorphoInfo() {
        return morphoInfo;
    }

    public void setMorphoInfo(String morphoInfo) {
        this.morphoInfo = morphoInfo;
    }
    
    
    
}
