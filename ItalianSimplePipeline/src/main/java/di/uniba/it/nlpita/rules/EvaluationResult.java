/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.rules;

import java.util.Map;

/**
 *
 * @author pierpaolo
 */
public class EvaluationResult {
    
    private double score;
    
    private Map<String,String> attributes;

    public EvaluationResult() {
    }
    
    public EvaluationResult(double score) {
        this.score = score;
    }

    public EvaluationResult(double score, Map<String, String> attributes) {
        this.score = score;
        this.attributes = attributes;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return "EvaluationResult{" + "score=" + score + ", attributes=" + attributes + '}';
    }
    
    
    
}
