/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.sentiment;

import di.uniba.it.nlpita.POSenum;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author pierpaolo
 */
public class WordNetFreqAPI {
    
    private final Map<String, Integer> map = new HashMap<>();
    
    private double countTot = 0;
    
    private static final Logger LOG = Logger.getLogger(WordNetFreqAPI.class.getName());
    
    public WordNetFreqAPI() {
    }
    
    public void init(File file) throws IOException {
        map.clear();
        LOG.log(Level.INFO, "Init WordNet Freq API {0}", file.getAbsolutePath());
        BufferedReader reader;
        if (file.getName().endsWith(".gz")) {
            reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
        } else {
            reader = new BufferedReader(new FileReader(file));
        }
        while (reader.ready()) {
            String[] split = reader.readLine().split(" ");
            String poscode = split[0].split(":+")[0].split("%")[1];
            String poss = "";
            switch (poscode) {
                case "1":
                    poss = "n";
                    break;
                case "2":
                    poss = "v";
                    break;
                case "3":
                    poss = "a";
                    break;
                case "4":
                    poss = "r";
                    break;
                case "5":
                    poss = "a";
                    break;
            }
            int f = Integer.parseInt(split[3]);
            String id = poss + split[1];
            Integer v = map.get(id);
            if (v == null) {
                map.put(id, f);
            } else {
                map.put(id, f + v);
            }
            countTot += f;
        }
        reader.close();
        LOG.log(Level.INFO, "Loaded: {0}", map.size());
    }
    
    public Integer getFreq(String id) {
        return map.get(id);
    }
    
    public Integer getFreq(String offset, POSenum pos) {
        return getFreq(generateId(offset, pos));
    }
    
    public Double getLaplaceSmooth(String id) {
        Integer f = map.get(id);
        if (f != null) {
            return (double) (f + 1) / (countTot + (double) map.size());
        } else {
            return null;
        }
    }
    
    public Double getLaplaceSmooth(String offset, POSenum pos) {
        return getLaplaceSmooth(generateId(offset, pos));
    }
    
    private String generateId(String offset, POSenum pos) {
        switch (pos) {
            case NOUN:
                return "n" + offset;
            case ADJ:
                return "a" + offset;
            case VERB:
                return "v" + offset;
            case ADV:
                return "r" + offset;
            default:
                return offset;
        }
    }
    
}
