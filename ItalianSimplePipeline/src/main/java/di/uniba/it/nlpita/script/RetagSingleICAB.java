/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import di.uniba.it.nlpita.ItalianTextProcessing;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierpaolo
 */
public class RetagSingleICAB {

    private static final Logger LOG = Logger.getLogger(RetagSingleICAB.class.getName());

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Set<String> typeSet = new HashSet<>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]), "ISO-8859-1"));
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.length() > 0) {
                    String[] split = line.split("\\s+");
                    if (split[3].startsWith("B")) {
                        String[] type = split[3].split("-");
                        typeSet.add(type[1]);
                    }
                }
            }
            reader.close();
            Logger.getLogger(RetagSingleICAB.class.getName()).log(Level.INFO, typeSet.toString());
            for (String vtag : typeSet) {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0]), "ISO-8859-1"));
                BufferedWriter writer = new BufferedWriter(new FileWriter(args[1] + "." + vtag));
                boolean tagOpen = false;
                while (reader.ready()) {
                    String line = reader.readLine();
                    if (line.length() > 0) {
                        String[] split = line.split("\\s+");
                        if (split[3].startsWith("B")) {
                            String[] type = split[3].split("-");
                            if (tagOpen) {
                                writer.append(" <END>");
                                tagOpen=false;
                            }
                            if (type[1].equals(vtag)) {
                                writer.append(" <START:").append(type[1]).append(">");
                                writer.append(" ").append(split[0]);
                                tagOpen = true;
                            } else {
                                writer.append(" ").append(split[0]);
                            }
                        } else if (split[3].startsWith("O") && tagOpen) {
                            tagOpen = false;
                            writer.append(" <END>");
                            writer.append(" ").append(split[0]);
                        } else {
                            writer.append(" ").append(split[0]);
                        }
                    } else {
                        if (tagOpen) {
                            writer.append(" <END>");
                            tagOpen = false;
                        }
                        writer.newLine();
                    }
                }
                reader.close();
                writer.close();
            }
        } catch (IOException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

}
