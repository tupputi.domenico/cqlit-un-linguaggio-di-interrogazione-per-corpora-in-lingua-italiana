/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.service;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author pierpaolo
 */
public class RESTService extends Thread {

    private final HttpServer server;

    private Properties props;

    /**
     * Start HTTP server
     *
     * @return @throws IOException
     */
    protected HttpServer startServer() throws IOException {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.example package
        final ResourceConfig rc = new ResourceConfig().packages(true, "di.uniba.it.nlpita.service.v1");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(props.getProperty("bind.address")), rc);
    }

    /**
     *
     * @throws Exception
     */
    public RESTService() throws Exception {
        this("./server.config");
    }

    /**
     *
     * @param propsFilename
     * @throws Exception
     */
    public RESTService(String propsFilename) throws Exception {
        //init config
        System.out.println("Starting server using config file: " + propsFilename);
        props = new Properties();
        FileReader freader = new FileReader(propsFilename);
        props.load(freader);
        freader.close();
        //init wrapper
        System.out.println("Init wrapper");
        server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl", props.getProperty("bind.address")));
        //attach a shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            //this method is run when the service is stopped (SIGTERM)
            @Override
            public void run() {
                try {
                    server.shutdownNow();
                } catch (Exception ex) {
                    Logger.getLogger(RESTService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }));
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(RESTService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            RESTService service;
            if (args.length > 0) {
                service = new RESTService(args[0]);
            } else {
                service = new RESTService();
            }
            service.start();
        } catch (Exception ex) {
            Logger.getLogger(RESTService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
