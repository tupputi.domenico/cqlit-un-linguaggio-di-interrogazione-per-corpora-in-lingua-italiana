/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.script;

import di.uniba.it.nlpita.PoSFeatureGenerator;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.NameSample;
import opennlp.tools.namefind.NameSampleDataStream;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;
import opennlp.tools.util.TrainingParameters;
import opennlp.tools.util.featuregen.AdaptiveFeatureGenerator;
import opennlp.tools.util.featuregen.BigramNameFeatureGenerator;
import opennlp.tools.util.featuregen.CachedFeatureGenerator;
import opennlp.tools.util.featuregen.OutcomePriorFeatureGenerator;
import opennlp.tools.util.featuregen.PreviousMapFeatureGenerator;
import opennlp.tools.util.featuregen.SentenceFeatureGenerator;
import opennlp.tools.util.featuregen.TokenClassFeatureGenerator;
import opennlp.tools.util.featuregen.TokenFeatureGenerator;
import opennlp.tools.util.featuregen.WindowFeatureGenerator;

/**
 *
 * @author pierpaolo
 */
public class TrainNER {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Charset charset = Charset.forName("UTF-8");
            ObjectStream<String> lineStream = new PlainTextByLineStream(new FileInputStream(args[0]), charset);
            ObjectStream<NameSample> sampleStream = new NameSampleDataStream(lineStream);

            InputStream modelIn = new FileInputStream("./it_nlp/it-pos-maxent.bin");
            POSModel posmodel = new POSModel(modelIn);
            POSTaggerME tagger = new POSTaggerME(posmodel);

            AdaptiveFeatureGenerator featureGenerator = new CachedFeatureGenerator(
                    new AdaptiveFeatureGenerator[]{
                        new WindowFeatureGenerator(new TokenFeatureGenerator(), 2, 2),
                        new WindowFeatureGenerator(new TokenClassFeatureGenerator(true), 2, 2),
                        new WindowFeatureGenerator(new PoSFeatureGenerator(tagger), 2, 2),
                        new OutcomePriorFeatureGenerator(),
                        new PreviousMapFeatureGenerator(),
                        new BigramNameFeatureGenerator(),
                        new SentenceFeatureGenerator(true, false)
                    });

            TokenNameFinderModel model = NameFinderME.train("en", "icab", sampleStream, TrainingParameters.defaultParams(), featureGenerator, Collections.<String, Object>emptyMap());

            BufferedOutputStream modelOut = new BufferedOutputStream(new FileOutputStream(args[1]));
            model.serialize(modelOut);
            modelOut.close();
        } catch (Exception ex) {
            Logger.getLogger(TrainNER.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
