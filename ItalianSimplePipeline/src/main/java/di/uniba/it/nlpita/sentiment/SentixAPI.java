/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.sentiment;

import di.uniba.it.nlpita.POSenum;
import di.uniba.it.nlpita.Token;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author pierpaolo
 */
public class SentixAPI {

    private Map<String, List<SentixEntry>> map = new HashMap<>();

    private final WordNetFreqAPI wnapi;

    private static final Logger LOG = Logger.getLogger(SentixAPI.class.getName());

    public SentixAPI(WordNetFreqAPI wnapi) {
        this.wnapi = wnapi;
    }

    public void init(File file) throws IOException {
        map.clear();
        LOG.log(Level.INFO, "Init Sentix API {0}", file.getAbsolutePath());
        BufferedReader reader;
        if (file.getName().endsWith(".gz")) {
            reader = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
        } else {
            reader = new BufferedReader(new FileReader(file));
        }
        while (reader.ready()) {
            String[] split = reader.readLine().split("\\t");
            String key = split[0] + "#" + split[1];
            List<SentixEntry> list = map.get(key);
            if (list == null) {
                list = new ArrayList<>();
                map.put(key, list);
            }
            list.add(new SentixEntry(split[1] + split[2], Float.parseFloat(split[3]), Float.parseFloat(split[4]), Float.parseFloat(split[5]), Float.parseFloat(split[6])));
        }
        reader.close();
        LOG.log(Level.INFO, "Loaded: {0}", map.size());
    }

    public float getPolarityScore(List<Token> tokens) {
        float score = 0;
        float d = 0;
        boolean negation = false;
        for (Token token : tokens) {
            if (token.getToken().equalsIgnoreCase("non")) {
                negation = !negation;
            } else if (token.getToken().matches("[.:;!?]")) {
                negation = false;
            } else if (token.getPos() != POSenum.OTHER) {
                List<SentixEntry> list = map.get(token.getLemma() + "#" + generatePosString(token.getPos()));
                if (list != null) {
                    int idx = getMFS(list);
                    if (idx >= 0) {
                        if (negation) {
                            token.setPolarity(-1 * list.get(idx).getPolarityScore());
                            score += token.getPolarity();
                        } else {
                            token.setPolarity(list.get(idx).getPolarityScore());
                            score += token.getPolarity();
                        }
                    }
                }
                d++;
            }
        }
        if (d == 0) {
            return 0;
        } else {
            return score / d;
        }
    }

    public float getTextPolarityScore(List<List<Token>> text) {
        float score = 0;
        float d = 0;
        boolean negation = false;
        for (List<Token> sentence : text) {
            for (Token token : sentence) {
                if (token.getToken().equalsIgnoreCase("non")) {
                    negation = !negation;
                } else if (token.getToken().matches("[.:;!?]")) {
                    negation = false;
                } else if (token.getPos() != POSenum.OTHER) {
                    List<SentixEntry> list = map.get(token.getLemma() + "#" + generatePosString(token.getPos()));
                    if (list != null) {
                        int idx = getMFS(list);
                        if (idx >= 0) {
                            if (negation) {
                                token.setPolarity(-1 * list.get(idx).getPolarityScore());
                                score += token.getPolarity();
                            } else {
                                token.setPolarity(list.get(idx).getPolarityScore());
                                score += token.getPolarity();
                            }
                        }
                    }
                    d++;
                }
            }
        }
        if (d == 0) {
            return 0;
        } else {
            return score / d;
        }
    }

    private int getMFS(List<SentixEntry> list) {
        int idx = -1;
        int max = 0;
        for (int i = 0; i < list.size(); i++) {
            Integer freq = wnapi.getFreq(list.get(i).getId());
            if (freq != null && freq > max) {
                max = freq;
                idx = i;
            }
        }
        return idx;
    }

    public SentixEntry getPolarity(String lemma, String pos) {
        POSenum postag = null;
        if (pos.startsWith("S")) {
            postag = POSenum.NOUN;
        } else if (pos.startsWith("V")) {
            postag = POSenum.VERB;
        } else if (pos.startsWith("A") || pos.startsWith("NO")) {
            postag = POSenum.ADJ;
        } else if (pos.startsWith("B")) {
            postag = POSenum.ADV;
        } else {
            postag = POSenum.OTHER;
        }
        List<SentixEntry> list = map.get(lemma + "#" + generatePosString(postag));
        if (list != null) {
            int idx = getMFS(list);
            if (idx >= 0) {
                return list.get(idx);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private String generatePosString(POSenum pos) {
        switch (pos) {
            case NOUN:
                return "n";
            case ADJ:
                return "a";
            case VERB:
                return "v";
            case ADV:
                return "r";
            default:
                return pos.name();
        }
    }

}
