/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.analysis;

import di.uniba.it.nlpita.Token;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author pierpaolo
 */
public class Chunk {

    private final List<Token> tokens;

    private int target;

    private String dep;

    private String type;

    public Chunk(List<Token> tokens) {
        this.tokens = tokens;
    }

    public Chunk() {
        tokens = new ArrayList<>();
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public String getDep() {
        return dep;
    }

    public void setDep(String dep) {
        this.dep = dep;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < tokens.size(); i++) {
            sb.append(tokens.get(i).getToken());
            if (i < tokens.size() - 1) {
                sb.append(" ");
            }
        }
        sb.append("]:").append(type).append(":").append(String.valueOf(target)).append(":").append(dep);
        return sb.toString();
    }

    public JSONObject toJSON() {
        JSONObject json = new JSONObject();
        if (type != null) {
            json.put("type", type);
        }
        json.put("target", target);
        if (dep != null) {
            json.put("dep", dep);
        }
        JSONArray jsonTokens=new JSONArray();
        for (Token token:tokens) {
            jsonTokens.add(token.getToken());
        }
        json.put("tokens", jsonTokens);
        return json;
    }

    public List<Dependency> getDependencies() {
        List<Dependency> deps = new ArrayList<>();
        for (Token token : tokens) {
            String[] split = token.getDepInfo().split("_");
            deps.add(new Dependency(split[1], Integer.parseInt(split[0])));
        }
        return deps;
    }

}
