/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.uniba.it.nlpita.analysis;

import di.uniba.it.nlpita.Token;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author pierpaolo
 */
public class AnalysisUtils {

    private static boolean containsOutPhrases(List<Token> list) {
        for (Token token : list) {
            if (token.getChunk().equals("O")) {
                return true;
            }
        }
        return false;
    }

    public static List<List<Token>> getPhrases(List<Token> sentence) {
        List<List<Token>> phrases = new ArrayList<>();
        List<Token> currentList = new ArrayList<>();
        for (Token token : sentence) {
            if (token.getChunk().startsWith("B")) {
                if (currentList.size() > 0) {
                    phrases.add(currentList);
                    currentList = new ArrayList<>();
                }
                currentList.add(token);
            } else if (token.getChunk().startsWith("I")) {
                currentList.add(token);
            } else if (token.getChunk().equals("O")) {
                if (currentList.size() > 0 && !containsOutPhrases(currentList)) {
                    phrases.add(currentList);
                    currentList = new ArrayList<>();
                }
                currentList.add(token);
            }
        }
        if (currentList.size() > 0) {
            phrases.add(currentList);
        }
        return phrases;
    }

    public static List<Chunk> getChunk(List<Token> sentence) {
        List<Chunk> chunks = new ArrayList<>();
        Chunk currentChunk = new Chunk();
        for (Token token : sentence) {
            if (token.getChunk().startsWith("B")) {
                if (currentChunk.getTokens().size() > 0) {
                    chunks.add(currentChunk);
                    currentChunk = new Chunk();
                }
                currentChunk.setType(token.getChunk().split("-")[1]);
                currentChunk.getTokens().add(token);
            } else if (token.getChunk().startsWith("I")) {
                currentChunk.getTokens().add(token);
            } else if (token.getChunk().equals("O")) {
                if (currentChunk.getTokens().size() > 0 && !containsOutPhrases(currentChunk.getTokens())) {
                    chunks.add(currentChunk);
                    currentChunk = new Chunk();
                    currentChunk.setType("O");
                }
                currentChunk.getTokens().add(token);
            }
        }
        if (currentChunk.getTokens().size() > 0) {
            chunks.add(currentChunk);
        }
        return chunks;
    }

    private static String findRelation(List<Token> tokens, List<Dependency> dependencies) {
        Set<Integer> posset = new HashSet<>();
        for (Token t : tokens) {
            posset.add(t.getPosition());
        }
        for (Dependency dep : dependencies) {
            if (posset.contains(dep.getHead())) {
                return dep.getType();
            }
        }
        return null;
    }

    public static void relateChunks(List<Chunk> chunks) {
        for (int i = 0; i < chunks.size(); i++) {
            List<Dependency> dependencies = chunks.get(i).getDependencies();
            for (int j = 0; j < chunks.size(); j++) {
                if (i != j) {
                    List<Token> tokens = chunks.get(j).getTokens();
                    String rel = findRelation(tokens, dependencies);
                    if (rel != null) {
                        chunks.get(i).setDep(rel);
                        chunks.get(i).setTarget(j + 1);
                    }
                }
            }
        }
    }

    public static Map<Integer, Chunk> getChunkMap(List<Chunk> chunks) {
        Map<Integer, Chunk> map = new HashMap<>();
        for (int i = 0; i < chunks.size(); i++) {
            map.put(i + 1, chunks.get(i));
        }
        return map;
    }

}
