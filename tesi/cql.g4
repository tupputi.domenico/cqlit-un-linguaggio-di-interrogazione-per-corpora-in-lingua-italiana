/**
 * Define a grammar called Hello
 */
grammar cql;

r : expressionpart+;

expressionpart: ('[' expression ']' repetition?);

expression
  :  or
  ;

or
  :  and (OR and)*    // make `||` the root
  ;

and
  :  not (AND not)*      // make `&&` the root
  ;

not
  :  NOT atom    // make `~` the root
  |  atom
  ;

atom
  :  clausepart
  |  '(' expression ')'    // omit both `(` and `)`
  ;

clausepart : attribute operator value;


value :    ID
            |   STRING
            |   NUM
            ;



AND:  '&' ;   
OR: '|'  ;
NOT: '!'   ;


attribute : ('lemma' | 'postag' | 'token' | 'pos' | 'morphoinfo' |
'chunk' | 'stopword' | 'commonword' | 'polarity' | 'emolabels' |
'iob2' |'depinfo');


repetition:   		'*'                             							# repetitionZeroOrMore
                    | '+'                           							# repetitionOneOrMore
                    | '?'				              							# repetitionZeroOrOne
                    | '{' NUMBER '}'               								# repetitionumberobject
                    | '{' NUMBER? ',' NUMBER? '}'    							# repetitionMinMax
                    | '[' ']'													# onewordinbetween
                    | '[' ']' '{' NUMBER? ','? NUMBER? '}'						# onewordinbetweenMinMax
                    ;
                    
                    
NUMBER: [0-9]+;




/** "a numeral [-]?(.[0-9]+ | [0-9]+(.[0-9]*)? )" */
NUM      :   '-'? ('.' DIGIT+ | DIGIT+ ('.' DIGIT*)? ) ;
fragment
DIGIT       :   [0-9] ;

/** "any double-quoted string ("...") possibly containing escaped quotes" */
STRING      :   '"' ('\\"'|.)*? '"' ;

/** "Any string of alphabetic ([a-zA-Z\200-\377]) characters, underscores
 *  ('_') or digits ([0-9]), not beginning with a digit"
 */
ID          :   LETTER (LETTER|DIGIT)*;
fragment
LETTER      :   [a-zA-Z\u0080-\u00FF_] ;


operator : LESS | MORE | MOREEQUAL | LESSEQUAL | EQUAL | NOTEQUAL | LESSNOTEQUAL
			| MORENOTEQUAL | EQUALREGULAR | NOTEQUALREGULAR 
			;

LESS :                      '<' ;
MORE :                      '>' ;
MOREEQUAL :              	'>=' ;
LESSEQUAL :              	'<=' ;
EQUALREGULAR :             	'=' ; 
NOTEQUALREGULAR :		    '!=' ;
LESSNOTEQUAL :				'!<=' ;
MORENOTEQUAL :				'!>=' ;
EQUAL :						'==' ;
NOTEQUAL :					'!==' ;


WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines



