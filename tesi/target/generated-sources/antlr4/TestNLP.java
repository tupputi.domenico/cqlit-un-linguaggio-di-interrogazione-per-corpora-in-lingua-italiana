import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import di.uniba.it.nlpita.ItalianTextProcessing;
import di.uniba.it.nlpita.Token;
import di.uniba.it.nlpita.sentiment.SentixAPI;
import di.uniba.it.nlpita.sentiment.WNAffectAPI;
import di.uniba.it.nlpita.sentiment.WordNetFreqAPI;

public class TestNLP {

    public List<Token> test(String t) {
    	List<Token> out = new ArrayList<Token>();
        try {
            ItalianTextProcessing instance = ItalianTextProcessing.getInstance("it_nlp/");
            List<List<Token>> processedText = instance.processText(t);
            WordNetFreqAPI wnapi = new WordNetFreqAPI();
            wnapi.init(new File("it_nlp/sentiment/index.sense.gz"));
            SentixAPI sentapi = new SentixAPI(wnapi);
            sentapi.init(new File("it_nlp/sentiment/sentix.gz"));
            WNAffectAPI affectapi = new WNAffectAPI();
            affectapi.init(new File("it_nlp/sentiment/wn_affect.lex"));
            for (List<Token> list : processedText) {
                affectapi.getAffectMap(list);
                for (Token token : list) {
                    boolean add = out.add(token);
                }

            }

            
           
        } catch (Exception ex) {
            Logger.getLogger(TestNLP.class.getName()).log(Level.SEVERE, null, ex);
        }

       return out;
		
    }

}
