import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Stream;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import di.uniba.it.nlpita.Token;

public class processNLP {

	public int sentence = 0, numToken = 0;

	public ArrayList<String> trasform() throws IOException {
		File fileDir = new File("C:\\Users\\Domenico\\Desktop\\paisa.raw.utf8\\paisa.raw.utf8");

		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF-8"));

		String str = new String();
		String temp = new String();
		List<Integer> id = new ArrayList<Integer>();

		while ((temp = in.readLine()) != null) {

			if (temp.startsWith("<text id=\"")) {

				str = temp.substring(10, 17);
				id.add(Integer.parseInt(str));
				Collections.shuffle(id);
			}

		}

		in.close();

		boolean flag = false;
		FileInputStream f = new FileInputStream(fileDir);
		BufferedReader doc = new BufferedReader(new InputStreamReader(f, "UTF-8"));

		ArrayList<String> output = new ArrayList<String>();
		int i = 0;
		int totsent = 0;
		String text = new String();
		List<Integer> part = new ArrayList<Integer>();
		part = id.subList(0, 10000);
		int cont = 0;
		f.getChannel().position(0);
		while (((temp = doc.readLine()) != null) && (cont != (part.size() + 1))) {

			if (temp.startsWith("<text id=\"")) {

				str = temp.substring(10, 17);
				if (part.contains(Integer.parseInt(str))) {
					flag = true;
					Object obj = Integer.parseInt(str);
					part.remove(obj);
					cont++;
					continue;
				}
			} else if (temp.startsWith("</text>") && (flag == true)) {
				flag = false;
				output.add(i, text);
				i++;
				text = "";
				f.getChannel().position(0);
				doc = new BufferedReader(new InputStreamReader(f, "UTF-8"));
			}
			if (flag == true) {
				text = text + temp;
				sentence++;
			}

		}

		doc.close();

		return output;

	}

	public void writeJsonStream() throws IOException {

		processNLP g = new processNLP();

		int i = 0;
		int tot = 0;
		ArrayList<String> test = g.trasform();
		while (i < test.size()) {
			File file = new File("C:\\Users\\Domenico\\Desktop\\DataSet\\10000doc\\doc" + i + ".json");
			file.createNewFile();
			FileOutputStream fileout = new FileOutputStream(file);
			OutputStream out = fileout;

			TestNLP nlp = new TestNLP();
			List<Token> list = nlp.test(test.get(i));
			numToken = list.size();
			tot = tot + numToken;
			System.out.println("\n\nParole: " + numToken + "\nFrasi: " + g.sentence);

			JsonWriter writer = new JsonWriter(new OutputStreamWriter(out, "UTF-8"));
			Gson gson = new Gson();
			writer.setIndent("  ");
			writer.beginArray();
			for (Token message : list) {
				gson.toJson(message, Token.class, writer);
			}
			writer.endArray();
			writer.close();
			i++;
		}
		System.out.println("\n\nParole: " + tot + "\nFrasi: " + g.sentence);

	}

	public List<Token> readJsonStream(int i) throws IOException {
		File file = new File("C:\\Users\\Domenico\\Desktop\\DataSet\\10000doc\\doc" + i + ".json");
		FileInputStream filein = new FileInputStream(file);
		InputStream in = filein;
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new InputStreamReader(in, "UTF-8"));
		List<Token> list = new ArrayList<Token>();
		reader.beginArray();
		while (reader.hasNext()) {
			Token message = gson.fromJson(reader, Token.class);
			list.add(message);
		}
		reader.endArray();
		reader.close();
		return list;
	}

	public static void main(String[] args) throws IOException {
		processNLP g = new processNLP();

		g.writeJsonStream();

		// long time = System.currentTimeMillis();
		// System.out.println(g.readJsonStream());
		// System.out.println("\n\nTime: " + (System.currentTimeMillis() - time));
	}

}
