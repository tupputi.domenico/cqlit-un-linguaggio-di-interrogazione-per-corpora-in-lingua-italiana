import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.RuleNode;

import di.uniba.it.nlpita.Token;

public class controlMatch {

	public void match(String query) throws IOException {

		ANTLRInputStream input = new ANTLRInputStream(query);

		cqlLexer lexer = new cqlLexer(input);

		CommonTokenStream tokens = new CommonTokenStream(lexer);

		cqlParser parser = new cqlParser(tokens);

		ParseTree tree = parser.r(); // ottiene la radice dell'albero

		ParseTreeWalker walker = new ParseTreeWalker();
		
		
		long read = System.currentTimeMillis();
		long totread = 0;
		
		File dir = new File("C:\\Users\\Domenico\\Desktop\\DataSet\\10000doc");
		int length = dir.listFiles().length;
		long[] temp = new long[length];
		int i = 0;
		
		long read2 = System.currentTimeMillis() - read;
		totread = totread + read2;
		
		while (i < length) {
			
			read = System.currentTimeMillis();
			processNLP g = new processNLP();
			List<Token> list = g.readJsonStream(i);
			read2 = System.currentTimeMillis() - read;
			totread = totread + read2;
			
			read = System.currentTimeMillis();

			listenerCql vis = new listenerCql(list);
			walker.walk(vis, tree);
			
			read2 = System.currentTimeMillis() - read;
			temp[i] = read2;
			i++;
		}
		System.out.println("finish");
		
		long somma = 0;
		for(int z = 0; z<temp.length;z++) {
			somma = somma + temp[z];
		}
		
		System.out.println("\nTempo match medio documento: " + (somma /temp.length));
		System.out.println("\nTempo medio lettura file: " + (totread/temp.length));
		System.out.println("\nTempo totale esecuzione: " + (totread+somma));


	}

	public static void main(String[] args) throws IOException {
		// String cqlquery = "[depinfo = \"[0-9a-zA-Z_]*\"]";
		// String cqlquery = "[stopword == \"true\"]* [morphoinfo == \"VER.*\" | morphoinfo == \"ART.*\"]{1,12} [postag != \"CS\"][]{9}[lemma >= \".*a\"]{2} [depinfo = \"[0-9a-zA-Z_]*\"]";
		// String cqlquery = "[stopword == \" false\"][] [(pos == \"OTHER\" | pos == \"NOUN\") & lemma < \"gi\"]{,9} [token = \"[a-l]*\"]+ [morphoinfo !== \"PRE\" | token >= \"c.*\"]{2,5} [chunk == \"B-NP\"][]{2,5} [commonword == \"true\"]{,3}";

		controlMatch m = new controlMatch();
		String cqlquery = "[stopword == \" false\"][] [(pos == \"OTHER\" | pos == \"NOUN\") & lemma < \"gi\"]{,9} [token = \"[a-l]*\"]+ [morphoinfo !== \"PRE\" | token >= \"c.*\"]{2,5} [chunk == \"B-NP\"][]{2,5} [commonword == \"true\"]{,3}";
		m.match(cqlquery);
	}

}