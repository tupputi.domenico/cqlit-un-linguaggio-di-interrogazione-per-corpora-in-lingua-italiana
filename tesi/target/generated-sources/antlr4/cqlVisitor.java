// Generated from cql.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link cqlParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface cqlVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link cqlParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull cqlParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull cqlParser.OrContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repetitionumberobject}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepetitionumberobject(@NotNull cqlParser.RepetitionumberobjectContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repetitionZeroOrOne}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepetitionZeroOrOne(@NotNull cqlParser.RepetitionZeroOrOneContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperator(@NotNull cqlParser.OperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code onewordinbetweenMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOnewordinbetweenMinMax(@NotNull cqlParser.OnewordinbetweenMinMaxContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#r}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitR(@NotNull cqlParser.RContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot(@NotNull cqlParser.NotContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#clausepart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClausepart(@NotNull cqlParser.ClausepartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code onewordinbetween}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOnewordinbetween(@NotNull cqlParser.OnewordinbetweenContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#and}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd(@NotNull cqlParser.AndContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repetitionZeroOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepetitionZeroOrMore(@NotNull cqlParser.RepetitionZeroOrMoreContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repetitionMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepetitionMinMax(@NotNull cqlParser.RepetitionMinMaxContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#attribute}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttribute(@NotNull cqlParser.AttributeContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtom(@NotNull cqlParser.AtomContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(@NotNull cqlParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link cqlParser#expressionpart}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionpart(@NotNull cqlParser.ExpressionpartContext ctx);
	/**
	 * Visit a parse tree produced by the {@code repetitionOneOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRepetitionOneOrMore(@NotNull cqlParser.RepetitionOneOrMoreContext ctx);
}