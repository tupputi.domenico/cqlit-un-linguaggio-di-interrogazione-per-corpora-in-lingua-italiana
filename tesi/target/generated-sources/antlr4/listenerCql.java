import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import di.uniba.it.nlpita.Token;
import dk.brics.automaton.Automaton;
import dk.brics.automaton.RegExp;

public class listenerCql extends cqlBaseListener {

	private Token tok;
	private List<Token> tokenList;
	public boolean flag = false;

	listenerCql(List<Token> token) {
		tokenList = token;
	}

	private boolean valutaExpr(cqlParser.ExpressionContext ctx) {

		boolean flagor = valutaAnd(ctx.or().and(0));
		for (int i = 1; i < (ctx.or().and().size()) && (ctx.or().and().size() > 0); i++) {
			boolean flagpart = valutaAnd(ctx.or().and(i));
			flagor = flagor || flagpart;
		}

		return flagor;
	}

	private boolean valutaAnd(cqlParser.AndContext ctx) {

		boolean flagand = valutaNot(ctx.not(0));
		for (int i = 1; i < (ctx.not().size()) && (ctx.not().size() > 0); i++) {
			boolean flagpart = valutaNot(ctx.not(i));
			flagand = flagand && flagpart;
		}

		return flagand;

	}

	private boolean valutaNot(cqlParser.NotContext ctx) {

		boolean flagnot = valutaAtom(ctx.atom());

		if (ctx.NOT() != null) {
			if (flagnot == false) {
				flagnot = true;
			} else if (flagnot == true) {
				flagnot = false;
			}
		}

		return flagnot;
	}

	private boolean valutaAtom(cqlParser.AtomContext ctx) {

		boolean flagatom = false;
		if (ctx.clausepart() != null) {

			flagatom = valutaClause(ctx.clausepart());

		} else if (ctx.expression() != null) {
			flagatom = valutaExpr(ctx.expression());
		}

		return flagatom;
	}

	private boolean valutaClause(cqlParser.ClausepartContext ctx) {

		boolean flagclause = false;
		if (ctx.getChild(0).getText().equals("token")) {

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getToken().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getToken().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getToken().toString().startsWith(temp[0]);
					f2 = tok.getToken().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getToken().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getToken().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getToken().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getToken().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("postag"))

		{

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getPostag().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getPostag().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getPostag().toString().startsWith(temp[0]);
					f2 = tok.getPostag().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getPostag().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPostag().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getPostag().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getPostag().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("pos")) {

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getPos().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getPos().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getPos().toString().startsWith(temp[0]);
					f2 = tok.getPos().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getPos().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getPos().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getPos().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getPos().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("lemma")) {

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getLemma().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getLemma().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getLemma().toString().startsWith(temp[0]);
					f2 = tok.getLemma().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getLemma().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getLemma().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getLemma().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getLemma().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("morphoinfo")) {

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getMorphoInfo().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getMorphoInfo().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getMorphoInfo().toString().startsWith(temp[0]);
					f2 = tok.getMorphoInfo().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getMorphoInfo().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getMorphoInfo().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getMorphoInfo().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getMorphoInfo().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("chunk")) {

			if (ctx.operator().EQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getChunk().toString().startsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					flagclause = tok.getChunk().toString().endsWith(temp[0]);
				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					boolean f1 = false, f2 = false;
					f1 = tok.getChunk().toString().startsWith(temp[0]);
					f2 = tok.getChunk().toString().endsWith(temp[1]);
					flagclause = f1 && f2;
				} else {
					flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getChunk().toString());
				}

			} else if (ctx.operator().LESS() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().MORE() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val < 0) {
						flagclause = true;
					}
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().LESSEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val != 0) {
						flagclause = true;
					} else {
						flagclause = false;
					}
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val > 0) {
						flagclause = false;
					} else if (val <= 0) {
						flagclause = true;
					}
				}
			} else if (ctx.operator().MORENOTEQUAL() != null) {
				if (ctx.value().getText().replace("\"", "").trim().endsWith(".*")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else if (ctx.value().getText().replace("\"", "").trim().startsWith(".*")) {

					flagclause = true;

				} else if (ctx.value().getText().replace("\"", "").trim().contains(".")) {
					String temp[] = ctx.value().getText().replace("\"", "").trim().split("\\.");
					int val = temp[0].compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				} else {
					int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getChunk().toString());
					if (val >= 0) {
						flagclause = true;
					} else if (val < 0) {
						flagclause = false;
					}
				}
			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getChunk().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getChunk().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("stopword")) {

			if (ctx.operator().EQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isStopWord()));

			} else if (ctx.operator().NOTEQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isStopWord()));
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim()
						.equals(String.valueOf(tok.getToken().toString()));

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isStopWord()));
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}
			}

		} else if (ctx.getChild(0).getText().equals("commonword")) {

			if (ctx.operator().EQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isCommonWord()));

			} else if (ctx.operator().NOTEQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isCommonWord()));
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isCommonWord()));

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = ctx.value().getText().replace("\"", "").trim().equals(String.valueOf(tok.isCommonWord()));
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}
			}

		} else if (ctx.getChild(0).getText().equals("polarity")) {

			if (ctx.operator().EQUAL() != null) {

				if (Integer.parseInt(ctx.value().toString()) == tok.getPolarity()) {
					flagclause = true;
				} else if (Integer.parseInt(ctx.value().toString()) < tok.getPolarity()) {
					flagclause = false;
				}

			} else if (ctx.operator().LESS() != null) {
				if (Integer.parseInt(ctx.value().toString()) > tok.getPolarity()) {
					flagclause = false;
				} else if (Integer.parseInt(ctx.value().toString()) < tok.getPolarity()) {
					flagclause = true;
				}

			} else if (ctx.operator().MORE() != null) {
				if (Integer.parseInt(ctx.value().toString()) > tok.getPolarity()) {
					flagclause = true;
				} else if (Integer.parseInt(ctx.value().toString()) < tok.getPolarity()) {
					flagclause = false;
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				if (Integer.parseInt(ctx.value().toString()) >= tok.getPolarity()) {
					flagclause = true;
				} else if (Integer.parseInt(ctx.value().toString()) < tok.getPolarity()) {
					flagclause = false;
				}

			} else if (ctx.operator().LESSEQUAL() != null) {
				if (Integer.parseInt(ctx.value().toString()) > tok.getPolarity()) {
					flagclause = false;
				} else if (Integer.parseInt(ctx.value().toString()) <= tok.getPolarity()) {
					flagclause = true;
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				if (Integer.parseInt(ctx.value().toString()) != tok.getPolarity()) {
					flagclause = true;
				} else {
					flagclause = false;
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {

				if (Integer.parseInt(ctx.value().toString()) > tok.getPolarity()) {
					flagclause = true;
				} else if (Integer.parseInt(ctx.value().toString()) <= tok.getPolarity()) {
					flagclause = false;
				}

			} else if (ctx.operator().MORENOTEQUAL() != null) {

				if (Integer.parseInt(ctx.value().toString()) >= tok.getPolarity()) {
					flagclause = false;
				} else if (Integer.parseInt(ctx.value().toString()) < tok.getPolarity()) {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {
				if (Integer.parseInt(ctx.value().toString()) == tok.getPolarity()) {
					flagclause = true;
				} else {
					flagclause = false;
				}

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				if (Integer.parseInt(ctx.value().toString()) != tok.getPolarity()) {
					flagclause = true;
				} else {
					flagclause = false;
				}
			}

		} else if (ctx.getChild(0).getText().equals("emolabels")) {

			if (ctx.operator().EQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getEmolabels().toString());

			} else if (ctx.operator().LESS() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().MORE() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val >= 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().LESSEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val <= 0) {
					flagclause = true;
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val != 0) {
					flagclause = true;
				} else {
					flagclause = false;
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val <= 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MORENOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getEmolabels().toString());
				if (val >= 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getEmolabels().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getEmolabels().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("iob2")) {

			if (ctx.operator().EQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getIob2().toString());

			} else if (ctx.operator().LESS() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().MORE() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val >= 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().LESSEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val <= 0) {
					flagclause = true;
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val != 0) {
					flagclause = true;
				} else {
					flagclause = false;
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val <= 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MORENOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getIob2().toString());
				if (val >= 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getIob2().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getIob2().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		} else if (ctx.getChild(0).getText().equals("depinfo")) {

			if (ctx.operator().EQUAL() != null) {
				flagclause = ctx.value().getText().replace("\"", "").trim().equals(tok.getDepInfo().toString());

			} else if (ctx.operator().LESS() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().MORE() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MOREEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val >= 0) {
					flagclause = true;
				} else if (val < 0) {
					flagclause = false;
				}

			} else if (ctx.operator().LESSEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val > 0) {
					flagclause = false;
				} else if (val <= 0) {
					flagclause = true;
				}

			} else if (ctx.operator().NOTEQUAL() != null) {
				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val != 0) {
					flagclause = true;
				} else {
					flagclause = false;
				}

			} else if (ctx.operator().LESSNOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val > 0) {
					flagclause = true;
				} else if (val <= 0) {
					flagclause = false;
				}

			} else if (ctx.operator().MORENOTEQUAL() != null) {

				int val = ctx.value().getText().replace("\"", "").trim().compareTo(tok.getDepInfo().toString());
				if (val >= 0) {
					flagclause = false;
				} else if (val < 0) {
					flagclause = true;
				}

			} else if (ctx.operator().EQUALREGULAR() != null) {

				flagclause = tok.getDepInfo().toString().matches(ctx.value().getText().replace("\"", "").trim());

			} else if (ctx.operator().NOTEQUALREGULAR() != null) {

				flagclause = tok.getDepInfo().toString().matches(ctx.value().getText().replace("\"", "").trim());
				if (flagclause == true) {
					flagclause = false;
				} else {
					flagclause = true;
				}

			}

		}

		return flagclause;
	}

	public void enterR(cqlParser.RContext ctx) {

		String str = new String();
		boolean match = false;
		int brackmin = 0, brackmax = 0;
		for (int i = 0; i < ctx.expressionpart().size(); i++) {
			str = str + i;
			if (ctx.expressionpart(i).repetition() != null) {
				if (ctx.expressionpart(i).repetition().getText().equals("*")) {
					str = str + '{' + '0' + ',' + 10 + '}';
				} else if (ctx.expressionpart(i).repetition().getText().equals("+")) {
					str = str + '{' + '1' + ',' + 10 + '}';
				} else if (ctx.expressionpart(i).repetition().getText().equals("?")) {
					str = str + '{' + '0' + ',' + '1' + '}';
				} else if (ctx.expressionpart(i).repetition().getChild(0).getText().equals("{")
						&& ctx.expressionpart(i).repetition().getChild(1).getText().equals(",")) {
					int v = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(2).getText());
					str = str + '{' + '0' + ',' + v + '}';
				} else if (ctx.expressionpart(i).repetition().getChild(0).getText().equals("{")
						&& ctx.expressionpart(i).repetition().getChild(2).getText().equals(",")
						&& ctx.expressionpart(i).repetition().getChild(3).getText().equals("}")) {
					int v = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(1).getText());
					str = str + '{' + v + ',' + 10 + '}';
				} else if ((ctx.expressionpart(i).repetition().getChild(0).getText().equals("["))
						&& (ctx.expressionpart(i).repetition().getText().length() == 2)) {

					str = str + "<" + ctx.expressionpart().size() + '-' + ctx.expressionpart().size() + ">" + '{' + '1'
							+ '}';
				} else if ((ctx.expressionpart(i).repetition().getChild(0).getText().equals("["))
						&& (ctx.expressionpart(i).repetition().getText().length() == 7)) {
					brackmin = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(3).getText());
					brackmax = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(5).getText());
					str = str + "<" + ctx.expressionpart().size() + '-' + ctx.expressionpart().size() + ">" + '{'
							+ brackmin + ',' + brackmax + '}';
				} else if ((ctx.expressionpart(i).repetition().getChild(0).getText().equals("["))
						&& (ctx.expressionpart(i).repetition().getText().length() == 6)
						&& (!(ctx.expressionpart(i).repetition().getChild(3).getText().equals(",")))) {
					brackmin = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(3).getText());
					brackmax = 10;

					str = str + "<" + ctx.expressionpart().size() + '-' + ctx.expressionpart().size() + ">" + '{'
							+ brackmin + ',' + brackmax + '}';
				} else if ((ctx.expressionpart(i).repetition().getChild(0).getText().equals("["))
						&& (ctx.expressionpart(i).repetition().getText().length() == 6)
						&& (ctx.expressionpart(i).repetition().getChild(3).getText().equals(","))) {
					brackmin = 0;
					brackmax = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(4).getText());
					str = str + "<" + ctx.expressionpart().size() + '-' + ctx.expressionpart().size() + ">" + '{'
							+ brackmin + ',' + brackmax + '}';
				} else if ((ctx.expressionpart(i).repetition().getChild(0).getText().equals("["))
						&& (ctx.expressionpart(i).repetition().getText().length() == 5)) {
					brackmin = Integer.parseInt(ctx.expressionpart(i).repetition().getChild(3).getText());
					str = str + "<" + ctx.expressionpart().size() + '-' + ctx.expressionpart().size() + ">" + '{'
							+ brackmin + '}';
				} else if (ctx.expressionpart(i).repetition().getChild(0).getText().equals("{")) {
					str = str + ctx.expressionpart(i).repetition().getText();
				}
			}
		}

		RegExp r = new RegExp(str);
		Automaton a = r.toAutomaton();

		int z = 0, start = 0, finish = 0;

		do {
			Set<String> s = a.getFiniteStrings();

			Iterator<String> iterator = s.iterator();
			
			while (iterator.hasNext()) {
				if (s.isEmpty()) {
					break;
				}
				String setElement = iterator.next();
				int index = 0, i = 0;

				if (match == true) {
					break;
				}
				for (z = start; z < tokenList.size(); z++) {
					tok = tokenList.get(z);
					i = Integer.parseInt(String.valueOf(setElement.charAt(index)));
					if (i > ctx.expressionpart().size() - 1) {
						

					} else {
						flag = valutaExpr(ctx.expressionpart(i).expression());
						if (flag == false) {
							MyPredicate<String> filter = new MyPredicate<>();
							filter.var1 = setElement.charAt(index);
							filter.var2 = index;
							s.removeIf(filter);
							iterator = s.iterator();
						}
					}

					if (flag == false) {
						break;
					} else if (flag == true) {
						index++;
					}

					if (setElement.length() == index) {
						if (flag == true) {
							if (finish <= z) {
								finish = z;
								match = true;
							}
							break;
						}
					}

				}

			}
			if ((start <= finish) && ((tokenList.size() - start) >= ctx.expressionpart().size()) && (match == true)) {
				System.out.println("\nMatch: start " + start + " to " + finish); // prints: true
				match = false;
			} else if (start > finish) {
				finish++;
			}
			start = finish + 1;
		} while (start < tokenList.size());
	}

}

class MyPredicate<String> implements Predicate<String> {
	char var1;
	int var2;

	public boolean test(String var) {
		if ((var.toString().length() >= var2 + 1) && (var.toString().charAt(var2) == var1)) {
			return true;
		}
		return false;
	}
}
