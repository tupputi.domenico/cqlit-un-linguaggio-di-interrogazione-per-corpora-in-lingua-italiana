// Generated from cql.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link cqlParser}.
 */
public interface cqlListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link cqlParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull cqlParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull cqlParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#or}.
	 * @param ctx the parse tree
	 */
	void enterOr(@NotNull cqlParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#or}.
	 * @param ctx the parse tree
	 */
	void exitOr(@NotNull cqlParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repetitionumberobject}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterRepetitionumberobject(@NotNull cqlParser.RepetitionumberobjectContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repetitionumberobject}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitRepetitionumberobject(@NotNull cqlParser.RepetitionumberobjectContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repetitionZeroOrOne}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterRepetitionZeroOrOne(@NotNull cqlParser.RepetitionZeroOrOneContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repetitionZeroOrOne}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitRepetitionZeroOrOne(@NotNull cqlParser.RepetitionZeroOrOneContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#operator}.
	 * @param ctx the parse tree
	 */
	void enterOperator(@NotNull cqlParser.OperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#operator}.
	 * @param ctx the parse tree
	 */
	void exitOperator(@NotNull cqlParser.OperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code onewordinbetweenMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterOnewordinbetweenMinMax(@NotNull cqlParser.OnewordinbetweenMinMaxContext ctx);
	/**
	 * Exit a parse tree produced by the {@code onewordinbetweenMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitOnewordinbetweenMinMax(@NotNull cqlParser.OnewordinbetweenMinMaxContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#r}.
	 * @param ctx the parse tree
	 */
	void enterR(@NotNull cqlParser.RContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#r}.
	 * @param ctx the parse tree
	 */
	void exitR(@NotNull cqlParser.RContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#not}.
	 * @param ctx the parse tree
	 */
	void enterNot(@NotNull cqlParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#not}.
	 * @param ctx the parse tree
	 */
	void exitNot(@NotNull cqlParser.NotContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#clausepart}.
	 * @param ctx the parse tree
	 */
	void enterClausepart(@NotNull cqlParser.ClausepartContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#clausepart}.
	 * @param ctx the parse tree
	 */
	void exitClausepart(@NotNull cqlParser.ClausepartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code onewordinbetween}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterOnewordinbetween(@NotNull cqlParser.OnewordinbetweenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code onewordinbetween}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitOnewordinbetween(@NotNull cqlParser.OnewordinbetweenContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#and}.
	 * @param ctx the parse tree
	 */
	void enterAnd(@NotNull cqlParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#and}.
	 * @param ctx the parse tree
	 */
	void exitAnd(@NotNull cqlParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repetitionZeroOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterRepetitionZeroOrMore(@NotNull cqlParser.RepetitionZeroOrMoreContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repetitionZeroOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitRepetitionZeroOrMore(@NotNull cqlParser.RepetitionZeroOrMoreContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repetitionMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterRepetitionMinMax(@NotNull cqlParser.RepetitionMinMaxContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repetitionMinMax}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitRepetitionMinMax(@NotNull cqlParser.RepetitionMinMaxContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(@NotNull cqlParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(@NotNull cqlParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(@NotNull cqlParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(@NotNull cqlParser.AtomContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(@NotNull cqlParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(@NotNull cqlParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link cqlParser#expressionpart}.
	 * @param ctx the parse tree
	 */
	void enterExpressionpart(@NotNull cqlParser.ExpressionpartContext ctx);
	/**
	 * Exit a parse tree produced by {@link cqlParser#expressionpart}.
	 * @param ctx the parse tree
	 */
	void exitExpressionpart(@NotNull cqlParser.ExpressionpartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code repetitionOneOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void enterRepetitionOneOrMore(@NotNull cqlParser.RepetitionOneOrMoreContext ctx);
	/**
	 * Exit a parse tree produced by the {@code repetitionOneOrMore}
	 * labeled alternative in {@link cqlParser#repetition}.
	 * @param ctx the parse tree
	 */
	void exitRepetitionOneOrMore(@NotNull cqlParser.RepetitionOneOrMoreContext ctx);
}