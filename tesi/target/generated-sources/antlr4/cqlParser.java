// Generated from cql.g4 by ANTLR 4.4
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class cqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__21=1, T__20=2, T__19=3, T__18=4, T__17=5, T__16=6, T__15=7, T__14=8, 
		T__13=9, T__12=10, T__11=11, T__10=12, T__9=13, T__8=14, T__7=15, T__6=16, 
		T__5=17, T__4=18, T__3=19, T__2=20, T__1=21, T__0=22, AND=23, OR=24, NOT=25, 
		NUMBER=26, NUM=27, STRING=28, ID=29, LESS=30, MORE=31, MOREEQUAL=32, LESSEQUAL=33, 
		EQUALREGULAR=34, NOTEQUALREGULAR=35, LESSNOTEQUAL=36, MORENOTEQUAL=37, 
		EQUAL=38, NOTEQUAL=39, WS=40;
	public static final String[] tokenNames = {
		"<INVALID>", "'stopword'", "'emolabels'", "'morphoinfo'", "'{'", "'['", 
		"'polarity'", "'chunk'", "'}'", "']'", "'?'", "'commonword'", "'token'", 
		"'postag'", "'lemma'", "'('", "')'", "'*'", "'+'", "','", "'iob2'", "'depinfo'", 
		"'pos'", "'&'", "'|'", "'!'", "NUMBER", "NUM", "STRING", "ID", "'<'", 
		"'>'", "'>='", "'<='", "'='", "'!='", "'!<='", "'!>='", "'=='", "'!=='", 
		"WS"
	};
	public static final int
		RULE_r = 0, RULE_expressionpart = 1, RULE_expression = 2, RULE_or = 3, 
		RULE_and = 4, RULE_not = 5, RULE_atom = 6, RULE_clausepart = 7, RULE_value = 8, 
		RULE_attribute = 9, RULE_repetition = 10, RULE_operator = 11;
	public static final String[] ruleNames = {
		"r", "expressionpart", "expression", "or", "and", "not", "atom", "clausepart", 
		"value", "attribute", "repetition", "operator"
	};

	@Override
	public String getGrammarFileName() { return "cql.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public cqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class RContext extends ParserRuleContext {
		public ExpressionpartContext expressionpart(int i) {
			return getRuleContext(ExpressionpartContext.class,i);
		}
		public List<ExpressionpartContext> expressionpart() {
			return getRuleContexts(ExpressionpartContext.class);
		}
		public RContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_r; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterR(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitR(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitR(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RContext r() throws RecognitionException {
		RContext _localctx = new RContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_r);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(24); expressionpart();
				}
				}
				setState(27); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__17 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionpartContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RepetitionContext repetition() {
			return getRuleContext(RepetitionContext.class,0);
		}
		public ExpressionpartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionpart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterExpressionpart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitExpressionpart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitExpressionpart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionpartContext expressionpart() throws RecognitionException {
		ExpressionpartContext _localctx = new ExpressionpartContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expressionpart);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(29); match(T__17);
			setState(30); expression();
			setState(31); match(T__13);
			setState(33);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				setState(32); repetition();
				}
				break;
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public OrContext or() {
			return getRuleContext(OrContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35); or();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrContext extends ParserRuleContext {
		public List<TerminalNode> OR() { return getTokens(cqlParser.OR); }
		public TerminalNode OR(int i) {
			return getToken(cqlParser.OR, i);
		}
		public AndContext and(int i) {
			return getRuleContext(AndContext.class,i);
		}
		public List<AndContext> and() {
			return getRuleContexts(AndContext.class);
		}
		public OrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_or; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitOr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrContext or() throws RecognitionException {
		OrContext _localctx = new OrContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_or);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37); and();
			setState(42);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==OR) {
				{
				{
				setState(38); match(OR);
				setState(39); and();
				}
				}
				setState(44);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AndContext extends ParserRuleContext {
		public TerminalNode AND(int i) {
			return getToken(cqlParser.AND, i);
		}
		public NotContext not(int i) {
			return getRuleContext(NotContext.class,i);
		}
		public List<NotContext> not() {
			return getRuleContexts(NotContext.class);
		}
		public List<TerminalNode> AND() { return getTokens(cqlParser.AND); }
		public AndContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_and; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterAnd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitAnd(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitAnd(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AndContext and() throws RecognitionException {
		AndContext _localctx = new AndContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_and);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(45); not();
			setState(50);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AND) {
				{
				{
				setState(46); match(AND);
				setState(47); not();
				}
				}
				setState(52);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotContext extends ParserRuleContext {
		public TerminalNode NOT() { return getToken(cqlParser.NOT, 0); }
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public NotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterNot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitNot(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitNot(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotContext not() throws RecognitionException {
		NotContext _localctx = new NotContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_not);
		try {
			setState(56);
			switch (_input.LA(1)) {
			case NOT:
				enterOuterAlt(_localctx, 1);
				{
				setState(53); match(NOT);
				setState(54); atom();
				}
				break;
			case T__21:
			case T__20:
			case T__19:
			case T__16:
			case T__15:
			case T__11:
			case T__10:
			case T__9:
			case T__8:
			case T__7:
			case T__2:
			case T__1:
			case T__0:
				enterOuterAlt(_localctx, 2);
				{
				setState(55); atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public ClausepartContext clausepart() {
			return getRuleContext(ClausepartContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitAtom(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitAtom(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_atom);
		try {
			setState(63);
			switch (_input.LA(1)) {
			case T__21:
			case T__20:
			case T__19:
			case T__16:
			case T__15:
			case T__11:
			case T__10:
			case T__9:
			case T__8:
			case T__2:
			case T__1:
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(58); clausepart();
				}
				break;
			case T__7:
				enterOuterAlt(_localctx, 2);
				{
				setState(59); match(T__7);
				setState(60); expression();
				setState(61); match(T__6);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClausepartContext extends ParserRuleContext {
		public AttributeContext attribute() {
			return getRuleContext(AttributeContext.class,0);
		}
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public OperatorContext operator() {
			return getRuleContext(OperatorContext.class,0);
		}
		public ClausepartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_clausepart; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterClausepart(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitClausepart(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitClausepart(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClausepartContext clausepart() throws RecognitionException {
		ClausepartContext _localctx = new ClausepartContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_clausepart);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(65); attribute();
			setState(66); operator();
			setState(67); value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(cqlParser.ID, 0); }
		public TerminalNode NUM() { return getToken(cqlParser.NUM, 0); }
		public TerminalNode STRING() { return getToken(cqlParser.STRING, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NUM) | (1L << STRING) | (1L << ID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeContext extends ParserRuleContext {
		public AttributeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attribute; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterAttribute(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitAttribute(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitAttribute(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeContext attribute() throws RecognitionException {
		AttributeContext _localctx = new AttributeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_attribute);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__21) | (1L << T__20) | (1L << T__19) | (1L << T__16) | (1L << T__15) | (1L << T__11) | (1L << T__10) | (1L << T__9) | (1L << T__8) | (1L << T__2) | (1L << T__1) | (1L << T__0))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepetitionContext extends ParserRuleContext {
		public RepetitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repetition; }
	 
		public RepetitionContext() { }
		public void copyFrom(RepetitionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OnewordinbetweenMinMaxContext extends RepetitionContext {
		public TerminalNode NUMBER(int i) {
			return getToken(cqlParser.NUMBER, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(cqlParser.NUMBER); }
		public OnewordinbetweenMinMaxContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterOnewordinbetweenMinMax(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitOnewordinbetweenMinMax(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitOnewordinbetweenMinMax(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OnewordinbetweenContext extends RepetitionContext {
		public OnewordinbetweenContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterOnewordinbetween(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitOnewordinbetween(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitOnewordinbetween(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepetitionumberobjectContext extends RepetitionContext {
		public TerminalNode NUMBER() { return getToken(cqlParser.NUMBER, 0); }
		public RepetitionumberobjectContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterRepetitionumberobject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitRepetitionumberobject(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitRepetitionumberobject(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepetitionZeroOrMoreContext extends RepetitionContext {
		public RepetitionZeroOrMoreContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterRepetitionZeroOrMore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitRepetitionZeroOrMore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitRepetitionZeroOrMore(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepetitionMinMaxContext extends RepetitionContext {
		public TerminalNode NUMBER(int i) {
			return getToken(cqlParser.NUMBER, i);
		}
		public List<TerminalNode> NUMBER() { return getTokens(cqlParser.NUMBER); }
		public RepetitionMinMaxContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterRepetitionMinMax(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitRepetitionMinMax(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitRepetitionMinMax(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepetitionZeroOrOneContext extends RepetitionContext {
		public RepetitionZeroOrOneContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterRepetitionZeroOrOne(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitRepetitionZeroOrOne(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitRepetitionZeroOrOne(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RepetitionOneOrMoreContext extends RepetitionContext {
		public RepetitionOneOrMoreContext(RepetitionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterRepetitionOneOrMore(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitRepetitionOneOrMore(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitRepetitionOneOrMore(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RepetitionContext repetition() throws RecognitionException {
		RepetitionContext _localctx = new RepetitionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_repetition);
		int _la;
		try {
			setState(103);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				_localctx = new RepetitionZeroOrMoreContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(73); match(T__5);
				}
				break;
			case 2:
				_localctx = new RepetitionOneOrMoreContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(74); match(T__4);
				}
				break;
			case 3:
				_localctx = new RepetitionZeroOrOneContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(75); match(T__12);
				}
				break;
			case 4:
				_localctx = new RepetitionumberobjectContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(76); match(T__18);
				setState(77); match(NUMBER);
				setState(78); match(T__14);
				}
				break;
			case 5:
				_localctx = new RepetitionMinMaxContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(79); match(T__18);
				setState(81);
				_la = _input.LA(1);
				if (_la==NUMBER) {
					{
					setState(80); match(NUMBER);
					}
				}

				setState(83); match(T__3);
				setState(85);
				_la = _input.LA(1);
				if (_la==NUMBER) {
					{
					setState(84); match(NUMBER);
					}
				}

				setState(87); match(T__14);
				}
				break;
			case 6:
				_localctx = new OnewordinbetweenContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(88); match(T__17);
				setState(89); match(T__13);
				}
				break;
			case 7:
				_localctx = new OnewordinbetweenMinMaxContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(90); match(T__17);
				setState(91); match(T__13);
				setState(92); match(T__18);
				setState(94);
				switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
				case 1:
					{
					setState(93); match(NUMBER);
					}
					break;
				}
				setState(97);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(96); match(T__3);
					}
				}

				setState(100);
				_la = _input.LA(1);
				if (_la==NUMBER) {
					{
					setState(99); match(NUMBER);
					}
				}

				setState(102); match(T__14);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OperatorContext extends ParserRuleContext {
		public TerminalNode LESS() { return getToken(cqlParser.LESS, 0); }
		public TerminalNode EQUAL() { return getToken(cqlParser.EQUAL, 0); }
		public TerminalNode EQUALREGULAR() { return getToken(cqlParser.EQUALREGULAR, 0); }
		public TerminalNode LESSEQUAL() { return getToken(cqlParser.LESSEQUAL, 0); }
		public TerminalNode MOREEQUAL() { return getToken(cqlParser.MOREEQUAL, 0); }
		public TerminalNode NOTEQUALREGULAR() { return getToken(cqlParser.NOTEQUALREGULAR, 0); }
		public TerminalNode MORE() { return getToken(cqlParser.MORE, 0); }
		public TerminalNode NOTEQUAL() { return getToken(cqlParser.NOTEQUAL, 0); }
		public TerminalNode LESSNOTEQUAL() { return getToken(cqlParser.LESSNOTEQUAL, 0); }
		public TerminalNode MORENOTEQUAL() { return getToken(cqlParser.MORENOTEQUAL, 0); }
		public OperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).enterOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof cqlListener ) ((cqlListener)listener).exitOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof cqlVisitor ) return ((cqlVisitor<? extends T>)visitor).visitOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperatorContext operator() throws RecognitionException {
		OperatorContext _localctx = new OperatorContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_operator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(105);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LESS) | (1L << MORE) | (1L << MOREEQUAL) | (1L << LESSEQUAL) | (1L << EQUALREGULAR) | (1L << NOTEQUALREGULAR) | (1L << LESSNOTEQUAL) | (1L << MORENOTEQUAL) | (1L << EQUAL) | (1L << NOTEQUAL))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3*n\4\2\t\2\4\3\t\3"+
		"\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f"+
		"\t\f\4\r\t\r\3\2\6\2\34\n\2\r\2\16\2\35\3\3\3\3\3\3\3\3\5\3$\n\3\3\4\3"+
		"\4\3\5\3\5\3\5\7\5+\n\5\f\5\16\5.\13\5\3\6\3\6\3\6\7\6\63\n\6\f\6\16\6"+
		"\66\13\6\3\7\3\7\3\7\5\7;\n\7\3\b\3\b\3\b\3\b\3\b\5\bB\n\b\3\t\3\t\3\t"+
		"\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\fT\n\f\3\f\3"+
		"\f\5\fX\n\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\fa\n\f\3\f\5\fd\n\f\3\f\5\f"+
		"g\n\f\3\f\5\fj\n\f\3\r\3\r\3\r\2\2\16\2\4\6\b\n\f\16\20\22\24\26\30\2"+
		"\5\3\2\35\37\6\2\3\5\b\t\r\20\26\30\3\2 )r\2\33\3\2\2\2\4\37\3\2\2\2\6"+
		"%\3\2\2\2\b\'\3\2\2\2\n/\3\2\2\2\f:\3\2\2\2\16A\3\2\2\2\20C\3\2\2\2\22"+
		"G\3\2\2\2\24I\3\2\2\2\26i\3\2\2\2\30k\3\2\2\2\32\34\5\4\3\2\33\32\3\2"+
		"\2\2\34\35\3\2\2\2\35\33\3\2\2\2\35\36\3\2\2\2\36\3\3\2\2\2\37 \7\7\2"+
		"\2 !\5\6\4\2!#\7\13\2\2\"$\5\26\f\2#\"\3\2\2\2#$\3\2\2\2$\5\3\2\2\2%&"+
		"\5\b\5\2&\7\3\2\2\2\',\5\n\6\2()\7\32\2\2)+\5\n\6\2*(\3\2\2\2+.\3\2\2"+
		"\2,*\3\2\2\2,-\3\2\2\2-\t\3\2\2\2.,\3\2\2\2/\64\5\f\7\2\60\61\7\31\2\2"+
		"\61\63\5\f\7\2\62\60\3\2\2\2\63\66\3\2\2\2\64\62\3\2\2\2\64\65\3\2\2\2"+
		"\65\13\3\2\2\2\66\64\3\2\2\2\678\7\33\2\28;\5\16\b\29;\5\16\b\2:\67\3"+
		"\2\2\2:9\3\2\2\2;\r\3\2\2\2<B\5\20\t\2=>\7\21\2\2>?\5\6\4\2?@\7\22\2\2"+
		"@B\3\2\2\2A<\3\2\2\2A=\3\2\2\2B\17\3\2\2\2CD\5\24\13\2DE\5\30\r\2EF\5"+
		"\22\n\2F\21\3\2\2\2GH\t\2\2\2H\23\3\2\2\2IJ\t\3\2\2J\25\3\2\2\2Kj\7\23"+
		"\2\2Lj\7\24\2\2Mj\7\f\2\2NO\7\6\2\2OP\7\34\2\2Pj\7\n\2\2QS\7\6\2\2RT\7"+
		"\34\2\2SR\3\2\2\2ST\3\2\2\2TU\3\2\2\2UW\7\25\2\2VX\7\34\2\2WV\3\2\2\2"+
		"WX\3\2\2\2XY\3\2\2\2Yj\7\n\2\2Z[\7\7\2\2[j\7\13\2\2\\]\7\7\2\2]^\7\13"+
		"\2\2^`\7\6\2\2_a\7\34\2\2`_\3\2\2\2`a\3\2\2\2ac\3\2\2\2bd\7\25\2\2cb\3"+
		"\2\2\2cd\3\2\2\2df\3\2\2\2eg\7\34\2\2fe\3\2\2\2fg\3\2\2\2gh\3\2\2\2hj"+
		"\7\n\2\2iK\3\2\2\2iL\3\2\2\2iM\3\2\2\2iN\3\2\2\2iQ\3\2\2\2iZ\3\2\2\2i"+
		"\\\3\2\2\2j\27\3\2\2\2kl\t\4\2\2l\31\3\2\2\2\16\35#,\64:ASW`cfi";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}