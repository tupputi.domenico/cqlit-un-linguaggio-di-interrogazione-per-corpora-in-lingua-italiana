n#10972097	core	respiro	emo
n#10875383	core	rimbecillimento rimbambimento istupidimento	cog
n#10428185	core	rischio insicurezza	att tra
n#10402349	core	confusione casino rivoluzione	cog
n#10396588	core	agio benessere agiatezza	sit
n#10392521	core	depressione demoralizzazione abbattimento sconforto accasciamento scoraggiamento	moo
n#10392337	core	disperazione	sit
n#10390190	core	fiducia	emo
n#10385041	core	afflizione	emo sit
n#10369117	core	sazietà	phy sen
n#10368450	core	fretta furia	att cog
n#10367071	core	ambascia angoscia angustia	emo
n#10366086	core	benessere	emo
n#10365835	core	malessere mal_essere	emo
n#10365730	core	disturbo	emo
n#10365540	core	disagio	emo
n#10364817	core	sollievo	emo
n#10361706	core	infamia	emo
n#10360768	core	avvilimento	emo
n#10360525	core	disonore obbrobrio vergogna ignominia onta disdoro infamia	emo
n#10359961	core	onta	emo
n#10358589	core	disistima	emo
n#10358350	core	considerazione riguardo stima	att
n#10344618	core	dissociazione	cog
n#10343521	core	isolamento	emo cog
n#10343430	core	isolamento	sit
n#10342359	core	benarrivato ben_venuto benvenuto accoglienza ben_arrivato	sit
n#10341656	core	benestare gradimento approvazione	beh
n#10340571	core	tensione	emo sen
n#10340224	core	impiccio pasticcio guaio scombussolamento imbroglio	sit
n#10339059	core	difficoltà intoppo	sit
n#10338400	core	insofferenza irrequietezza impazienza	att emo
n#10337915	core	molestia irritazione esasperazione dispetto fastidio	beh emo
n#10337390	core	panico	emo
n#10336704	core	euforia	moo
n#10336281	core	malinconia	emo
n#10335051	core	alterazione	emo
n#10326963	core	delirio	beh emo cog
n#10323024	core	delirio	cog
n#10316756	core	lucidità	cog
n#10314284	core	stress tensione	emo moo
n#10314018	core	logorio surmenage	sit
n#10313853	core	nervosità nervosismo	phy eds emo cog
n#10311493	core	anemia	beh
n#10311355	core	capogiro giramento vertigine giramento_di_testa giracapo	sen
n#10302574	core	nausea	emo
n#10285502	core	supplizio strazio cilicio tortura	emo
n#10279078	core	bua	sen
n#10278925	core	intorpidimento	phy cog sen
n#10257548	core	trauma ferita lesione danno male	beh
n#10207260	core	affezione malattia acciacco disturbo magagna malanno	emo moo
n#10191374	core	indisposizione malessere mal_essere	phy
n#10126933	core	esaurimento_nervoso	phy cog
n#10124557	core	dipendenza assuefazione	phy cog
n#10123988	core	malattia male	phy
n#10120211	core	radiosità fulgidezza	phy emo
n#10119788	core	energia dinamismo vitalità	tra
n#10112984	core	fame	phy
n#10112569	core	desiderio	beh
n#10111948	core	foia fregola calore	phy
n#10111097	core	eccitazione esaltazione animazione concitazione	sen
n#10110976	core	incazzatura rabbia furore incavolatura bile collera furia	emo
n#10110870	core	ira rabbia bile collera furia	emo
n#10107420	core	sopore addormentamento assopimento sonno sonnolenza	att cog
n#10101037	core	insonnia	phy
n#10097920	core	ubbriachezza ebbrezza ubriachezza ebrezza ubriacatura ubbriacatura ciucca sbornia inebriamento	phy cog
n#10096823	core	straccaggine stremezza spossamento straccamento stracchezza snervatezza abbattimento sfinimento spossatezza rifinitezza infiacchimento fiaccatura	phy
n#10096231	core	fiacca affaticamento stracca fatica stanchezza estenuazione fiacchezza	att cog
n#10094762	core	letargia letargo	phy sen
n#10094429	core	torpidezza intorpidimento letargo torpore	cog
n#10089688	core	calma tranquillità immobilità	phy sit
n#10088916	core	tremore tremito	res beh
n#10086957	core	conto assegnamento	cog
n#10083436	core	sudditanza soggezione soggiogamento assoggettamento	att
n#10078983	core	colpevolezza colpa reità	beh
n#10077799	core	malinconia mestizia infelicità tristezza	emo
n#10077637	core	inasprimento	emo
n#10077447	core	infelicità	emo
n#10076837	core	felicità beatitudine	emo
n#10076721	core	beatitudine	emo
n#10076479	core	felicità contentezza	emo
n#10075968	core	appagamento gratificazione compiacimento soddisfazione soddisfacimento	emo
n#10075760	core	solluchero estasi impeto sublimazione esaltazione rapimento sollucchero trasporto	emo
n#10075577	core	imbarazzo impaccio	emo
n#10071758	core	conflitto frizione maretta	beh
n#10071108	core	ostilità	att beh emo
n#10070966	core	furore	emo
n#10070620	core	fermento agitazione	emo
n#10070121	core	diavolio clamore cagnara putiferio bailamme buriana parapiglia tumulto	emo
n#10069389	core	sommovimento fermento turbamento agitazione tumulto	emo
n#10065762	core	affiatamento armonia accordo	att emo
n#10065501	core	calma	sit
n#10064713	core	pace	beh sit
n#10064397	core	calma tranquillità	sit
n#10058657	core	falsità	tra
n#10037333	core	confidenza	att emo
n#09769206	core	zero	emo
n#07832913	core	riluttanza	emo
n#07637393	core	peste terremoto	emo
n#07453274	core	rompicazzo rompitasche vescicante importuno rompiscatole scocciatore rottorio ossessione impiastro rogna seccatore gonfiatore antipatico noia empiastro piattola martellatore peste attaccabottoni piaga rompicoglioni rompimento	sen
n#07325495	core	speranza	emo
n#07121483	core	noioso pizza mattone cataplasma impiastro lagna noia pittima empiastro	tra
n#07109169	core	fiamma cocco caro amato amore diletto	tra
n#07064973	core	contradittore osteggiatore nemico oppugnatore contraddittore avversario antagonista contendente opponente rivale duellatore contrastante avversatore oppositore	tra
n#07059467	core	mostro asso cima cannone fenomeno fuoriclasse demonio mago	tra
n#06661976	core	malattia mania	att tra emo
n#06661163	core	impulso	cog
n#05617136	core	esuberanza	att tra
n#05616909	core	ardore impegno calore fervore	emo
n#05616745	core	ardore avidità	emo cog sen
n#05616611	core	esaltazione amore	emo
n#05616285	core	empatia	att tra
n#05615937	core	compassione misericordia mercé	emo
n#05615645	core	commiserazione misericordia pena	emo
n#05613629	core	umore	emo
n#05613388	core	malizia malvagità malignità	emo
n#05612865	core	gelosia	emo
n#05612539	core	invidia	emo
n#05611927	core	risentimento livore astio rancura mal_animo astiosità agro acredine malanimo fiele rancore amaro cruccio	emo
n#05611157	core	animosità	emo moo
n#05610898	core	inimicizia ostilità	att beh emo
n#05609785	core	odio	emo
n#05609011	core	considerazione rispetto stima	att
n#05608483	core	affezione bene attaccamento affetto amore	emo
n#05608223	core	cotta infatuazione_giovanile	emo
n#05608042	core	ardore passione	emo
n#05607724	core	amore	emo
n#05606636	core	disconforto sconsolazione disperazione sconforto costernazione	emo
n#05605777	core	frustrazione	emo
n#05605389	core	scontentezza dispiacere	emo
n#05605136	core	stanchezza noia tedio uggia	emo
n#05604874	core	scontento mal_umore insoddisfazione malumore malcontento	emo
n#05604436	core	mal_umore malumore	emo
n#05603932	core	scoramento magone abbacchiamento abbattimento	emo
n#05603809	core	vittimismo autocommiserazione	att emo
n#05603599	core	demoralizzazione frustrazione	moo
n#05602852	core	pentimento penitenza	emo
n#05602697	core	colpa senso_di_colpa	emo
n#05602526	core	rimorso rimordimento compunzione	emo
n#05602279	core	pentimento dispiacere rincrescimento	emo
n#05601776	core	struggicuore crepacuore struggimento	emo
n#05601647	core	pentimento attrizione	emo
n#05601413	core	dolore	emo
n#05600345	core	mestizia rammarico amarezza	emo
n#05599930	core	infelicità tristezza	emo
n#05599694	core	appagamento accontentamento contentatura godibilità contentamento soddisfacimento	emo
n#05599367	core	narcisismo autocompiacimento	emo
n#05599226	core	orgoglio	emo
n#05598833	core	contentezza	emo
n#05598350	core	giocondità allegria	moo
n#05597799	core	appartenenza	emo
n#05597617	core	giubilo rallegramento	emo
n#05597503	core	ilarità	beh emo
n#05597371	core	rallegranza gaiezza allegria allegrezza allegranza	att tra
n#05597250	core	euforia	emo
n#05596742	core	effervescenza	emo
n#05596527	core	giubilo tripudio esultanza giolito gioia esultazione gongolamento	beh emo
n#05596414	core	euforia	emo
n#05596218	core	gioia letizia gaudio	emo
n#05596105	core	gioia	emo
n#05595732	core	felicità lietezza contentezza gioiosità contento	emo
n#05595498	core	affidamento buona_fede buonafede fiducia fede	emo
n#05594446	core	inquietudine	emo
n#05594325	core	insicurezza	att tra
n#05594044	core	pensiero timore	emo
n#05593817	core	pensiero angoscia affanno cruccio angustia patema preoccupazione	emo
n#05593480	core	trepidazione ansia trepidanza apprensione angoscia ansietà affanno batticuore preoccupazione turbamento trepidezza	emo
n#05592914	core	ritrosia vergogna timidezza timidità	emo
n#05591481	core	venerazione	emo
n#05591212	core	orrore	emo
n#05591021	core	fremito brivido	sen
n#05590744	core	smarrimento allarme	emo
n#05590260	core	fifa spavento timore terrore paura	emo
n#05589957	core	esasperazione	emo
n#05589637	core	incocciatura	emo
n#05589430	core	irritazione esasperazione	emo
n#05589301	core	paturnie nervoso mal_umore stizza malumore paturne	moo
n#05588960	core	sdegno indignazione	emo
n#05588413	core	accanimento ira furore bile furia	emo
n#05587782	core	languore	emo sen
n#05587663	core	rilassamento abbandono	beh sen
n#05587497	core	calma serenità requie	emo moo
n#05587339	core	calma quiete tranquillità	beh emo sit
n#05586494	core	ribollimento foga oppure_agitazione concitamento concitazione ridda tumulto infervoramento	emo
n#05586117	core	impazienza	att emo
n#05585895	core	smania	emo
n#05585608	core	inquietudine irrequietezza imbizzarrimento agitazione	emo
n#05584398	core	genuinità	att tra emo
n#05584176	core	gaiezzza allegria	att tra
n#05583116	core	choc shock	emo
n#05583004	core	intontimento	emo
n#05582577	core	meraviglia	emo
n#05582358	core	sbigottimento sbalordimento sconcerto stupore stupefazione attonimento meraviglia	emo cog
n#05581967	core	umiltà	att tra beh
n#05581850	core	vanagloria boria vanità	tra beh
n#05581430	core	orgoglio	emo
n#05581180	core	modestia	att tra
n#05581054	core	confusione smarrimento	cog
n#05580938	core	mortificazione	emo
n#05580651	core	disagio	att
n#05580420	core	confusione imbarazzo vergogna	emo
n#05579963	core	vergogna	emo
n#05579830	core	ferocia crudeltà spietatezza inumanità	emo
n#05579569	core	distacco	att
n#05579322	core	indifferenza disamore disinteresse	att
n#05579184	core	indifferenza	emo
n#05578355	core	riconoscenza	att
n#05578169	core	riconoscenza gratitudine	emo
n#05578076	core	voltastomaco	emo
n#05577970	core	raccapriccio disgusto ripulsione ribrezzo sgomento orrore ripugnanza schifo repugnanza repulsione avversione	emo
n#05577810	core	abominazione orrore abborrimento odio abbominazione aborrimento	emo
n#05577676	core	disgusto ribrezzo ripugnanza repugnanza nausea	emo
n#05577544	core	dispregio disprezzo disdegno sdegno	att emo
n#05577411	core	riprovazione disapprovazione	emo
n#05577305	core	fobia disgusto idiosincrasia contrarietà ripugnanza renitenza repugnanza antipatia avversione	emo
n#05576323	core	antipatia	emo
n#05575841	core	ammirazione considerazione	emo
n#05574602	core	tendenza	att
n#05574157	core	debole	cog
n#05573285	core	gradimento	emo
n#05573093	core	tormento tormentone	emo
n#05572909	core	dolore accorazione accoramento angustia afflizione	emo
n#05572363	core	agonia tormento tortura	eds emo
n#05572211	core	dolore patimento pena male afflizione	eds
n#05570696	core	alleviamento sollievo refrigerio	emo
n#05569706	core	gusto	emo sen
n#05569487	core	godimento gusto divertimento	eds
n#05569390	core	divertimento	emo
n#05568539	core	fregola	cog
n#05568414	core	carnalità	emo
n#05568116	core	cupidigia concupiscenza desiderio voglia	cog sen
n#05567643	core	passione	att tra emo
n#05567241	core	amore passione	emo
n#05566935	core	concupiscenza	emo cog
n#05566463	core	nostalgia disio rimpianto desio	emo
n#05565891	core	anelito ansia bramosia brama	emo cog sen
n#05565534	core	dipendenza mania	cog
n#05565069	core	appetito appetenza desiderio	sen
n#05564936	core	desiderio	cog
n#05564760	core	tentazione	emo cog sen
n#05563906	core	languidezza	emo sen
n#05563705	core	indifferenza	att
n#05562060	core	zelo	att beh
n#05561864	core	fiamma ardore bollore vemenza fuoco calore fervore veemenza	emo
n#05560878	core	emozione commozione	psy
n#05559901	core	basimento deliquio sdilinquimento sturbo svenimento	res beh
n#05522458	core	scapito danno detrimento discapito	beh
n#05522274	core	disgrazia dannazione	emo
n#05489772	core	impiccio seccatura incomodo rogna scomodo fastidio briga cancan noia rottura	emo
n#05480231	core	alleviamento	emo
n#05463089	core	illanguidimento sfinitezza debilitazione snervamento	phy
n#05455349	core	infievolimento illanguidimento mitigamento affievolimento attenuamento raffreddamento	beh
n#05442564	core	sorpresa	sit
n#05436975	core	portento meraviglia prodigio meraviglioso	sit
n#05436632	core	briga	sit
n#05414101	core	dimissione	emo
n#05383758	core	diniego	beh
n#05368498	core	contrasto dissidio bega contestazione lite	cog
n#05364269	core	beneplacito benestare consenso assentimento assenso permesso acconsentimento adesione	beh
n#05149034	core	espressione manifestazione	psy
n#05094224	core	avvisatore allarme	emo
n#05069878	core	constatazione costatazione	cog
n#05053336	core	soluzione	att
n#05051677	core	giustificazione	beh
n#05040252	core	impertinenza insolenza arditezza impudenza	att tra beh
n#05037154	core	scherno sdegno vilipendio spregio sprezzo	att
n#05035457	core	censura	beh
n#05035217	core	rimbrotto appunto biasimo	beh
n#05032520	core	deplorazione riprovazione censura biasimo	att
n#05032286	core	condanna	beh
n#04976770	core	condoglianza condoglianze commiserazione	beh
n#04974979	core	ben_venuto benvenuto accoglienza	beh
n#04705429	core	reverenza riverenza	att
n#04704758	core	rispetto stima	att
n#04704180	core	meschinità grettezza	tra
n#04703628	core	intolleranza	att tra
n#04701864	core	ingiustizia	beh
n#04701316	core	pregiudizio preconcetto	att emo
n#04700409	core	deplorazione disapprovazione	att
n#04699755	core	set	att
n#04699267	core	convenzione comprensione	att
n#04697205	core	difensiva	att
n#04591399	core	disincanto disinganno disillusione	emo cog
n#04588033	core	disegno intendimento animo fine intento meta proposito mira finalità intenzione scopo pensamento	cog
n#04586506	core	scetticismo	att
n#04568851	core	apprensione	emo
n#04567625	core	pregiudizio prevenzione preconcetto	att emo
n#04563026	core	fiducia fede	att emo
n#04545382	core	intuito intuizione	emo cog
n#04544110	core	credenza impressione	cog
n#04492345	core	scorno malgrado dolore dispiacimento dispiacenza compianto spiacenza desolazione dispiacere rammaricamento	beh
n#04491326	core	pensiero inquietudine assillo fastidio affanno cruccio preoccupazione afflizione	emo
n#04490800	core	pallosità scocciatura seccatura ossessione rogna rompiballe scomodo briga disturbo noia rottura rompipalle fastidio piaga noiosità	sit
n#04490585	core	felicità delizia gioia diletto	emo
n#04485374	core	motivazione giustificazione	sit
n#04483505	core	riguardo rispetto	att
n#04478900	core	amore	emo
n#04464063	core	desiderio	beh
n#04461993	core	risoluzione	att
n#04459998	core	cogitazione riflessione elucubrazione raccoglimento meditazione	att beh
n#04437019	core	moda	emo
n#04422784	core	freddo	sen
n#04422689	core	caldo calore	sen
n#04421414	core	pizzicore prurito	sen
n#04420993	core	formicolio	sen
n#04416226	core	gusto sapore	emo sen
n#04413760	core	impressione	cog
n#04409766	core	intuito intuizione	cog
n#04408740	core	disattenzione distrazione	cog
n#04406313	core	cura	att
n#04403595	core	suspense	cog
n#04403483	core	tentennare esitazione tentennamento tentennio	beh cog
n#04403239	core	ondeggiamento titubanza barcollio indecisione irresoluzione pendolamento tentennio increspamento tentennare traballamento irrisoluzione vacillamento barcollamento vacillazione dondolamento	att
n#04403103	core	scetticismo miscredenza incredulità	cog
n#04402984	core	diffidenza	emo
n#04402548	core	dubbiosità dubbio forse problematicità incertezza	att emo cog
n#04402313	core	affidamento assegnamento	cog
n#04400046	core	allettamento esca lenocinio insidia pania adescamento	sit
n#04395081	core	casino guaio	sit
n#04393934	core	perplessità	cog
n#04393745	core	sfasamento smarrimento	cog
n#04390301	core	disorientamento stordimento	cog
n#04389032	core	coscienza	cog
n#04388620	core	senso	sen
n#04388082	core	autoconsapevolezza	att
n#04387608	core	coscienza consapevolezza	cog
n#04383528	core	entusiasmo	emo
n#04371175	core	senso	psy
n#04370050	core	attenzione	cog
n#04368468	core	insipienza dappocaggine	tra
n#04366760	core	tardività ritardo	tra
n#04366643	core	balordaggine ottusità scipitezza ebetaggine melensaggine scipitaggine	cog
n#04363549	core	abilità destrezza	tra beh
n#04358717	core	originalità novità	sen
n#04352214	core	sveltezza	tra
n#04349139	core	discrezione tatto saviezza	att tra
n#04055000	core	pessimismo	att tra
n#04051410	core	stucchevolezza tediosità	emo
n#04051149	core	insulsaggine	tra
n#04050713	core	debolezza	tra
n#04050476	core	impotenza	tra
n#04018800	core	superiorità	sit
n#04005196	core	vanità	tra beh
n#03951223	core	lentezza lungaggine ritardo	att cog
n#03949998	core	fretta furia	att cog
n#03949337	core	speditezza celerità subitaneità prestezza rapidezza rattezza rapidità subitezza fulmineità	tra
n#03935080	core	gracilità mala_voglia bolsaggine indebolimento malavoglia languore cascaggine fiacchezza mollezza	tra
n#03933665	core	ferocia impeto irruenza ferocità furia	emo
n#03933119	core	serietà gravità	att tra emo
n#03932475	core	energia dinamismo vigore	beh
n#03929458	core	resistenza	tra
n#03917468	core	caldo calore	sen
n#03917136	core	frescura fresco rezzo orezzo	sen sit
n#03916773	core	gelo freddo	att tra cog
n#03909434	core	legnosità	beh
n#03908956	core	sgraziataggine goffaggine	beh
n#03902140	core	agrezzza acerbità	sit
n#03893757	core	silenzio quiete	sit
n#03890129	core	pallore lividezza	res
n#03868242	core	sete fame	cog
n#03863677	core	floscezza flaccidezza afflosciamento moscezza	sit
n#03855313	core	freschezza	sen
n#03848510	core	insistenza insolenza ardire procacità arditezza impudenza petulanza sfacciataggine sfrontatezza improntitudine	att tra beh
n#03847224	core	sgarberia sgarbatezza villania	beh
n#03846897	core	deferenza ossequiosità	att
n#03843490	core	cocciutaggine caparbietà caponaggine zucconaggine testardaggine	att tra
n#03841705	core	remissività sommissione cedevolezza	tra
n#03841543	core	ubbidienza	tra
n#03841165	core	arrendevolezza docilità mansuetudine	att tra
n#03839746	core	calma flemma compostezza placidità	tra
n#03834482	core	diffidenza sospettosità	tra
n#03834330	core	diffidenza sfiducia	emo
n#03833898	core	fiducia	tra
n#03833126	core	imprudenza avventatezza svagatezza	tra
n#03832040	core	previdenza prudenza accortezza lungimiranza circospezione cautezza avvedutezza cautela oculatezza giudiziosità	beh
n#03830857	core	prudenza precauzione circospezione cautela	tra
n#03829940	core	mansuetudine	att tra beh
n#03829724	core	modestia semplicità umiltà	att tra beh
n#03829111	core	degnazione	tra
n#03828648	core	despotismo spocchia boria prepotenza strafottenza tracotanza sdegnosità albagia burbanza insolenza sostenutezza orgogliosità sfrontatezza superbia alterigia altezzosità muffosità sussiego presupponenza sicumera tronfiezza oltracotanza arroganza grandigia dispotismo altura iattanza boriosità protervia	tra
n#03828045	core	boria vanità	tra beh
n#03825629	core	inibizione	cog
n#03822374	core	sfarfallamento	tra
n#03821553	core	perseveranza costanza	att
n#03814020	core	fintaggine finteria falsità insincerità	tra
n#03813132	core	irrisolutezza tentennare tiremmolla irrisoluzione irresolutezza titubanza tentennamento indecisione irresoluzione tira_e_molla tentennio	att
n#03812825	core	irrisolutezza irrisoluzione irresolutezza irresoluzione	att
n#03812111	core	assiduità diligenza	att cog
n#03811751	core	fermezza costanza	tra
n#03811208	core	mordente decisione determinazione fine grinta finalità	att
n#03810995	core	decisione determinazione risolutezza	tra
n#03810298	core	incaponimento cocciutaggine irremovibilità zucconaggine ostinazione mulaggine piccosità caparbieria caparbiaggine caparbietà incocciatura caponaggine puntiglio pervicacia testardaggine	att tra
n#03809479	core	volontà fermezza decisione determinazione risolutezza saldezza	att tra
n#03807361	core	ardire baldanza sicuranza ardimento arditezza audacia securanza	tra
n#03804255	core	ingiustizia	tra
n#03801117	core	giustizia	tra
n#03797612	core	sordidezza grettezza	tra
n#03797439	core	asprezza crudeltà ferocità	tra
n#03795640	core	mal_volere malevolenza malvolere	tra
n#03794011	core	benevolenza	tra
n#03793456	core	ingiustizia iniquità	tra
n#03792240	core	faccia_tosta arditezza audacia temerarietà intraprendenza	beh
n#03791327	core	energia	tra
n#03790984	core	ambizione brama	emo
n#03789602	core	egoismo	tra
n#03789279	core	grettezza	tra
n#03789185	core	meschinità	tra
n#03788961	core	avarizia pidocchieria pitoccheria	tra
n#03788750	core	avarizia lesina spilorceria tirchieria pitoccheria taccagneria grettezza	tra
n#03785836	core	compassione pietà misericordia compatimento pena	emo
n#03785586	core	misericordia clemenza	emo
n#03779450	core	limpidezza nitidezza nettezza chiarezza nitore perspicuità lucidità	cog
n#03777039	core	sgraziataggine goffaggine	tra
n#03771512	core	lodevolezza	tra
n#03748312	core	GAP!	tra cog
n#03736465	core	incertezza	cog
n#03717609	core	primazia sovreminenza sopraeminenza superiorità perfezione	tra
n#03706895	core	seccatura preoccupazione inconveniente	sit
n#03705224	core	facilità	tra
n#03692429	core	magia attrattiva seduzione charme vezzosità fascino malia	tra
n#03684009	core	impressione effetto	cog
n#03682651	core	sufficienza distacco noncuranza incuranza sufficenza	att
n#03681159	core	inaffidabilità	tra
n#03680922	core	incoscienza irresponsabilità	tra beh
n#03680488	core	attendibilità serietà sincerità lealtà affidabilità	tra
n#03677298	core	svagatezza	tra
n#03676822	core	inavvertenza distrazione sbalorditaggine negligenza sbadataggine	tra
n#03675022	core	impetuosità irruenza	att beh
n#03674868	core	impulsività focosità	tra
n#03674196	core	incoscienza irriflessione irriflessività sbalorditaggine sconsideratezza inconsideratezza sconsiderazione	tra
n#03669571	core	confidenza intimità familiarità intrinsechezza dimestichezza famigliarità intrinsichezza	att
n#03665167	core	giocosità	att tra
n#03663574	core	serietà	att tra emo
n#03663200	core	tentennare riluttanza tentennio	emo
n#03662995	core	ritrosia	tra
n#03662132	core	belligeranza bellicosità	tra
n#03662017	core	acrimonia acerbità	tra
n#03661420	core	intolleranza	att tra
n#03661288	core	impazienza	att emo
n#03661040	core	misantropia intrattabilità riservatezza caratteraccio irritabilità scontrosità ruvidezza ispidezza orsaggine riserbatezza	emo
n#03659752	core	magnanimità pazienza longanimità indulgenza sopportazione	att tra
n#03659222	core	durezza rigore asprezza rigidezza crudezza severità gravità	tra
n#03657503	core	pigrizia infingardaggine accidia poltroneria neghittosità poltronaggine	att tra cog
n#03657239	core	pigrizia accidia poltroneria svogliatezza scioperataggine poltronaggine	att tra
n#03657083	core	torpidezza intorpidimento	cog
n#03656931	core	abulia indifferenza disamore apatia letargo	att
n#03656520	core	ignavia pigrizia abulia oziosità fannulloneria pigrezza	sen
n#03656252	core	inerzia	att
n#03655741	core	inanizione	phy
n#03655378	core	esuberanza	att tra
n#03654225	core	energia vigore	beh
n#03652203	core	freddezza frigidità	att tra cog
n#03651126	core	ardore calore passione	att tra emo
n#03650658	core	amorevolezza	att
n#03648944	core	nervosismo	tra
n#03647110	core	solitudine	emo cog
n#02832377	core	obbrobrio orrore ignominia	emo
n#00819088	core	brivido	res beh
n#00818927	core	eccitazione	sen
n#00806644	core	giustificazione	beh
n#00797364	core	ossequio rispetto	att
n#00794619	core	oltraggio offesa ingiuria sfregio onta affronto insulto	beh
n#00793037	core	esacerbazione	emo
n#00792860	core	aggravante	beh
n#00791599	core	critica riprovazione biasimo disapprovazione	beh
n#00790504	core	apprezzamento	emo
n#00785108	core	incoraggiamento	beh
n#00783978	core	assistenza	emo
n#00758712	core	contrasto bega contesa contestazione	beh
n#00755927	core	ubbidienza	beh
n#00706393	core	carità	beh
n#00705199	core	assistenza	emo
n#00694042	core	esaudimento impetrazione	emo
n#00693756	core	divertimento	beh
n#00693635	core	piacere	beh
n#00693354	core	clemenza	emo
n#00692957	core	inibizione	cog
n#00688597	core	riposo distensione	beh sen
n#00687997	core	tentennare esitamento se peritanza dubitazione oscitanza esitazione tentennio	beh
n#00661612	core	irrigidimento	res beh
n#00630513	core	offesa offensiva	beh
n#00619852	core	forza violenza	att tra beh
n#00619330	core	belligeranza	att beh emo
n#00615322	core	scaramuccia avvisaglia	beh
n#00543267	core	voluttà	emo sen
n#00541686	core	sesso	beh
n#00503611	core	sforzo prova tentativo	beh
n#00490220	core	lussuria	emo cog sen
n#00489770	core	indolenza accidia poltroneria scioperataggine scioperatezza poltronaggine	att tra
n#00489276	core	gonfiaggine boria supponenza superbia presunzione	emo
n#00479886	core	ingiustizia ingiusto scorrettezza	beh
n#00411516	core	tensione	emo sen
n#00411168	core	difficoltà contrarietà disagio intoppo briga guaio inconveniente	beh
n#00409314	core	fatica sforzo	beh
n#00362788	core	fretta premura furia	att cog
n#00362255	core	sconvolgimento	beh
n#00361901	core	agitazione tumulto disordine	emo
n#00361442	core	clamore confusione chiasso putiferio cancan chiassata	res emo
n#00332306	core	sollazzamento distrazione diporto intrattenimento svago spasso divertimento	emo
n#00330345	core	divertimento	beh
n#00272358	core	distrazione diporto diversivo divertimento	beh
n#00271222	core	molestie_sessuali	beh
n#00271094	core	esasperazione	beh
n#00270946	core	molestia irritazione dispetto angheria disturbo	beh emo
n#00270022	core	empietà crudeltà	beh
n#00258668	core	nocimento lesione nocumento pregiudizio intaccamento detrimento	beh
n#00240869	core	esasperazione aggravamento	beh
n#00229447	core	esaurimento	beh
n#00228188	core	attenuazione alleggerimento alleviamento lenimento sgravio	emo
n#00218699	core	prostrazione	beh
n#00175673	core	invilimento avvilimento abbiosciamento umiliazione	emo
n#00133315	core	rigetto	beh
n#00103541	core	allettamento lusinga pania adescamento	beh
n#00054499	core	conquista sottomissione aggiogamento soggiogamento assoggettamento	att
n#00040483	core	disinganno	beh
n#00014045	core	sentimento	psy
a#02566453	core	convulso febbricitante febbricoso febbrile	phy
a#02446777	core	dissennato	tra beh
a#02423250	core	GAP!	cog
a#02421145	core	infermo ammalato affetto colpito malato	phy eds emo cog
a#02419103	core	apprezzato reputato beneaccetto accettevole benarrivato gradito ben_venuto benvenuto richiesto ben_arrivato benaccetto riputato	eds emo
a#02392968	core	virtuoso onesto	tra beh
a#02390806	core	violento parossistico	att tra beh
a#02377697	core	inutilizzabile inutile inadoperabile inservibile irrecuperabile irricuperabile	tra
a#02346835	core	sicuro	att tra
a#02317430	core	tollerante	tra
a#02314990	core	stanco_morto	phy
a#02302641	core	irriflessivo incosciente	tra cog sit
a#02276745	core	indomato indomito	tra
a#02229453	core	modesto	tra
a#02224056	core	GAP!	tra emo sit
a#02216003	core	duro accanito testardo puntiglioso piccoso ostinato pertinace caparbio cocciuto pervicace	tra
a#02120998	core	sicuro	tra
a#02094165	core	chiaro sincero genuino sentito franco	att tra
a#02081029	core	irrilevante da_poco dappoco insignificante	tra
a#02061900	core	erotico afrodisiaco amatorio	att tra
a#02056455	core	sexy	att tra
a#02043566	core	serio	att tra emo
a#02023556	core	egoista egoistico	tra
a#02007200	core	scrupoloso conscienzioso coscienzioso	att tra
a#02000611	core	salace mordace sarcastico velenoso caustico	att tra beh
a#01980655	core	sicuro	sit
a#01918843	core	chiaro	tra cog
a#01915914	core	distaccato	att tra
a#01862436	core	pronto preparato	att tra
a#01856161	core	inquieto	emo
a#01855798	core	agitato	tra emo sen
a#01753470	core	potente forte	phy
a#01730738	core	al_vetriolo caustico	att tra
a#01701024	core	perplesso	cog
a#01680937	core	contrito penitente pentito	emo
a#01678759	core	calmo	emo
a#01678209	core	pacifico	sit
a#01662378	core	infuocato infocato ardente appassionato travolgente passionale sviscerato	emo
a#01659853	core	parziale	att tra
a#01536230	core	modesto	tra
a#01476902	core	modesto	tra
a#01475714	core	modesto	tra
a#01407421	core	portafortuna	tra
a#01403478	core	affabile estroverso paterno dimestichevole affettuoso cordiale comunicativo bonario materno tenero amoroso benevolo compito amorevole mite	emo
a#01340057	core	GAP!	sit
a#01318368	core	benigno gentile buono grazioso garbato	att tra beh
a#01310561	core	festoso	emo
a#01308632	core	triste mesto malinconico mortificato	emo
a#01308059	core	lieto contento	man
a#01268094	core	colpevole	sit
a#01261153	core	inibito	beh
a#01254100	core	ben_informato	cog
a#01177991	core	disperato	sit
a#01144205	core	sconsigliato inconsiderato sconsiderato	cog
a#01102326	core	irritato spiacente rattristato contrariato rammaricato seccato dispiaciuto infastidito	emo
a#01101968	core	malato_d\	res
a#01097922	core	grato riconoscente	emo
a#01088903	core	scuro	moo
a#01088579	core	burbero	tra
a#01087857	core	collerico irascibile	tra
a#01080665	core	di_merda	phy cog
a#00910225	core	sleale scorretto	tra
a#00871958	core	eccitato concitato accalorato agitato fremente infervorato palpitante gasato	emo
a#00842521	core	fervido caloroso entusiastico	emo
a#00824498	core	deluso disincantato disilluso disingannato	emo
a#00813486	core	GAP!	emo
a#00751688	core	sottomesso remissivo	att tra
a#00627092	core	incuriosito curioso	tra
a#00610261	core	incredulo	cog
a#00585147	core	GAP!	att tra
a#00557893	core	insoddisfatto	emo
a#00556568	core	soddisfatto_di_sé	emo
a#00478236	core	concorrenziale competitivo	att tra
a#00475938	core	competente	tra
a#00472490	core	compassionevole	emo sit
a#00408403	core	balordo grullo tonto citrullo stupido stolto sciocco	tra
a#00404496	core	macchinoso verboso prolisso farraginoso ingarbugliato aggrovigliato confuso	cog
a#00354187	core	tetro	tra emo
a#00351663	core	gaio giocondo allegro ilare	tra
a#00331296	core	insicuro	att tra emo
a#00330980	core	sicuro_di_sé	emo
a#00330147	core	sicuro	att tra
a#00329769	core	persuaso convinto	cog
a#00328258	core	sicuro	emo cog
a#00317140	core	cauto assennato circospetto oculato misurato prudente guardingo considerato	att
a#00306130	core	malaccorto negligente sciatto	cog
a#00303585	core	attento esatto accurato	beh
a#00279556	core	discriminatorio	att emo
a#00248123	core	timoroso pauroso	att tra emo
a#00246712	core	audace	tra
a#00222609	core	malizioso	tra
a#00122245	core	GAP!	tra
a#00119743	core	litigioso livido stizzoso livoroso acrimonioso ostile maligno astioso risentito garoso rancoroso invidioso	emo
a#00117308	core	arrabbiato	emo
a#00107565	core	GAP!	tra
a#00107334	core	ambizioso	att tra emo
a#00094531	core	sveglio	cog
a#00068206	core	avventuroso	tra
a#00031053	core	avido	tra
a#00011658	core	ingordo	cog
v#01857688	core	dovere	beh
v#01843927	core	soddisfare andare_bene_a contentare	emo
v#01827903	core	intrigare avvincere incuriosire	emo
v#01827745	core	interessare	emo
v#01827420	core	preoccupare	emo
v#01822979	core	esaudire soddisfare appagare contentare	emo
v#01792695	core	richiedere volere	cog
v#01759411	core	ricattare	emo
v#01754557	core	bidonare imbrogliare	emo
v#01750864	core	marocchinare violentare stuprare violare	emo
v#01750581	core	violare	emo
v#01744324	core	contrariare	emo
v#01736849	core	infamare disonorare	emo
v#01709356	core	scomodare disturbare molestare scocciare importunare seccare infastidire incomodare	emo
v#01709101	core	disturbarsi prendersi_il_disturbo preoccuparsi incomodarsi scomodarsi	emo
v#01708144	core	terrorizzare	emo
v#01707058	core	sforzare	emo
v#01701149	core	soggiogare sottomettere asservire assoggettare	emo
v#01698559	core	svagare distrarre divertire appagare dilettare ricreare	emo
v#01698431	core	intrattenere	emo
v#01649916	core	sottomettere reprimere assoggettare	emo
v#01603985	core	raccomandare	emo
v#01531034	core	accogliere gradire	emo
v#01502875	core	nauseare disgustare stomacare	emo
v#01502738	core	rivoltare disgustare	emo
v#01481912	core	ammirare	emo
v#01456000	core	perdere	cog
v#01453033	core	pungere pizzicare	eds
v#01451659	core	patire dolorare soffrire	eds
v#01447824	core	meravigliare stordire sbalordire stupefare intronare	emo
v#01447667	core	intormentire intorpidire	emo cog
v#01441871	core	avvertire sentire	sen
v#01407881	core	agitarsi	emo
v#01378733	core	disturbare seccare	emo
v#01303431	core	trasalire sussultare	res
v#01289419	core	tremare tremolare	res
v#01247189	core	stimare apprezzare ammirare	emo
v#01246259	core	preferire	cog
v#01246175	core	desiderare disiderare	cog
v#01245362	core	desiderare disiderare volere	cog
v#01244286	core	disamorare inimicare straniare indisporre disaffezionare allontanare svogliare alienare estraniare	emo
v#01243184	core	compatire compiangere compassionare commiserare	emo
v#01243090	core	tediare stancare annoiare	emo
v#01242949	core	galvanizzare	emo
v#01242774	core	interessare incuriosire	emo
v#01242326	core	irritare esasperare inacerbire esacerbare esulcerare	emo
v#01241470	core	intimidire	emo
v#01241339	core	smontare sconfortare avvilire	emo
v#01240173	core	rinfrancare	emo
v#01240044	core	disilludere deludere disingannare	emo cog
v#01239874	core	affascinare rapire ammaliare incantare	emo
v#01239708	core	dispiacere_a	emo
v#01239331	core	non_soddisfare dispiacere scontentare	emo
v#01239189	core	accontentare appagare contentare	emo
v#01238865	core	dilettare contentare deliziare	emo
v#01238399	core	confortare riconfortare consolare sollevare sorreggere aiutare	emo
v#01238101	core	costernare affliggere demoralizzare buttar_giù scoraggiare abbattere deprimere abbacchiare avvilire	emo
v#01237297	core	addolorarsi immalinconire immelanconire	emo
v#01237116	core	addolorare rattristare abbattere deprimere immalinconire immelanconire	emo
v#01235891	core	disperarsi	emo
v#01235670	core	offendere scandalizzare	emo
v#01234562	core	sopraffare sorpassare travolgere	emo
v#01234179	core	repellere	emo
v#01234043	core	disgustare	emo
v#01233868	core	schifare nauseare disgustare stomacare	emo
v#01233464	core	allettare richiamare attirare sedurre accentrare attrarre	emo
v#01233236	core	allettare attirare attrarre	emo
v#01232387	core	stregare conquistare affascinare ammaliare innamorare sedurre incantare	emo
v#01231785	core	morire_dalla_voglia_di desiderare disiderare	emo
v#01230185	core	tormentare molestare ossessionare importunare angariare punzecchiare	emo
v#01229968	core	tormentare crocifiggere crocefiggere infastidire torturare assillare	emo
v#01229834	core	tormentare affliggere angosciare angustiare martoriare torturare martirizzare crucciare travagliare	eds
v#01228413	core	umiliare degradare	sit
v#01228249	core	mortificare umiliare scornare smaccare avvilire	emo
v#01227912	core	frustrare castrare	emo
v#01227336	core	deludere disattendere	emo
v#01226967	core	testare cimentare saggiare mettere_a_dura_prova	emo
v#01226873	core	affliggere angosciare angustiare	emo
v#01224189	core	addolorare toccare vulnerare offendere ferire	emo
v#01223033	core	confondere	cog
v#01222893	core	confondere turbare sconcertare	emo
v#01222254	core	impressionare sconcertare	emo
v#01221816	core	oltraggiare	emo
v#01221002	core	tediare contrariare tormentare disturbare stancare seccare infastidire indispettire irritare molestare scocciare importunare innervosire urtare	emo
v#01220887	core		cog
v#01217709	core	shoccare traumatizzare scioccare allucinare sconvolgere	emo
v#01217571	core	sgomentare spaventare	emo
v#01216971	core	terrorizzare inorridire	emo
v#01216252	core	intimidire	tra emo
v#01216115	core	terrificare terrorizzare atterrire spaurire impaurire	emo
v#01215448	core	temere	emo
v#01215350	core	mettere_in_fuga spaurire spaventare intimorire	emo
v#01214618	core	atterire spaurire spaventare intimorire impaurire	emo
v#01214483	core	venerare adorare	emo
v#01214144	core	idolatrare venerare adorare	emo
v#01213998	core	amare voler_bene adorare	emo
v#01212004	core	amare	emo
v#01211759	core	amare	emo
v#01211520	core	sprezzare disprezzare disdegnare	emo
v#01211403	core	abominare aborrire abborrire abbominare detestare esecrare	emo
v#01211167	core	detestare odiare	emo
v#01210323	core	covare nutrire albergare	emo
v#01209074	core	sentirsi sentire	emo
v#01208224	core	perturbare scombussolare impressionare commuovere turbare agitare sconvolgere stravolgere	emo
v#01205957	core	tormentarsi affliggersi	emo
v#01205112	core	impensierire turbare preoccupare	emo
v#01204371	core	racchetare calmare pacare rabbonire ammansire tranquillare placare	emo
v#01203947	core	conturbare turbare sconvolgere	emo cog
v#01203630	core	tranquillizzarsi pacarsi ammansirsi acquietarsi calmarsi rabbonirsi placarsi	emo
v#01202989	core	esaltare	emo
v#01202395	core	shoccare scioccare scuotere eccitare scotere turbare	emo
v#01034588	core	respingere allontanare scacciare	eds
v#00977560	core	fottere trombare chiavare sbattere scopare	emo
v#00958408	core	picchiare bastonare battere percuotere malmenare	eds emo
v#00914799	core	affogare sommergere coprire	emo
v#00823328	core	tastare toccare palpare	beh
v#00809510	core	stordire	cog
v#00808096	core	richiedere necessitare	cog
v#00807946	core	agognare bramare	cog
v#00760988	core	caricare	emo
v#00752542	core	soverchiare sopraffare soperchiare	emo
v#00689385	core	calmare tranquillizzare	emo
v#00623049	core	dubitare	emo
v#00612137	core	rimettere abbonare perdonare	emo
v#00591365	core	allarmare sgomentare allertare turbare	emo
v#00590070	core	ammonire	emo
v#00583232	core	svagare distrarre divertire trastullare dilettare sollazzare	emo
v#00582994	core	consolare sollevare allietare rasserenare rallegrare	emo
v#00582840	core	rincuorarsi rasserenarsi rallegrarsi consolarsi	emo
v#00582621	core	aizzare incitare	emo
v#00576741	core	deridere sfottere canzonare	emo
v#00575170	core	ingiuriare insolentire insultare villaneggiare	emo
v#00571108	core	incolpare	emo
v#00560318	core	rimbrottare	emo
v#00558872	core	rimproverare riprendere fustigare richiamare ammonire biasimare deplorare condannare	emo
v#00548543	core	bocciare	emo cog
v#00547234	core	divergere cozzare scontrarsi	emo
v#00542706	core	respingere	emo
v#00541445	core	sprezzare respingere sdegnare disdegnare	emo
v#00531787	core	sommuovere incitare istigare	emo
v#00522671	core	convincere	cog
v#00491471	core	strabiliare stupire meravigliare sbalordire stupefare	emo
v#00483900	core	trovare credere pensare	emo
v#00481620	core	accarezzare	emo cog
v#00479841	core	desiderare disiderare volere	cog
v#00467945	core	avvantaggiare	cog
v#00466561	core	sentirsi	sen
v#00462716	core	repudiare ripudiare	att cog
v#00454501	core	ratificare approvare	emo
v#00450980	core	trangugiare	emo
v#00421500	core	sfasare disorientare sconcertare	emo
v#00421101	core	confondere	cog
v#00417522	core	snobbare trascurare ignorare	cog
v#00415625	core	piantare	emo
v#00358494	core	sottolineare accentuare evidenziare	cog
v#00293010	core	allentare allascare	phy emo
v#00260318	core	intiepidirsi intiepidire scaldarsi intepidire	sit
v#00260044	core	intiepidire intepidire riscaldare	emo
v#00183525	core	intontire stordire tramortire istupidire	emo
v#00177802	core	danneggiare	emo
v#00142648	core	aggravare acuire peggiorare esasperare inacerbire esacerbare esulcerare inasprire rincrudire	beh
v#00132229	core	impietrire pietrificare	res emo
v#00071939	core	sentirsi	sen
v#00061196	core	traumatizzare	emo
v#00059890	core	disgustare	emo
v#00056311	core	attutire acquietare lenire	emo
v#00049733	core	affliggere travagliare prostrare	emo
v#00048850	core	toccare vulnerare ferire	eds emo
v#00044261	core	irritare	emo
v#00018983	core	scaldarsi	emo cog
v#00018644	core	rilassarsi scaricarsi rilasciarsi distendersi	emo
v#00018031	core	rilassare	emo
v#00016435	core	tramortire svenire basire	phy emo cog
v#00010240	core	dormicchiare sonnecchiare	beh
r#00223640	core	iratamente stizzosamente rabbiosamente irosamente	emo
r#00053329	core	spaventosamente terribilmente disumanamente tremendamente	emo
r#00053180	core	spaventosamente maledettamente malissimo disumanamente tremendamente formidabilmente	emo
r#00051724	core	bene	phy emo moo
r#00012661	core	vantaggiosamente	emo
r#00010099	core	bene	emo
a#00010710	similar-to	ghiottone	cog
a#00030536	similar-to	GAP!	tra
a#00053037	similar-to	congruo	sit
a#00088670	similar-to	GAP!	emo
a#00089141	similar-to	delirante pazzo fuori_di_sé frenetico	emo
a#00121842	similar-to	animato mosso movimentato vivace vivo	phy cog
a#00177964	similar-to	propizio favorevole fasto fausto	att tra
a#00243444	similar-to	sanguinoso	att tra beh
a#00248588	similar-to	timido riservato schivo riserbato	att tra emo
a#00306683	similar-to	GAP!	cog
a#00318095	similar-to	GAP!	att
a#00353226	similar-to	disinvolto	tra
a#00478654	similar-to	in_concorrenza	att tra
a#00480379	similar-to	GAP!	tra
a#00549951	similar-to	inalterabile	tra cog
a#00566069	similar-to	controllato	cog
a#00602736	similar-to	premuroso riguardoso	att tra beh
a#00659233	similar-to	vinto	sit
a#00669114	similar-to	sconfortato scoraggiato depresso abbattuto scorato avvilito sbattuto sfiduciato moscio smontato umiliato mogio	emo
a#00710342	similar-to	arduo difficile difficoltoso	emo sit
a#00744086	similar-to	indeciso indistinto	sen
a#00831759	similar-to	energico vigoroso	sit
a#00844381	similar-to	invogliato	emo
a#00859401	similar-to	spregevole vigliacco disprezzabile vile	tra
a#00871295	similar-to	eccitabile emotivo	emo
a#00888335	similar-to	GAP!	cog
a#00918492	similar-to	bizzarro inusato malagevole prodigioso inusuale mostruoso strano curioso disusato strambo anomalo insolito	sit
a#00976328	similar-to	inadattabile	tra
a#00999165	similar-to	infelice	emo
a#01065548	similar-to	generoso	att tra beh
a#01067907	similar-to	contraffatto falso falsificato	tra
a#01073959	similar-to	glorioso	tra
a#01145952	similar-to	utile	att tra beh
a#01209385	similar-to	sovraumano sovrumano	tra
a#01212979	similar-to	umoristico	tra
a#01229098	similar-to	imponente	tra emo
a#01252717	similar-to	edotto informato consapevole	cog
a#01262811	similar-to	offeso infortunato	emo
a#01288643	similar-to	GAP!	cog
a#01289289	similar-to	indifferente	emo
a#01291274	similar-to	GAP!	emo
a#01339871	similar-to	lecito	sit
a#01401505	similar-to	amato benvoluto stimato	emo
a#01402650	similar-to	GAP!	emo
a#01447403	similar-to	intenso vivo	emo
a#01467790	similar-to	moderato modico modesto	tra
a#01475929	similar-to	GAP!	tra
a#01485475	similar-to	immorale scostumato	tra
a#01514900	similar-to	innaturale	sit
a#01519028	similar-to	soprannaturale sovrannaturale	tra
a#01529876	similar-to	orrendo infame pauroso orribile	tra emo
a#01550110	similar-to	inavvertito	sit
a#01604600	similar-to	ottimistico ottimista	emo
a#01606992	similar-to	ordinato	sit
a#01609771	similar-to	male_organizzato disorganizzato dispersivo	cog
a#01613712	similar-to	corrente qualunque comune ordinario banale qualsiasi	att
a#01616002	similar-to	eclatante straordinario	emo sit
a#01648301	similar-to	doloroso penoso	tra eds
a#01662898	similar-to	ardente focoso appassionato	emo
a#01663469	similar-to	GAP!	emo
a#01728957	similar-to	funzionale amabile buono dolce ospitale confortevole accogliente	emo
a#01730232	similar-to	sgradevole increscioso inamabile spiacevole disameno inameno antipatico odioso	emo
a#01733361	similar-to	soddisfatto contento	emo
a#01733963	similar-to	scontento	emo
a#01758782	similar-to	autorevole altolocato influente	tra
a#01840382	similar-to	GAP!	tra
a#01846136	similar-to	opinabile dubbio confutabile disputabile eccepibile contestabile discutibile problematico	tra emo cog
a#01920205	similar-to	incerto titubante insicuro esitante irrisoluto dubbioso irresoluto peritoso tentennante	emo
a#01925162	similar-to	irresponsabile	tra
a#01929458	similar-to	disfrenato effrenato sfrenato	tra
a#01940621	similar-to	reverente riverente	emo
a#01964598	similar-to	robusto aitante gagliardo prestante quartato	tra
a#01996274	similar-to	demenziale insano delirante alienato folle forsennato	tra
a#02024286	similar-to	GAP!	tra
a#02025056	similar-to		tra
a#02035198	similar-to	separato	cog
a#02057430	similar-to	salace piccante	att tra
a#02220457	similar-to	florido in_rapida_ascesa prospero	tra
a#02221362	similar-to	GAP!	tra
a#02225138	similar-to	manchevole	emo cog
a#02230368	similar-to	superiore	tra
a#02233830	similar-to	inferiore	tra
a#02247656	similar-to	GAP!	emo
a#02249645	similar-to	sensitivo	tra
a#02313346	similar-to	fiacco stanco	phy
a#02315952	similar-to	riposato	phy cog sen
a#02345809	similar-to	attendibile fidato	att tra
a#02435610	similar-to	GAP!	beh emo
n#00096215	antonym	irrigidimento	beh
n#00240588	antonym	consolidamento rinforzo	beh
n#00764013	antonym	disobbedienza disubbidienza	beh
n#00787796	antonym	benedizione approvazione crisma	beh
n#03655835	antonym	alacrità laboriosità attività	att
n#03662405	antonym	bonavoglia buonavoglia	tra
n#03664422	antonym	frivolezza	att tra emo
n#03679828	antonym	responsabilità	tra beh
n#03734468	antonym	certezza sicurezza	cog
n#03781615	antonym	astrusità ermetismo metafisicheria astruseria oscurità	cog
n#03789939	antonym	egoismo	tra
n#03787376	antonym	munificenza generosità	tra
n#03788315	antonym	altruismo generosità disinteresse	tra
n#03794865	antonym	delicatezza	tra
n#03814900	antonym	onore	sit
n#03832977	antonym	imprudenza	beh
n#03843872	antonym	disobbedienza disubbidienza	tra
n#03846037	antonym	garbatezza creanza compiacenza buona_grazia gentilezza bontà buonagrazia educazione cortesia amabilità civiltà contegno garbo delicatezza	beh
n#03856323	antonym	GAP!	sen
n#03873073	antonym	luminosità	cog
n#03908392	antonym	grazia garbo	beh
n#03927465	antonym	fibra forza	tra
n#04405653	antonym	attenzione avvertenza	cog
n#04702275	antonym	neutralità imparzialità	beh
n#04705323	antonym	GAP!	att
n#04705529	antonym	irriverenza	att
n#05013513	antonym	omologazione	beh
n#05575439	antonym	beneplacito approvazione consenso	emo
n#05578483	antonym	ingratitudine disconoscenza sconoscenza	emo
n#05578595	antonym	interessamento	emo
n#05595229	antonym	coraggio	emo
n#10072413	antonym	scontro dissenso	att emo
n#10078249	antonym	innocenza	beh
n#10357889	antonym	onore	emo
a#00167697	antonym	incantevole magico portentoso stupendo affascinante allettante avvenente piacente bellissimo attraente delizioso fascinoso allettevole appetibile paradisiaco suggestivo magnifico meraviglioso	tra emo
a#00280426	antonym	equanime spassionato neutrale imparziale	att emo
a#00444669	antonym	comodo	man
a#00477060	antonym	inetto	tra
a#00584542	antonym	cooperativo	att tra
a#00609652	antonym	gonzo credulo baggiano credulone	cog
a#00661714	antonym	consenziente malleabile maneggevole prono condiscendente corrivo arrendevole accondiscendente permissivo assenziente remissivo	tra
a#00749952	antonym	prepotente	att tra
a#00823936	antonym	incantato fatato stregato	emo
a#00843640	antonym	GAP!	emo
a#00909260	antonym	giusto	tra
a#01083652	antonym	infame cattivo sciagurato scellerato reprobo tristo malefico malvagio peccaminoso	tra
a#01098383	antonym	irriconoscente ingrato disconoscente sconoscente	emo
a#01261943	antonym	disinibito	beh
a#01267106	antonym	innocente candido	sit
a#01281024	antonym	intelligente	tra
a#01310938	antonym	dolente triste	emo
a#01407688	antonym	scalognato mal_augurato malaugurato sfortunato	tra
a#01660176	antonym	equanime spassionato imparziale	att tra
a#01681432	antonym	impenitente	emo
a#01981873	antonym	pericoloso	sit
a#02078228	antonym	importante significante	tra
a#02095621	antonym	insincero	att tra
a#02217708	antonym	docile mansueto	tra
a#02222066	antonym	fallito	tra
a#02275724	antonym	domato	tra
a#02376473	antonym	utile	tra
a#02419749	antonym	malaccetto malaccolto sgradito mal_accetto mal_accolto	eds emo
a#02445888	antonym	saggio	tra beh
v#00017738	antonym	contrarre	emo
v#00044675	antonym	disacerbare assopire calmare mitigare sopire blandire lenire sedare addolcire placare ammollire molcere	emo
v#00140937	antonym	migliorare valorizzare	beh
v#00350457	antonym	solleticare stuzzicare sollecitare	emo
v#00469237	antonym	stimare rispettare valutare	att emo
v#00548199	antonym	ratificare sancire sanzionare approvare	emo cog
v#00614225	antonym	dolersi	emo
v#01034097	antonym	calamitare polarizzare attirare accentrare attrarre	eds
v#01237403	antonym	allietare rallegrare	emo
v#01237013	antonym	allietarsi	emo
v#01236227	antonym	sollevare	emo
v#01531148	antonym	ricusare respingere recusare rigettare rifiutare	att emo
v#01674231	antonym	riverire onorare rispettare	emo cog
r#00010485	antonym	male	emo
r#00012882	antonym	svantaggiosamente sconvenientemente	emo
n#10306292	pertains-to	febbrone	phy
n#05560878	pertains-to	emozione commozione	psy
a#02519277	pertains-to	benvolente benevolente	beh
a#02598044	pertains-to	ricreativo	beh
a#02613791	pertains-to	termico	sit
a#02655713	pertains-to	tattile	sen
a#02715926	pertains-to	traumatizzante traumatico	beh
a#02758309	pertains-to	appetitivo	sen
a#02840545	pertains-to	penitenziale	emo
a#00066844	derived-from	salutare giovevole profittevole benefico vantaggioso	emo
a#01080665	derived-from	di_merda	emo
a#01915914	derived-from	distaccato	att
a#00117308	derived-from	arrabbiato	emo
a#01177991	derived-from	disperato	emo
r#00003409	derived-from	garbatamente benignamente gentilmente premurosamente bonariamente benevolmente	att tra beh
r#00014775	derived-from	gravemente seriamente	emo
r#00015508	derived-from	maliziosamente	tra
r#00034364	derived-from	curiosamente balordamente bizzarramente strambamente singolarmente stranamente	tra
r#00040173	derived-from	fortunatamente fortuitamente	tra
r#00040533	derived-from	tristemente	emo
r#00048845	derived-from	giocondamente	att tra emo
r#00049382	derived-from	saldamente fermamente	tra cog
r#00053329	derived-from	spaventosamente terribilmente disumanamente tremendamente	phy cog
r#00054569	derived-from	schifosamente malissimo orridamente tremendamente	emo
r#00059927	derived-from	meditatamente appositamente calcolatamente intenzionalmente deliberatamente apposta volutamente scientemente studiatamente	sit
r#00075112	derived-from	affezionatamente affettuosamente caramente	tra emo
r#00077772	derived-from	disdegnosamente schernevolmente sdegnosamente sprezzantemente spregiativamente	emo
r#00078147	derived-from	follemente da_folle da_pazzo pazzamente	tra beh emo
r#00078632	derived-from	comicamente	emo
r#00080201	derived-from	vertiginosamente	cog
r#00087631	derived-from	straccamente stancamente	sit
r#00090468	derived-from	tristemente deplorevolmente	eds emo
r#00107789	derived-from	umilmente	tra
r#00107917	derived-from	docilmente	tra
r#00111977	derived-from	penosamente	emo
r#00116887	derived-from	felicemente	tra
r#00137715	derived-from	febbrilmente	emo
r#00140819	derived-from	sicuro sicuramente ben_inteso certo beninteso certamente	emo cog
r#00142272	derived-from	dubbiosamente	att tra cog
r#00149700	derived-from	attentamente consideratamente	beh
r#00150109	derived-from	concitatamente	emo
r#00159973	derived-from	sprovvedutamente inconsultamente	cog
r#00160986	derived-from	freddamente	att tra
r#00161132	derived-from	seriamente	att tra
r#00170270	derived-from	apologeticamente	beh
r#00171299	derived-from	all\	emo
r#00171508	derived-from	balordamente stupidamente insensatamente stoltamente	tra
r#00174625	derived-from	coscienziosamente religiosamente scrupolosamente	att tra
r#00175618	derived-from	schifiltosamente schizzinosamente	att tra
r#00177762	derived-from	vigorosamente	sit
r#00179150	derived-from	meravigliosamente stupendamente portentosamente sovranamente	emo sit
r#00179882	derived-from	sobriamente	tra
r#00182087	derived-from	affannosamente ansiosamente penosamente apprensivamente	emo
r#00185177	derived-from	appassionatamente entusiasticamente	emo
r#00186377	derived-from	gravemente ieraticamente solennemente	tra
r#00193184	derived-from	enormemente	emo sit
r#00195597	derived-from	ostinatamente testardamente caparbiamente cocciutamente	tra
r#00196189	derived-from	vittoriosamente	emo
r#00196317	derived-from	paurosamente	emo
r#00196457	derived-from	intrepidamente impavidamente	tra
r#00197382	derived-from	ansiosamente smaniosamente desiderosamente	phy sen
r#00197662	derived-from	malevolmente malignamente	tra
r#00197782	derived-from	selvaggiamente brutalmente ferocemente	tra
r#00198370	derived-from	scioccamente balordamente insensatamente stoltamente	tra beh
r#00199681	derived-from	aristocraticamente	tra
r#00202298	derived-from	caritatevolmente piamente	tra
r#00203513	derived-from	irrequietamente	tra emo sen
r#00205200	derived-from	GAP!	tra
r#00206319	derived-from	focosamente appassionatamente svisceratamente passionalmente	emo
r#00208567	derived-from	grintosamente	tra cog
r#00209071	derived-from	disinvoltamente sicuramente confidentemente spigliatamente	att tra
r#00210579	derived-from	espertamente	tra
r#00211373	derived-from	stucchevolmente noiosamente uggiosamente tediosamente	emo
r#00213545	derived-from	impensatamente	tra cog sit
r#00214298	derived-from	compuntamente	emo
r#00215267	derived-from	mirabilmente encomiabilmente lodevolmente ammirevolmente	tra
r#00216245	derived-from	vivamente caldamente calorosamente cordialmente	tra
r#00220271	derived-from	violentemente	att tra beh
r#00223640	derived-from	iratamente stizzosamente rabbiosamente irosamente	emo
r#00225227	derived-from	ritrosamente vergognosamente paurosamente timidamente	att tra
r#00225391	derived-from	amorevolmente	emo
r#00227177	derived-from	allegramente	emo
r#00228716	derived-from	cupamente lugubremente tetramente	emo
r#00234420	derived-from	pietosamente compassionevolmente	emo
r#00235380	derived-from	dimessamente modestamente pudicamente semplicemente	tra
r#00238715	derived-from	scontrosamente	moo
r#00238872	derived-from	ostilmente bellicosamente	att
r#00239709	derived-from	competitivamente agonisticamente	att tra
r#00240003	derived-from	ossessivamente	cog
r#00241621	derived-from	trucemente acremente efferatamente ferocemente	beh
r#00241807	derived-from	trucemente acremente	att tra beh emo
r#00242297	derived-from	GAP!	emo
r#00261067	derived-from	apaticamente	cog
r#00261423	derived-from	prepotentemente boriosamente altezzosamente arrogantemente	tra
r#00262544	derived-from	avidamente	cog
r#00270270	derived-from	beatamente	emo
r#00271781	derived-from	ingordamente golosamente ghiottamente avidamente cupidamente	tra
r#00273230	derived-from	barbaramente	tra
r#00273738	derived-from	salutarmente	phy eds
r#00277483	derived-from	causticamente	att tra
r#00277636	derived-from	diligentemente attentamente piano studiosamente cautamente oculatamente consideratamente accuratamente guardingamente prudentemente	att
r#00277901	derived-from	malaccortamente	cog
r#00280723	derived-from	slealmente scorrettamente	tra
r#00284503	derived-from	vanamente boriosamente	tra
r#00285719	derived-from	cinicamente	att tra
r#00287994	derived-from	irritabilmente	att tra
r#00290067	derived-from	incredibilmente spaventosamente inverosimilmente sbalorditivamente romanzescamente formidabilmente pazzescamente	tra
r#00290667	derived-from	GAP!	att tra
r#00298208	derived-from	riprovevolmente biasimevolmente	emo
r#00298948	derived-from	ardimentosamente	att tra
r#00300063	derived-from	sforzatamente mal_volentieri malavoglia malvolentieri malincuore	att
r#00302613	derived-from	remissivamente	att tra
r#00303849	derived-from	spregevolmente ignobilmente abiettamente	tra emo
r#00308881	derived-from	schifosamente disgustosamente	beh emo
r#00315910	derived-from	GAP!	emo
r#00316857	derived-from	incertamente equivocamente	emo
r#00317916	derived-from	GAP!	cog
r#00318182	derived-from	ottusamente	tra
r#00319955	derived-from	estaticamente	emo
r#00321960	derived-from	egoisticamente	tra
r#00330276	derived-from	infidamente proditoriamente	tra
r#00331034	derived-from	familiarmente	tra
r#00340392	derived-from	spaventosamente paurosamente	emo
r#00342819	derived-from	giocondamente gioiosamente gaudiosamente festosamente lietamente	emo
r#00346753	derived-from	burberamente	tra
r#00346852	derived-from	colpevolmente	emo
r#00348685	derived-from	odiosamente	tra
r#00349302	derived-from	spericolatamente	att tra
r#00356374	derived-from	pigramente indolentemente	tra
r#00367067	derived-from	mediocremente	cog
r#00367841	derived-from	svogliatamente indolentemente	tra
r#00373134	derived-from	schietto sinceramente	att tra
r#00374554	derived-from	offensivamente	emo
r#00378612	derived-from	gelosamente	emo
r#00379595	derived-from	giovialmente	att tra emo
r#00381393	derived-from	lascivamente lussuriosamente	tra cog
r#00381927	derived-from	ridicolmente	tra
r#00383647	derived-from	oscenamente	tra
r#00388736	derived-from	lascivamente cupidamente	tra cog
r#00389294	derived-from	maestosamente	tra emo
r#00390903	derived-from	svenevolmente	tra emo
r#00393252	derived-from	meditatamente	att cog
r#00397323	derived-from	miracolosamente prodigiosamente	tra
r#00397497	derived-from	miseramente	tra
r#00398064	derived-from	GAP!	moo
r#00398358	derived-from	cupamente	moo
r#00399172	derived-from	lamentosamente luttuosamente	emo
r#00406589	derived-from	ossequiosamente servilmente	att tra
r#00413486	derived-from	scontrosamente	tra beh
r#00413918	derived-from	pensosamente	att cog
r#00416074	derived-from	filantropicamente	tra
r#00419218	derived-from	pietosamente	tra
r#00422664	derived-from	impotentemente	tra
r#00431564	derived-from	curiosamente stravagantemente balordamente bizzarramente strambamente singolarmente cervelloticamente stranamente	tra
r#00432013	derived-from	discutibilmente equivocamente	cog
r#00434226	derived-from	riflessivamente	att cog
r#00435658	derived-from	astiosamente risentitamente	emo
r#00437488	derived-from	vendicativamente	tra
r#00440176	derived-from	sarcasticamente sardonicamente	att tra beh
r#00440953	derived-from	scetticamente	att cog
r#00441387	derived-from	scurrilmente	att emo
r#00443833	derived-from	sentimentalmente	tra emo
r#00444458	derived-from	serenamente	emo
r#00449995	derived-from	sapientemente destramente abilmente magistralmente	tra
r#00451625	derived-from	sonnacchiosamente	cog
r#00469972	derived-from	teneramente	tra emo
r#00486667	derived-from	veementemente	att tra beh emo
r#00487465	derived-from	ignobilmente turpemente	tra
r#00494028	derived-from	zelantemente	tra
r#00496696	derived-from	dissipatamente scostumatamente dissolutamente	tra
a#00258473	also-see	bravo strenuo prode valoroso animoso coraggioso ardito	tra
a#01674134	also-see	insofferente impaziente	emo
a#02192636	also-see	vacillante instabile	emo
a#00841516	also-see	intraprendente	att tra emo
a#00538832	also-see	inanimato inconscio incosciente	phy cog
a#00260289	also-see	codardo pusillanime vigliacco vile	att tra emo
a#00718804	also-see	diligente solerte	beh
a#01825807	also-see	avvertito cauto previdente oculato giudizioso prudente avveduto accorto savio provvido sagace lungimirante	beh
a#00166631	also-see	sventato distratto inattento irrazionale sbadato svagato stordito disattento	cog
a#00318235	also-see	malavveduto	cog
a#00719426	also-see	negligente	cog
a#01062627	also-see	generoso	tra
a#00301090	also-see	capace	tra
a#00800025	also-see	efficiente	tra
a#02120292	also-see	qualificato esperto specializzato	tra
a#02336391	also-see	impensierito sconvolto affannato inqueto turbato preoccupato allarmato inquieto ansioso	emo
a#01148308	also-see	inutile	att tra
a#00608804	also-see	favoloso pazzesco incredibile spettacoloso rocambolesco spaventoso	cog
a#02344540	also-see	malfidato	cog
a#00835722	also-see	inquisitore interrogativo indagatore curioso investigatore	tra
a#02219234	also-see	indisciplinato insubordinato	tra
a#02218350	also-see	dipendente	att tra
a#02290045	also-see	teso	emo
a#02148682	also-see	socievole	tra
a#02461416	also-see	valido degno	tra
a#00825624	also-see	ignominioso ignobile desolante scoraggiante demoralizzante degradante infamante squallido mortificante vituperoso vergognoso misero deprimente avvilente disonorevole inonorato sconsolante sconfortante obbrobrioso disperante	sit
a#01605516	also-see	pessimista catastrofico pessimistico	sit
a#01752468	also-see	impossibile	sit
a#00085200	also-see	aggressivo battagliero	att
a#01400194	also-see	odioso	att
a#01768786	also-see	ritardato	tra
a#01445129	also-see	misericordioso pietoso	att tra beh
a#00631927	also-see	maledetto	phy emo cog
a#00648548	also-see	perbene per_bene	tra
a#00298353	also-see	calmo tranquillo sereno placido quieto	sit
a#00795039	also-see	efficace valido	phy
a#00950068	also-see	fertile fecondo	phy
a#01754717	also-see	possente robusto poderoso potente vigoroso	phy
a#00950799	also-see	infecondo	tra
a#01436246	also-see	significativo	tra
a#01917862	also-see	determinato deciso risoluto	tra
a#01771525	also-see	pronto preparato	att tra
a#02442005	also-see	volonteroso volenteroso	att tra
a#01113117	also-see	innocuo inoffensivo	sit
a#02403408	also-see	invulnerabile	sit
a#00105187	also-see	egoista personalistico egoistico narcisistico	tra
a#01063943	also-see	tirato migragnoso taccagno tignoso pitocco avaro micragnoso pidocchioso spilorcio sparagnino gretto tirchio	tra
a#02022535	also-see	maliardo seducente seduttorio malioso provocante seduttivo seduttore	att tra
a#01066794	also-see	autentico vero	att tra
a#01255933	also-see	ingenuo candido	att tra
a#00997654	also-see	fortunato	tra
a#01793947	also-see	produttivo	tra
a#00527630	also-see	piacevole ameno gradevole gustoso	emo
a#02331399	also-see	intrattabile	tra
a#01672718	also-see	paziente	tra
a#00796147	also-see	inefficace	tra
a#01800617	also-see	GAP!	tra
a#00350226	also-see	castigato casto puro onesto	tra beh
a#01484340	also-see	retto onesto	tra beh
a#01713215	also-see	pio devoto	tra beh
a#02405088	also-see	voluto	eds emo
a#01124146	also-see	malsano	phy eds emo cog
a#00070368	also-see	sconsigliabile	tra beh
a#00070853	also-see	malconsigliato	tra beh
a#01743148	also-see	impolitico	tra beh
a#01826937	also-see	incauto imprudente	tra beh
v#00011707	also-see	appisolarsi assopirsi	beh
v#00429256	also-see	sbrogliare risolvere	emo
v#01761948	also-see	protestare fomentare agitarsi eccitare attizzare	emo
v#01574057	also-see	assumere	att emo
v#01602202	also-see	mutuare	att emo
a#00351663	attribute	gaio giocondo allegro ilare	sit
a#02043566	attribute	serio	att tra emo
a#02045053	attribute	frivolo	att tra emo
a#02046203	attribute	scherzoso giocoso	att tra
a#00398231	attribute	chiaro	cog
a#00107334	attribute	ambizioso	emo
a#00221675	attribute	malevolo maldisposto perverso prevenuto	tra
a#00246332	attribute	intraprendente	tra
a#00248123	attribute	timoroso pauroso	att tra emo
a#01817423	attribute	fiero superbo orgoglioso altero inorgoglito	emo
a#01821083	attribute	umile	att tra beh
a#01554065	attribute	ubbidiente disciplinato obbediente	tra
a#01555193	attribute	disobbediente indocile disubbidiente ribelle	tra
a#01144205	attribute	sconsigliato inconsiderato sconsiderato	att
a#01145007	attribute	attento premuroso delicato riguardoso	att
a#00084648	attribute	intrepido spericolato impavido	emo
a#00192906	attribute	sconcertante impressionante preoccupante minaccioso allarmante inquietante	emo
a#00330147	attribute	sicuro	emo
a#01099314	attribute	felice	emo
a#01101074	attribute	triste infelice	emo
a#01862436	attribute	pronto preparato	tra
a#00873571	attribute	avvolgente entusiasmante esaltante inebriante trascinante solleticante eccitante coinvolgente mozzafiato avvincente elettrizzante emozionante appassionante	sen
a#00875100	attribute	GAP!	sen
n#03790984	is-value-of	ambizione brama	att tra emo
n#03691562	is-value-of	appetibilità avvenenza estetica	tra emo
n#04401608	is-value-of	certezza	emo cog
n#04401836	is-value-of	baldanza fiducia_in_se_stesso sicurezza sicurezza_di_sé	emo cog
n#05595498	is-value-of	affidamento buona_fede buonafede fiducia fede	att tra
n#10364483	is-value-of	agio	man
n#04051149	is-value-of	insulsaggine	emo
n#03792988	is-value-of	equità adeguatezza onestà	tra
n#03668938	is-value-of	affabilità confidenzialità amichevolezza simpatia	tra
n#03800378	is-value-of	bontà buono	tra
n#04406313	is-value-of	cura	cog
n#04350992	is-value-of	raziocinio intelligenza comprendonio	tra
n#03793754	is-value-of	affabilità cordialità benignità bontà cortesia amabilità gentilezza	att tra beh
n#03837747	is-value-of	modestia pudicizia verecondia pudore	tra
n#03930785	is-value-of	bontà	tra
n#04041746	is-value-of	potere potenza	tra
n#05076385	is-value-of	causticità scherno ironia sarcasmo	att tra beh
n#03663574	is-value-of	serietà	att tra emo
n#03813378	is-value-of	lealtà	att tra
n#10383516	is-value-of	fortuna	tra
n#04011108	is-value-of	vantaggiosità utilità	tra
n#03648054	is-value-of	temperamento indole animo	att
